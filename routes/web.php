<?php

//  Load Classes
use Intervention\Image\Image;


//  Define Patterns
Route::pattern( 'cache_file_name', '[a-zA-Z0-9\-\_]+' );
Route::pattern( 'cache_file_ext', '(' . implode( '|', config( 'laravel-core.image_exts' ) ) . ')' );
Route::pattern( 'cache_file_info', '[a-zA-Z0-9\=]+' );


//  Cache Route Base
$cache_route_base = str_ireplace( '\\', '/', assets()->getRelative()->cache( null, false ) );

//  Define the Cache Path with Settings
Route::get( $cache_route_base . '{cache_file_name}__{cache_file_info}.{cache_file_ext}', function( $file_name, $file_info, $file_ext )
{
    //  Return
    return handle_cache_request( $file_name, $file_ext, $file_info );
} );

//  Define the Cache Path
Route::get( $cache_route_base . '{cache_file_name}.{cache_file_ext}', function( $file_name, $file_ext )
{
    //  Return
    return handle_cache_request( $file_name, $file_ext );
} );

//  Define the Cache Path Fallback
Route::get( $cache_route_base . '{any}', function( $any )
{
    //  Return
    return respond_cache_404( $any );
} );


//  Handle the Cache Request
function handle_cache_request( $file_name, $file_ext = null, $file_info = null )
{
    //  Source Directories
    $sources = apply_filters( 'image_sources', array( assets()->upload( null, false ), assets()->asset( 'images/', false ) ) );

    //  Read Request File Info
    $pathinfo = pathinfo( $file_name );

    //  Fix Extension
    if( !$file_ext ) $file_ext = @$pathinfo['extension'];

    //  Config
    $config = array(
        'fit_size' => false,
        'round' => false,
        'strict' => false,
        'wprimary' => false,
        'hprimary' => false,
        'width' => null,
        'height' => null,
        'quality' => 65
    );

    //  Result
    $result = null;

    //  Is 404
    $is404 = ( !$pathinfo || !in_array( $file_ext, config( 'laravel-core.image_exts' ) ) );

    //  Check
    if( !$is404 )
    {
        //  Check
        if( $file_info )
        {
            //  Decode
            $configStr = base64_decode( $file_info );

            //  Explode Again
            $explodes2 = explode( '|', $configStr );

            //  Loop Each
            foreach( $explodes2  as $exp )
            {
                //  Check for Fit
                if( $exp == 'fit' ) $config['fit_size'] = true;

                //  Check for Round
                else if( $exp == 'round' )  $config['round'] = true;

                //  Check for Strict
                else if( $exp == 'strict' )  $config['strict'] = true;

                //  Check for Width Primary
                else if( $exp == 'wprimary' )  $config['round'] = true;

                //  Check for Height Primary
                else if( $exp == 'hprimary' )  $config['hprimary'] = true;

                //  Check for Quality
                else if( preg_match( '/\q(?<quality>[0-9]+)/i', $exp, $qMatch ) )
                {
                    //  Set Quality
                    $config['quality'] = $qMatch['quality'];
                }

                //  Check Dimensions
                else if( preg_match( '/(?<width>[0-9]+)(x)?(?<height>[0-9]+)/i', $exp, $dimMatch ) )
                {
                    //  Set Width
                    $config['width'] = (int)$dimMatch['width'];

                    //  Set Height
                    $config['height'] = ( isset( $dimMatch['height'] ) ? (int)$dimMatch['height'] : $config['width'] );
                }
            }
        }

        //  Source Image
        $sourceImage = null;

        //  Loop Each
        foreach( $sources as $source ) {

            //  File
            $tFile = $source . ( isset( $pathinfo['dirname'] ) && $pathinfo['dirname'] != '.' ? $pathinfo['dirname'] . '/' : '' ) . $file_name . '.' . $file_ext;

            //  Check
            if( file_exists( $tFile ) )
            {
                //  Set Source Image
                $sourceImage = $tFile;
                break;
            }
        }
dd($sourceImage, $config, $is404, $pathinfo, $sources, $file_name, $file_info);
    }

    //  Return
    return ( $is404 ? respond_cache_404( $config ) : $result );
}

//  Respond with Cache 404
function respond_cache_404( $config = null )
{
    //  404 Image
    $image404 = apply_filters( 'cache_404_image', assets()->placeholderImage( null, false ) );

    //  Read Image
    $image = new Image( $image404 );
dd($image);
    //  Return
    dd($image404);
}