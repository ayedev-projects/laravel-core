<?php

//  Error Reporting
error_reporting(0);
ini_set('display_errors', 'Off');

//  Image Directory
define('IMAGE_EXT', 'jpg|jpeg|JPEG|png|gif');
define('CACHE_DIR', str_ireplace('\\', '/', dirname(__FILE__)) . '/');


//  Directories
$dirs = array(
    str_ireplace( '\\', '/', dirname( dirname( __FILE__ ) ) ) . '/uploads/',
    str_ireplace( '\\', '/', dirname( dirname( __FILE__ ) ) ) . '/assets/images/'
);

//  Get the Request
$request = filter_input(INPUT_GET, 'request');

//  Path Info
$pathinfo = pathinfo($request);

//  Check
if(!$request || !$pathinfo || !in_array(@$pathinfo['extension'], explode('|', IMAGE_EXT)))  return OnFlyResizer::no_image();

//  Extract Info Attempt #1
$match1 = null;
preg_match("/(?<filename>.*)\-(?<width>[0-9]+)(x)?(?<height>[0-9]+)?\.(?<extension>(" . IMAGE_EXT . "))/i", basename($request), $match1);

//  Extract Info Attempt #2
$match2 = null;
if(!$match1)    preg_match("/(?<filename>.*)\.(?<extension>(" . IMAGE_EXT . "))/i", basename($request), $match2);

//  Get Match
$match = ($match1 ?: $match2);

//  Filename
$filename = ($match ? $match['filename'] : null);

//  Explode
$explodes = explode( '__', $filename );

//  Config
$config = array(
    'fit_size' => false,
    'round' => false,
    'strict' => false,
    'wprimary' => false,
    'hprimary' => false,
    'width' => null,
    'height' => null,
    'quality' => 65
);

//  Config Str
$configStr = ( sizeof( $explodes ) > 1 ? end( $explodes ) : null );

//  Check
if( $configStr )
{
    //  Trim
    $filename = substr( $filename, 0, - ( strlen( $configStr ) + 2 ) );

    //  Decode
    $configStr = base64_decode( $configStr );

    //  Explode Again
    $explodes2 = explode( '|', $configStr );

    //  Loop Each
    foreach( $explodes2  as $exp )
    {
        //  Check for Fit
        if( $exp == 'fit' ) $config['fit_size'] = true;

        //  Check for Round
        else if( $exp == 'round' )  $config['round'] = true;

        //  Check for Strict
        else if( $exp == 'strict' )  $config['strict'] = true;

        //  Check for Width Primary
        else if( $exp == 'wprimary' )  $config['round'] = true;

        //  Check for Height Primary
        else if( $exp == 'hprimary' )  $config['hprimary'] = true;

        //  Check for Quality
        else if( preg_match( '/\q(?<quality>[0-9]+)/i', $exp, $qMatch ) )
        {
            //  Set Quality
            $config['quality'] = $qMatch['quality'];
        }

        //  Check Dimensions
        else if( preg_match( '/(?<width>[0-9]+)(x)?(?<height>[0-9]+)/i', $exp, $dimMatch ) )
        {
            //  Set Width
            $config['width'] = $dimMatch['width'];

            //  Set Height
            $config['height'] = ( isset( $dimMatch['height'] ) ? $dimMatch['height'] : $config['width'] );
        }
    }
}

//  Source Image
$sourceImage = null;

//  Loop Each
foreach( $dirs as $dir ) {

    //  File
    $tFile = $dir . ( isset( $pathinfo['dirname'] ) && $pathinfo['dirname'] != '.' ? $pathinfo['dirname'] . '/' : '' ) . $filename . '.' . $match['extension'];

    //  Check
    if( $match && file_exists( $tFile ) ) {

        //  Set Source Image
        $sourceImage = $tFile;
        break;
    }
}

//  Check
if(!$sourceImage || !file_exists($sourceImage))  return OnFlyResizer::no_image($config['width'], $config['height']);

//  Check
//if(!$config['width'] && !$config['height'])    return OnFlyResizer::return_image($sourceImage, $match['extension']);

//  Return
return new OnFlyResizer( $request, $match['extension'], $sourceImage, $config['width'], $config['height'], $config['fit_size'], $config['round'], $config['strict'], $config['wprimary'], $config['hprimary'], $config['quality'] );


//  CLASS DECLARATION
class OnFlyResizer {

    //  Construct
    function __construct($filepath, $extension, $sourceImage, $targetWidth, $targetHeight, $fitSize = false, $createCircle = false, $strictSize = false, $widthPrimary = false, $heightPrimary = false, $quality = null) {

        //  Create Function Name
        $fnName = self::create_image_func($extension);

        //  Create Image
        $image = $fnName($sourceImage);

        //  Get width and height
        $sourceWidth  = imagesx($image);
        $sourceHeight = imagesy($image);

        //  Return
        return self::return_image($image, $extension, $filepath, function($img, $extension) use ($sourceWidth, $sourceHeight, $targetWidth, $targetHeight, $fitSize, $createCircle, $strictSize, $widthPrimary, $heightPrimary) {

            //  Check
            if( !$targetWidth || !$targetHeight )   return $img;

            //  Check
            if($targetWidth > $sourceWidth)   $targetWidth = $sourceWidth;
            if($targetHeight > $sourceHeight)   $targetHeight = $sourceHeight;

            //  Calculate Ratio
            $sourceRatio = $sourceWidth / $sourceHeight;
            $targetRatio = $targetWidth / $targetHeight;

            //  Calculate the Scale
            if ( $sourceRatio < $targetRatio ) {
                $scale = $sourceWidth / $targetWidth;
            } else {
                $scale = $sourceHeight / $targetHeight;
            }

            //  New Dimensions
            $resizeWidth = (int)($sourceWidth / $scale);
            $resizeHeight = (int)($sourceHeight / $scale);

            //  Crop Positions
            $cropLeft = 0;
            $cropTop = 0;

            //  Check for Fit Size
            if($fitSize) {

                //  Set Width
                $resizeWidth = $targetWidth;
                $resizeHeight = $targetHeight;

                //  Change Size
                $sourceWidth = $resizeWidth;
                $sourceHeight = $resizeHeight;
            }
            else if($widthPrimary && $resizeWidth < $resizeHeight) {

                //  Set the Width
                $resizeWidth = $targetWidth;
            }
            else if($heightPrimary && $resizeHeight < $resizeWidth) {

                //  Set the Height
                $resizeHeight = $targetHeight;
            }

            //  Check Resize Dimensions
            if($sourceWidth < $cropLeft)    $cropLeft = 0;
            if($sourceHeight < $cropTop)    $cropTop = 0;

            //  New Image
            $newImg = imagecreatetruecolor($resizeWidth, $resizeHeight);

            //  Preserve Image
            OnFlyResizer::preserve_image($newImg, $extension);

            //  Check Strict
            if($strictSize) {

                //  Copy Image
                imagecopyresampled($newImg, $img, 0, 0, 0, 0, $targetWidth, $targetHeight, $sourceWidth, $sourceHeight);
            } else {

                //  Copy Image
                imagecopyresampled($newImg, $img, 0, 0, 0, 0, $resizeWidth, $resizeHeight, $sourceWidth, $sourceHeight);
            }

            //  Check
            if($createCircle) {

                //  Create masking
                $mask = imagecreatetruecolor($resizeWidth, $resizeHeight);

                //  Allocate Transparent Bit
                $transparent = imagecolorallocate($mask, 255, 0, 0);

                //  Fill
                imagecolortransparent($mask, $transparent);

                //  Fill
                imagefilledellipse($mask, $resizeWidth / 2, $resizeWidth / 2, $resizeWidth, $resizeWidth, $transparent);

                //  Allocate Red
                $red = imagecolorallocate($mask, 0, 0, 0);

                //  Copy
                imagecopymerge($img, $mask, 0, 0, 0, 0, $resizeWidth, $resizeHeight, 100);

                //  Set Transparent
                imagecolortransparent($img, $red);

                //  Fill Image
                imagefill($img, 0, 0, $red);

                //  Destroy Mask
                imagedestroy($mask);
            }

            //  Destroy Image
            imagedestroy($img);

            //  Return
            return $newImg;
        }, $quality);
    }


    //  Create No Image Snap
    public static function no_image($width = 250, $height = 250) {

        //  Return
        return self::return_image(imagecreatetruecolor($width, $height), 'png', null, function($img) use ($width, $height) {

            //  Font Size
            $fontSize = 12;

            //  Check
            if($width <= 200)    $fontSize = 7;
            if($width <= 100)    $fontSize = 5;

            //  Fill Background
            imagefill($img, 0, 0, 0x00DADADA);

            //  Write the Text
            imagettftext($img, $fontSize, 0, (($width / 2) * 0.38), ($height / 2), 0x00888888, "font.ttf", 'No image');
            imagettftext($img, $fontSize, 0, (($width / 2) * 0.5), (($height / 2) * 1.4), 0x00888888, "font.ttf", 'found');

            //  Return
            return $img;
        });
    }

    //  Return the Source Image
    public static function return_image($path, $extension, $saveAs = null, $callback = null, $quality = null) {

        //  Fix Quality
        $quality || $quality = 65;

        //  Create Function Name
        $fnName = self::create_image_func($extension);

        //  Render Function Name
        $fnName2 = self::render_image_func($extension);

        //  Create Image
        $img = (is_resource($path) ? $path : $fnName($path));

        //  Check
        if($callback)   $img = call_user_func_array($callback, array($img, $extension));

        //  Preserve Image
        self::preserve_image($img, $extension);

        //  Set Header
        header("Content-Type: " . self::image_header_content_type($extension), true);

        //  Check
        if($saveAs) {

            //  Dirname
            $dirname = dirname(CACHE_DIR . $saveAs) . '/';

            //  Check
            if(!file_exists($dirname))  mkdir($dirname, 0777, true);
        }

        //  Check
        if($extension == 'png') {

            // *** Scale quality from 0-100 to 0-9
            $scaleQuality = round( ( $quality / 100) * 9 );

            // *** Invert quality setting as 0 is best, not 9
            $quality = 9 - $scaleQuality;
        }

        //  Display Image
        $fnName2($img, $saveAs, $quality);

        //  Check
        if($saveAs) {

            //  Send to Browser Again
            $fnName2($img, null, $quality);
        }

        //  Destroy Image
        imagedestroy($img);
    }

    //  Preserve Image
    public static function preserve_image($img, $extension) {

        //  Check
        if(in_array($extension, array('png', 'gif'))) {

            //  Preserve Transparency
            imagealphablending( $img, false );
            imagesavealpha( $img, true );
        }

        //  Return
        return $img;
    }

    //  Detect Content Type for Image
    public static function image_header_content_type($extension) {

        //  Return
        return 'image/' . ($extension == 'jpg' ? 'jpeg' : $extension);
    }

    //  Detect Create Image Function
    public static function create_image_func($extension) {

        //  Create Function Name
        $fnName = 'imagecreatefromjpeg';
        if($extension == 'png') $fnName = 'imagecreatefrompng';
        else if($extension == 'gif')    $fnName = 'imagecreatefromgif';

        //  Return
        return $fnName;
    }

    //  Detect Create Image Function
    public static function render_image_func($extension) {

        //  Create Function Name
        $fnName = 'imagejpeg';
        if($extension == 'png') $fnName = 'imagepng';
        else if($extension == 'gif')    $fnName = 'imagegif';

        //  Return
        return $fnName;
    }
}