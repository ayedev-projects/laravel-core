<?php

//  Return
return array(

    //  Validation Image Types
    'image_types' => array( 'jpeg', 'jpg', 'png', 'gif' ),

    //  Image Extensions
    'image_exts' => array( 'jpg', 'jpeg', 'JPEG', 'png', 'gif' ),

    //  Site Title
    'site_title' => 'Laravel Core',

    //  Site Title Seperator
    'site_title_sep' => ' | ',

    //  Assets Dir
    'assets_dir' => array(

        //  Base Directory
        'base' => 'assets',

        //  Images Dir
        'image' => 'images',

        //  Styles Dir
        'style' => 'styles',

        //  Scripts Dir
        'script' => 'scripts',

        //  Plugins Dir
        'plugin' => 'plugins',

        //  Uploads Dir
        'upload' => 'uploads',

        //  Cache Dir
        'cache' => 'cache'
    ),

    //  Image Cache Dir
    'image_cache_dir' => storage_path( 'app/public/image' ),

    //  Placeholder Image
    'placeholder_image' => 'no_image.jpg'
);