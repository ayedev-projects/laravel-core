<?php

//  Add Attachment Image Validation
Validator::extend( 'image', function( $attribute, $file, $parameters, $validator )
{
    //  Check
    $files = ( $file ? ( is_array( $file ) ? $file : array( $file ) ) : null );

    //  Valid Extensions
    $valid_exts = array( 'jpeg', 'jpg', 'png', 'gif' );

    //  Is Valid
    $isValid = !is_null( $files );

    //  Check
    if( $isValid ) {

        //  Loop
        foreach( $files as $file ) {

            //  Check
            if( is_null( $file ) || $file->getError() == UPLOAD_ERR_NO_FILE || !in_array( $file->getClientOriginalExtension(), $valid_exts ) ) {

                //  Set Not Valid
                $isValid = false;
            }
        }
    }

    //  Return
    return $isValid;
} );


//  Add Directive for Fire Event
Blade::directive( 'fire', function( $arg )
{
    //  Return
    return '<?php ' .
    '$theArgs = array(' . $arg . ');' .
    '$eventName = array_shift( $theArgs );' .
    'fire( $eventName, $theArgs );' .
    '?>';
} );

//  Add Directive for Setter
Blade::directive( 'set', function( $arg )
{
    //  Return
    return '<?php ' .
    '$theArgs = array(' . $arg . ');' .
    '$__theKey = array_shift( $theArgs );' .
    '$__theValue = array_shift( $theArgs );' .
    '$$__theKey = $__theValue;' .
    'View::share( $__theKey, $__theValue );' .
    '?>';
} );

//  Add Directive for PHP Run Code
Blade::directive( 'runphp', function( $arg )
{
    //  Return
    return '<?php ' .
    '$theArgs = array(' . $arg . ');' .
    '$__compiledOutput = array_shift( $theArgs );' .
    '?>';
} );

//  Add Directive for PHP Code
Blade::directive( 'php', function()
{
    //  Return
    return '<?php ob_start(); ?>';
} );

//  Add Directive for End PHP Code
Blade::directive( 'endphp', function()
{
    //  Return
    return '<?php $contents = ob_get_clean(); eval( $contents ); ?>';
} );

//  Add Directive for Language Block
Blade::directive( 'block', function( $arg )
{
    //  Return
    return '<?php ' .
    '$theArgs = array(' . $arg . ');' .
    '$lang_block_file = trans_block_file( $theArgs[0], null, @$theArgs[1] );' .
    'if( $lang_block_file && file_exists( $lang_block_file ) )  include $lang_block_file;' .
    '?>';
} );


//  Load the App Start File
if( file_exists( $appStartFile = app_path( '__static/boot.php' ) ) )
    require_once $appStartFile;
