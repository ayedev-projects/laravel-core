<?php

//  Message Keys
define( 'ALERT_MSG_SUCCESS', '_alert_success_' );
define( 'ALERT_MSG_DANGER', '_alert_danger_' );
define( 'ALERT_MSG_WARNING', '_alert_warning_' );
define( 'ALERT_MSG_INFO', '_alert_info_' );

//  Priorities
define( 'PRIORITY_HIGH', 1 );
define( 'PRIORITY_MEDIUM', 2 );
define( 'PRIORITY_NORMAL', 3 );
define( 'PRIORITY_OK', 4 );
define( 'PRIORITY_LOW', 5 );

//  Add Session Message
function addMessage( \Illuminate\Http\RedirectResponse $redirectResponse, $type, $message, $points = array() ) {

    //  Detect Title
    $title = ( is_string( $message ) ? $message : ( is_array( $message ) ? $message[0] : null ) );

    //  Detect Points
    if( is_array( $message ) ) {

        //  Remove First
        unset( $message[0] );

        //  Merge to Points
        $points = array_merge( array_values( $message ), ( $points ? $points : array() ) );
    }

    //  Return
    return $redirectResponse->with( $type, ( $points ? array( 'title' => $title, 'points' => $points ) : $title ) );
}

//  Add Success Session Message
function addSuccessMessage( $redirectResponse, $message, $points = array() ) {

    //  Return
    return addMessage( $redirectResponse, ALERT_MSG_SUCCESS, $message, $points );
}

//  Add Error Session Message
function addErrorMessage( $redirectResponse, $message, $points = array() ) {

    //  Return
    return addMessage( $redirectResponse, ALERT_MSG_DANGER, $message, $points );
}

//  Add Warning Session Message
function addWarningMessage( $redirectResponse, $message, $points = array() ) {

    //  Return
    return addMessage( $redirectResponse, ALERT_MSG_WARNING, $message, $points );
}

//  Add Info Session Message
function addInfoMessage( $redirectResponse, $message, $points = array() ) {

    //  Return
    return addMessage( $redirectResponse, ALERT_MSG_INFO, $message, $points );
}

//  Check for Session Message
function hasMessage( $type ) {

    //  Return
    return ( session( $type ) != null );
}

//  Check for Success Session Message
function hasSuccessMessage() {

    //  Return
    return hasMessage( ALERT_MSG_SUCCESS );
}

//  Check for Error Session Message
function hasErrorMessage() {

    //  Return
    return hasMessage( ALERT_MSG_DANGER );
}

//  Check for Warning Session Message
function hasWarningMessage() {

    //  Return
    return hasMessage( ALERT_MSG_WARNING );
}

//  Check for Info Session Message
function hasInfoMessage() {

    //  Return
    return hasMessage( ALERT_MSG_INFO );
}

//  Check has Any Messages
function hasAnyMessage()
{
    //  Return
    return ( hasSuccessMessage() || hasErrorMessage() || hasWarningMessage() || hasInfoMessage() || hasValidatorErrors() );
}

//  Prepare Session Message
function prepareMessage( $type ) {

    //  Get Message
    $message = session( $type );

    //  Check
    if( $message ) {

        //  Detect the Properties
        $title = ( is_string( $message ) ? $message : $message['title'] );
        $points = ( is_array( $message ) ? $message['points'] : array() );

        //  Return
        return '<span class="message-bag-title">' . $title . '</span>' . ( $points ? '<ul><li>' . implode( '</li><li>', $points ) . '</li></ul>' : '' );
    }

    //  Return
    return '';
}

//  Prepare Success Session Message
function prepareSuccessMessage() {

    //  Prepare Message
    return prepareMessage( ALERT_MSG_SUCCESS );
}

//  Prepare Error Session Message
function prepareErrorMessage() {

    //  Prepare Message
    return prepareMessage( ALERT_MSG_DANGER );
}

//  Prepare Warning Session Message
function prepareWarningMessage() {

    //  Prepare Message
    return prepareMessage( ALERT_MSG_WARNING );
}

//  Prepare Info Session Message
function prepareInfoMessage() {

    //  Prepare Message
    return prepareMessage( ALERT_MSG_INFO );
}

/**
 * Get Errors Bag
 *
 * @param null $key
 * @return \Illuminate\Contracts\Support\MessageBag
 */
function getErrorsBag( $key = null ) {

    //  Key
    $key || $key = 'default';

    //  Get the Errors Bag
    $errorsBag = session('errors');

    //  Return
    return ( $errorsBag ? $errorsBag->getBag( $key ) : null );
}

//  Check for Validation Errors
function hasValidatorErrors( $key = null ) {

    //  Get Bag
    $bag = getErrorsBag( $key );

    //  Return
    return ( $bag && !$bag->isEmpty() );
}

//  Prepare Validate Errors
function prepareValidatorErrors( $key = null, $title = null ) {

    //  Return
    return processErrorsBag( getErrorsBag( $key ), $title );
}

//  Process the Errors Bag
function processErrorsBag( $bag, $title = null )
{
    //  Title
    $title = $title ?: 'Please fix the following errors:';

    //  Check
    if( $bag && !$bag->isEmpty() ) {

        //  Return
        return '<span class="message-bag-title">' . $title . '</span><ul><li>' . implode( '</li><li>', $bag->all() ) . '</li></ul>';
    }

    //  Return
    return '';
}


//  Check Session is Ready
function sessionIsReady()
{
    //  Return
    return app( 'request' )->hasSession();
}

//  Check Logged In
function isLoggedIn()
{
    //  Return
    return ( sessionIsReady() ? \Auth::check() : false );
}

//  Get Current User ID
function currentUserID()
{
    //  Return
    return ( isLoggedIn() ? user()->getID() : null );
}

/**
 * Get Current User
 *
 * @return \App\Model\UserModel
 */
function user() {

    //  Return
    return ( isLoggedIn() ? \Auth::user() : null );
}

//  Get the Current Token for Email
function passwordResetToken( $email )
{
    //  Search
    $record = DB::table( config( 'auth.passwords.' . config( 'auth.defaults.passwords' ) . '.table' ) )
        ->where( 'email', '=', $email )
        ->select( 'token' )
        ->first();

    //  Return
    return ( $record ? $record->token : null );
}

//  Get the User by Password Reset Token
function userByPasswordResetToken( $token )
{
    //  Search
    $record = DB::table( config( 'auth.passwords.' . config( 'auth.defaults.passwords' ) . '.table' ) )
        ->where( 'token', '=', $token )
        ->select( 'email' )
        ->first();

    //  Return
    return ( $record ? \App\Model\UserModel::whereEmailAddress( $record->email )->first() : null );
}

//  Get the User by Password Reset Token using Email
function userByPasswordResetTokenUsingEmail( $email )
{
    //  Get Token
    $token = passwordResetToken( $email );

    //  Return
    return ( $token ? userByPasswordResetToken( $token ) : null );
}

//  Set User Context
function set_user_context( $user_id = null )
{
    //  User ID
    if( !$user_id && isLoggedIn() ) $user_id = currentUserID();

    //  Check
    if( $user_id )  getTS()->writeStorage( 'context_user', $user_id );
}

//  Get User Context
function user_context()
{
    //  Return
    return getTS()->readStorage( 'context_user' );
}