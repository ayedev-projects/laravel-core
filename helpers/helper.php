<?php

//  Define Constants
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Artisan;


//  Make the Closure
function closure( $closure, $bindTo = null, $no_instance = false )
{
    //  Closure
    $closure = \Ayedev\LaravelCore\Core\Closure::make( $closure, $bindTo );

    //  Return
    return ( $no_instance ? $closure->getClosure() : $closure );
}

//  Make Plain Closure
function plain_closure( $closure, $bindTo = null )
{
    //  Return
    return closure( $closure, $bindTo, true );
}

//  Share Variable to View
function shareVar( $key, $value )
{
    //  Global
    global $shared_data;

    //  Check
    if( !$shared_data ) $shared_data = array();

    //  Store
    $shared_data[$key] = $value;

    //  Share to View
    view()->share( $key, $value );
}

//  Read Shared Var
function readSharedVar( $key, $def = null )
{
    //  Global
    global $shared_data;

    //  Check
    if( !$shared_data ) $shared_data = array();

    //  Return
    return ( isset( $shared_data[$key] ) ? $shared_data[$key] : $def );
}

//  Check Has Shared Var
function hasSharedVar( $key )
{
    //  Global
    global $shared_data;

    //  Check
    if( !$shared_data ) $shared_data = array();

    //  Return
    return ( isset( $shared_data[$key] ) );
}

//  Generate Color Code for Text
function genColorCodeFromText($text, $min_brightness = 100,$spec = 10 ) {

    // Check inputs
    if(!is_int($min_brightness)) throw new Exception("$min_brightness is not an integer");
    if(!is_int($spec)) throw new Exception("$spec is not an integer");
    if($spec < 2 or $spec > 10) throw new Exception("$spec is out of range");
    if($min_brightness < 0 or $min_brightness > 255) throw new Exception("$min_brightness is out of range");


    $hash = md5($text);  //Gen hash of text
    $colors = array();
    for($i=0;$i<3;$i++)
        $colors[$i] = max(array(round(((hexdec(substr($hash,$spec*$i,$spec)))/hexdec(str_pad('',$spec,'F')))*255),$min_brightness)); //convert hash into 3 decimal values between 0 and 255

    if($min_brightness > 0)  //only check brightness requirements if min_brightness is about 100
        while( array_sum($colors)/3 < $min_brightness )  //loop until brightness is above or equal to min_brightness
            for($i=0;$i<3;$i++)
                $colors[$i] += 10;	//increase each color by 10

    $output = '';

    for($i=0;$i<3;$i++)
        $output .= str_pad(dechex($colors[$i]),2,0,STR_PAD_LEFT);  //convert each color to hex and append to output

    return '#'.$output;
}

//  Make Placeholder Image
function makePlaceholderImage( $text, $bgColor = null, $textColor = null, $short_text = true, $fontSize = null, $width = null, $height = null ) {

    //  Fix Arguments
    $width || $width = 96;
    $height || $height = 96;
    $fontSize || $fontSize = 22;
    $bgColor || $bgColor = '000000';
    $textColor || $textColor = 'FFFFFF';

    //  Txt
    $txt = $text;

    //  Check
    if( $short_text ) {

        //  Explode
        $explodes = explode( ' ', $txt, 2 );

        //  Check
        if( sizeof( $explodes ) > 1 ) {

            //  Get Chars
            $txt = substr( $explodes[0], 0, 1 ) . substr( $explodes[1], 0, 1 );
        } else {

            //  Get First 2 Chars
            $txt = substr( $txt, 0, 2 );
        }

        //  Transform
        $txt = strtoupper( $txt );
    }

    //  Fix Color Codes
    if( substr( $bgColor, 0, 1 ) == '#' )   $bgColor = substr( $bgColor, 1 );
    if( substr( $textColor, 0, 1 ) == '#' )   $textColor = substr( $textColor, 1 );

    //  Return
    return "https://placeholdit.imgix.net/~text?txtsize={$fontSize}&bg={$bgColor}&txtclr={$textColor}&txt=" . urlencode( $txt ) . "&w={$width}&h={$height}";
}

//  Fix the List if Empty
function fixListIfEmpty( $list, $append_to_empty = true, $append = '-1' ) {

    //  Check
    if( $list === false || is_null( $list ) || empty( $list ) ) $list = array();

    //  Check
    if( !is_array( $list ) )    $list = array( $list );

    //  Check
    if( sizeof( $list ) == 0 && $append_to_empty )  $list[] = $append;

    //  Return
    return $list;
}

/** Abort Page **/
function abort_page( $code = null, $msg = null ) {

    //  Code
    $code || $code = 403;
    $msg || $msg = 'Unknown application error occurred.';

    //  Abort Application
    abort( $code, $msg );
}

/** Get the Section **/
function getSection( $name, $def = null ) {

    //  Return
    return ( View::hasSection( $name ) ? View::yieldContent( $name ) : $def );
}

/** Make Plain Message from HTML **/
function makePlainMessage( $msg = null, $key = 'mail_message' ) {

    //  Plain Message
    $plain_msg = strip_tags( nl2br( getSection( $key, $msg ) ) );

    //  Return
    return $plain_msg;
}

/** Get Input Value **/
function input( $key, $def = null )
{
    //  Val
    $val = app( 'request' )->get( $key );

    //  Return
    return ( $val ?: $def );
}

/** Get Input with Old Value **/
function input_old( $key, $def = null )
{
    //  Return
    return ( app( 'request' )->hasSession() ? input( $key, old( $key, $def ) ) : $def );
}

/** Get Input with Old Value - Array **/
function input_old_array( $key, $def = null ) {

    //  Data
    $data = input_old( $key, $def );

    //  Return
    return ( $data ? ( is_array( $data ) ? $data : array( $data ) ) : array() );
}

/**
 * Get Current Route
 *
 * @return \Illuminate\Routing\Route
 */
function currentRoute() {

    //  Return
    return app( 'router' )->getCurrentRoute();
}

/** Get Route Name **/
function routeName() {

    //  Return
    return ( currentRoute() ? currentRoute()->getName() : null );
}

/** Get Route Param **/
function routeParam( $key, $def = null ) {

    //  Return
    return ( currentRoute() ? currentRoute()->getParameter( $key, $def ) : $def );
}

/** Get Current Slug **/
function currentSlug( $key = 'slug' ) {

    //  Return
    return routeParam( $key );
}

/**
 * Get Breadcrumb
 *
 * @return \Ayedev\LaravelCore\Extension\Breadcrumb
 */
function breadcrumb() {

    //  Return
    return \Ayedev\LaravelCore\Extension\Breadcrumb::getInstance();
}

/** Extract Opengraph Info **/
function extract_og_info( $url, $opts = array() ) {

    //  Data
    $data = null;

    //  Try
    try {

        //  Data
        $data = Embed\Embed::create( $url, $opts );
    }
    catch( Exception $e ) {}

    //  Return
    return $data;
}

/** Extract Opengraph Info - Cached **/
function extract_og_info_cached( $url, $opts = array() ) {

    //  Lifetime
    $lifetime = array_get( $opts, 'lifetime', 240 );

    //  Forget
    array_forget( $opts, 'lifetime' );

    //  Cache Key
    $cache_key = 'og_data_' . str_ireplace( '=', '', base64_encode( $url ) );

    //  Return
    return app('cache')->remember( $cache_key, $lifetime, function()use ( $url, $opts ) {
        return extract_og_info( $url, $opts );
    } );
}

/** Opengraph Data Value **/
function og_info( $url, $key = null, $cache = true, $opts = array() ) {

    //  Info
    $info = call_user_func_array( 'extract_og_info'  . ( $cache ? '_cached' : '' ), array( $url, $opts ) );

    //  Return
    return ( $key ? ( $info ? $info->{$key} : null ) : $info );
}

/** Get Default Timezone **/
function default_timezone( $tz = null )
{
    //  Apply Filters
    if( !$tz )  $tz = apply_filters( 'default_timezone', $tz );

    //  Return
    return ( $tz ?: env( 'APP_TZ', config( 'settings.timezone' ) ) );
}

/**
 * Carbon Time Now
 *
 * @param null $tzName
 * @return \Carbon\Carbon
 */
function carbon_now( $tzName = null ) {

    //  Return
    return \Carbon\Carbon::now( default_timezone( $tzName ) );
}

/**
 * Create Carbon Object
 *
 * @param $time
 * @return \Carbon\Carbon
 */
function carbon( $time, $format = null ) {

    //  Return
    return ( !$format && is_int( $time ) ? \Carbon\Carbon::createFromTimestamp( $time ) : ( $format ? \Carbon\Carbon::createFromFormat( $format, $time ) : \Carbon\Carbon::parse( $time ) ) );
}

/** Mock Request **/
function mock_request( $uri, $method = null, $parameters = null, $cookies = null, $files = null, $server = null, $content = null ) {

    //  Method
    $method || $method = \Illuminate\Support\Facades\Request::method();

    //  Parameters
    $parameters || $parameters = \Illuminate\Support\Facades\Input::all();

    //  Cookies
    $cookies || $cookies = \Illuminate\Support\Facades\Request::cookie();

    //  Files
    $files || $files = \Illuminate\Support\Facades\Input::file();

    //  Server
    $server || $server = \Illuminate\Support\Facades\Request::server();

    //  Return
    return \Illuminate\Support\Facades\Request::create( $uri, $method, $parameters, $cookies, $files, $server, $content );
}

//  Recursive Remove Directory and Files
function rrmdir($dir) {

    //  Check is Dir
    if (is_dir($dir)) {

        //  Get the Objects
        $objects = scandir($dir);

        //  Loop Objects
        foreach ($objects as $object) {

            //  Check
            if ($object != "." && $object != "..") {

                //  Remove
                if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
            }
        }

        //  Reset Objects
        reset($objects);

        //  Remove Directory
        rmdir($dir);
    }
}

/**
 * Return Temporary Storage Manager
 *
 * @param string $name
 * @return \Ayedev\LaravelCore\Extension\TemporaryStorage
 */
function getTS( $name = 'default' ) {
    return \Ayedev\LaravelCore\Extension\TemporaryStorage::getInstance( $name );
}

/**
 * Return Worker Helper
 * @param $obj
 * @param array $params
 * @return \Ayedev\LaravelCore\Extension\Worker
 */
function worker( $obj, $params = array() ) {
    return new \Ayedev\LaravelCore\Extension\Worker( $obj, $params );
}

/**
 * Return Assets Manager
 *
 * @return \Ayedev\LaravelCore\Extension\AssetsManager
 */
function assets() {
    return \Ayedev\LaravelCore\Extension\AssetsManager::instance();
}

/**
 * Return Navigation Manager
 *
 * @param null $type
 * @return \Ayedev\LaravelCore\Extension\NavigationManager
 */
function navigation( $type = null ) {
    return \Ayedev\LaravelCore\Extension\NavigationManager::instance( $type );
}

//  Add Filter
function add_filter($filter, $callback, $priority = 10) {

    //  Work with Temporary Storage
    getTS()->start('system_filters')
        ->work(function($system_filters, $instance) use($filter, $callback, $priority) {

            //  Validate Filter Subkey
            $instance->validateSubKey($system_filters, $filter);

            //  Validate Priority Subkey
            $instance->validateSubKey($system_filters[$filter], $priority);

            //  Add the Filter
            $system_filters[$filter][$priority][] = $callback;

            //  Return
            return $system_filters;
        })->stop();
}

//  Apply Filters
function apply_filters($filter, $value) {

    //  Get the Filters
    $filters = getTS()->getStorageSub('system_filters', $filter);

    //  Sort
    ksort($filters);

    //  Arguments
    $arguments = func_get_args();
    array_shift($arguments);
    array_shift($arguments);

    //  Loop Each
    foreach($filters as $theFilters) {

        //  Loop Each Filters
        foreach($theFilters as $theFilter) {

            //  Run Callback
            $value = call_user_func_array($theFilter, array_merge(array($value), $arguments));
        }
    }

    //  Return
    return $value;
}

//  Get Current URL
function current_url() {
    return \Illuminate\Support\Facades\URL::current();
}

//  Generate GUID
function generate_guid() {

    //  GUID
    $guid = '';

    //  Check Native Function Exists
    if (function_exists('com_create_guid')){

        //  Generate
        $guid = com_create_guid();
    } else {

        //  optional for php 4.2.0 and up.
        mt_srand((double)microtime()*10000);

        //  Char
        $charid = strtoupper(md5(uniqid(rand(), true)));

        //  Hyphen
        $hyphen = chr(45);// "-"

        //  Generate
        $guid = chr(123)// "{"
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12)
            .chr(125);// "}"
    }

    //  Trim the Parenthesis
    $guid = ltrim(rtrim($guid, '}'), '{');

    //  Return
    return $guid;
}

//  Capitalize Keyword
function capitalizeKeyword($str) {
    return ucwords(implode(' ', explode('_', $str)));
}

//  Merge Array Recursive
function mergeArrayRecursive() {

    //  Args
    $args = func_get_args();

    //  Source
    $source = array_shift($args);

    //  Loop Each
    foreach($args as $arg) {

        //  Loop Each
        foreach($source as $key => $val) {

            //  Check
            if(is_array($val) || is_object($val)) {

                //  Merge
                $source[$key] = (isset($arg[$key]) ? mergeArrayRecursive($val, $arg) : $val);
            } else {

                //  Set
                $source[$key] = (isset($arg[$key]) ? $arg[$key] : $val);
            }
        }

        //  Loop Each Arg
        foreach($arg as $aKey => $aVal) {

            //  Check
            if(!isset($source[$aKey]))  $source[$aKey] = $aVal;
        }
    }

    //  Return
    return $source;
}

//  Check Environment
function is_environment( $env )
{
    //  Return
    return ( app()->environment() == $env );
}

//  Recursive Remove Dir
function clear_dir( $dir, $remove_main = false, $ignore_top = false, $level = 0, $exclude = null ) {

    //  Fix Dir
    $dir = rtrim( $dir, '/' ) . '/';

    //  Check
    if( !file_exists( $dir ) )  return;

	//	Exclude List
	$excludeList = array( '.', '..' );

	//	Check for Exclude
	if( $exclude )	$excludeList = array_merge( $excludeList, $exclude );

    //  Open
    $hn = opendir( $dir );

    //  Read
    while( $file = readdir( $hn ) ) {

        //  Check
        if( !in_array( $file, $excludeList ) ) {

            //  Full Path
            $fullPath = $dir . $file;

            //  Check for Directory
            if( is_dir( $fullPath ) ) {

                //  Go Recursive
                clear_dir( $fullPath . '/', $remove_main, $ignore_top, $level + 1 );
            } else {

                //  Check
                if( $level > 0 || ( $level == 0 && !$ignore_top ) ) {

                    //  Remove File
                    unlink( $fullPath );
                }
            }
        }
    }

    //  Check
    if( $level > 0 || ( $level == 0 && $remove_main ) ) {

        //  Remove Directory
        @rmdir( $dir );
    }
}

//  Clear Cache
function clear_cache( $clear_config_view = true )
{
    //  Run Clear Commands
	if( $clear_config_view )   Artisan::call( 'config:clear' );
    if( $clear_config_view )    Artisan::call( 'view:clear' );
    Artisan::call( 'cache:clear' );

    //  Fire Event
    event( 'clear_cache' );

    //  Return
    return true;
}

//  Convert Array to File
function array_to_file( $array, $level = 1 )
{
    //  Output
    $output = '';

    //  Check
    if( $level == 1 )
    {
        //  Add Pre
        $output .= "<?php\n\n//\tUpdated at => " . carbon_now()->format( 'Y-m-d H:i:s A' ) . " from " . request()->ip() . "\nreturn array(";
    }

    //  Loop
    foreach( $array as $key => $val )
    {
        //  Check
        if( in_array( ( $sKey = substr( $key, 0, 2 ) ), array( '__', '//' ) ) )
        {
            //  Check
            if( $sKey == '__' )   $output .= "\n";
            else if( $sKey == '//' )   $output .= "\n" . str_repeat( "\t", $level ) . "//\t{$val}";
        }
        else if( is_array( $val ) )
        {
            //  Append
            $output .= "\n" . str_repeat( "\t", $level ) . "'{$key}' => array(" . array_to_file( $val, $level + 1 ) . "\n" . str_repeat( "\t", $level ) . "),";
        }
        else
        {
            //  Append
            $output .= "\n" . str_repeat( "\t", $level ) . "'{$key}' => " . ( is_null( $val ) ? 'null' : ( is_numeric( $val ) ? $val : "'{$val}'" ) ) . ",";
        }
    }

    //  Check
    if( $level == 1 )
    {
        //  Add Post
        $output .= "\n);";
    }

    //  Return
    return $output;
}

//  Check if Multi-Dimensional
function is_array_multi( $arr )
{
    //  Check
    if( !is_array( $arr ) ) return false;

    //  Loop
    foreach( $arr as $key => $val )
    {
        //  Check
        if( is_array( $val ) )
        {
            //  Return
            return true;
            break;
        }
    }

    //  Return
    return false;
}


//  Loop Other Helper Files
foreach( glob( dirname( __FILE__ ) . '/others/*.php' ) as $hFile )
{
    //  Load Other Helper
    require_once $hFile;
}

//  Load the App Helper
if( file_exists( $appHelperFile = app_path( 'helpers/helper.php' ) ) )
    require_once $appHelperFile;

//  Load the App Setup File
if( file_exists( $appSetupFile = app_path( 'helpers/setup.php' ) ) )
    require_once $appSetupFile;
