<?php

use Illuminate\Support\Facades\Input;

//  Make the Link from Current Query
function makeLinkCurrent( $data, $exclude = null ) {

    //  Params
    $params = array();

    //  Loop
    foreach( Input::query() as $key => $val ) {

        //  Check
        if( $exclude && in_array( $key, $exclude ) )    continue;

        //  Append
        $params[$key] = $val;
    }

    //  Return
    return current_url() . '?' . http_build_query( array_merge( $params, $data ) );
}

//  Current URL Excluding
function current_url_with_params( $exclude = null ) {

    //  Params
    $params = Input::query();

    //  Url
    $url = current_url();

    //  Check
    if( $exclude )  $exclude = explode( ',', $exclude );
    else    $exclude = array();

    //  Loop
    foreach( $exclude as $exc ) {

        //  Check
        if( isset( $params[$exc] ) ) {

            //  Remove
            unset( $params[$exc] );
        }
    }

    //  Return
    return $url . ( sizeof( $params ) > 0 ? '?' . http_build_query( $params ) : '' );
}

//  Current Path
function current_path($pageOnly = true) {

    //  Path
    $path = preg_replace('/^' . preg_quote( url( '/' ), '/') . '(\/.*|$)/', '\1', current_url());

    //  Explode
    $explodes = explode('?', $path);

    //  Return
    return ($pageOnly ? trim($explodes[0], '/') : ltrim($path, '/'));
}

//  Get the Relative Route Link
function rel_route( $name, $params = array() ) {

    //  URL
    $url = route( $name, $params );

    //  Return
    return substr( $url, strlen( url() ) );
}

//  Convert Dir to URL
function convertDirToURL( $path ) {

    //  Return
    return url( substr( $path, strlen( base_path( '' ) ) ) );
}

//  Set Moving Header
function set_moving_header()
{
    //  Set
    set_header_type( 'moving' );
}

//  Set Static Header
function set_static_header()
{
    //  Set
    set_header_type( 'static' );
}

//  Set Header Type
function set_header_type($type)
{
    //  Check
    if( $type == 'moving' ) assets()->addBodyClass( 'with-moving-header' );
    else    assets()->removeBodyClass( 'with-moving-header' );

    //  Set
    getTS()->writeStorage('page_header_type', $type);
}

//  Get Header Type
function get_header_type() {

    //  Return
    return getTS()->readStorage('page_header_type', 'static');
}

//  Check Moving Header Requested
function moving_header_requested()
{
    //  Return
    return ( get_header_type() == 'moving' );
}

//  Check Static Header Requested
function static_menu_requested()
{
    //  Return
    return ( get_header_type() == 'static' );
}

//  Set Page Type
function set_pagetype($type) {

    //  Set
    getTS()->writeStorage('page_type', $type);
}

//  Get Page Type
function get_pagetype() {

    //  Return
    return getTS()->readStorage('page_type');
}

//  Compare Page Type
function is_pagetype($type) {

    //  Return
    return (get_pagetype() == $type);
}

//  Set Page Group
function set_pagegroup($group) {

    //  Set
    getTS()->writeStorage('page_group', $group);
}

//  Get Page Group
function get_pagegroup() {

    //  Return
    return getTS()->readStorage('page_group');
}

//  Compare Page Group
function is_pagegroup($group) {

    //  Return
    return (get_pagegroup() == $group);
}


//  Fix the Link
function fix_the_link( $link )
{
    //  Return
    return ( in_array( trim( $link ), array( '', '#', 'javascript:void(0);' ) ) ? $link : ( stripos( $link, '://' ) > -1 ? $link : 'http://' . $link ) );
}

//  Prepare Link Text
function prepare_link_text( $link )
{
    //  Return
    return ( stripos( $link, '://' ) > -1 ? explode( '://', $link, 2 )[1] : $link );
}