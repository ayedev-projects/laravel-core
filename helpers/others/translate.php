<?php

//  Get the Translation Block File
function trans_block_file( $id = null, $parameters = [], $locale = null, $domain = null, $resolve = true )
{
    //  Prefix ID
    $id = 'blocks.' . $id;

    //  Fixes
    if( !is_array( $parameters ) )  $parameters = array();
    if( !$domain )  $domain = 'messages';

    //  Get Value
    $filename = trans( $id, $parameters, $domain, $locale );

    //  File
    $file = null;

    //  Get Block File
    $block_file = trans_file_strict( $filename, $locale, true );

    //  Check
    if( $resolve && file_exists( $block_file ) )
    {
        //  Blade Compiler
        $blade_compiler = new \Illuminate\View\Compilers\BladeCompiler( app( 'files' ), storage_path( 'framework/blocks_cache' ) );

        //  Compile
        $blade_compiler->compile( $block_file );

        //  Get Contents File
        $file = $blade_compiler->getCompiledPath( $block_file );
    }

    //  Check
    if( !$resolve ) $file = $block_file;

    //  Return
    return $file;
}

//  Strict Translation
function strict_trans_locale( $id, $locale, $parameters = [], $domain = 'messages' )
{
    //  Translator
    $translator = app( 'translator' );

    //  Get Fallback
    $fallback = $translator->getFallback();

    //  Change Fallback
    $translator->setFallback( $locale );

    //  Get Data
    $data = trans_locale( $id, $locale, $parameters, $domain );

    //  Revert Fallback
    $translator->setFallback( $fallback );

    //  Return
    return $data;
}

//  Translation Helper
function trans_locale( $id, $locale = null, $parameters = [], $domain = 'messages' )
{
    //  Return
    return trans( $id, $parameters, $domain, $locale );
}

//  Translation File
function trans_file( $name, $locale = null, $block = false )
{
    //  Get Locale
    if( !$locale )  $locale = app('translator')->getLocale();

    //  Return
    return resource_path( "lang" . DIRECTORY_SEPARATOR . $locale . DIRECTORY_SEPARATOR . ( $block ? 'blocks' . DIRECTORY_SEPARATOR : '' ) . "{$name}." . ( $block ? 'blade.' : '' ) . "php" );
}

//  Translations File with Fallback
function trans_file_strict( $name, $locale = null, $block = false )
{
    //  File
    $file = trans_file( $name, $locale, $block );

    //  Check
    if( !file_exists( $file ) ) $file = trans_file( $name, app( 'translator' )->getFallback(), $block );

    //  Return
    return $file;
}

//  Detect all Translations
function detect_all_translations()
{
    //  Detected List
    $list = array();

    //  Directory
    $dir = resource_path( 'lang' . DIRECTORY_SEPARATOR );

    //  Open Dir
    $hn = opendir( $dir );

    //  Loop
    while( $file = readdir( $hn ) )
    {
        //  File Path
        $file_path = $dir . $file . DIRECTORY_SEPARATOR;

        //  Check
        if( in_array( $file, array( '.', '..' ) ) || !is_dir( $file_path )  )    continue;

        //  Scan for Language Files
        $files = glob( $file_path . '*.php' );

        //  Scan for Block Files
        $block_files = glob( $file_path . 'blocks' . DIRECTORY_SEPARATOR . '*.blade.php' );

        //  Filter Files
        array_walk( $files, function( &$path )
        {
            //  Get Basename Only
            $path = pathinfo( $path, PATHINFO_FILENAME );
        } );

        //  Filter Block Files
        array_walk( $block_files, function( &$path )
        {
            //  Get Basename Only
            $path = str_ireplace( '.blade', '', pathinfo( $path, PATHINFO_FILENAME ) );
        } );

        //  Remove Blocks from List
        if( ( $index = array_search( 'blocks', $files ) ) > -1 )
        {
            //  Remove
            unset( $files[$index] );
        }

        //  Store Info
        $list[] = array(
            'lang' => $file,
            'code' => get_lang_code( $file ),
            'label' => get_lang_label( $file ),
            'path' => $file_path,
            'blocks_path' => $file_path . 'blocks',
            'lang_files' => $files,
            'block_files' => $block_files
        );
    }

    //  Close Dir
    closedir( $hn );

    //  Return
    return $list;
}

//  Save the Translation File
function save_trans( $name, $data, $locale = null )
{
    //  Get Locale
    if( !$locale )  $locale = app('translator')->getLocale();

    //  Output File
    $file = trans_file( $name, $locale );

    //  File Dir
    $file_dir = dirname( $file );

    //  Check
    if( !file_exists( $file_dir ) )   mkdir( $file_dir, 0777, true );

    //  Save Output
    return file_put_contents( $file, array_to_file( $data ) );
}

//  Save the Translation Block
function save_trans_block( $key, $name, $contents, $locale = null )
{
    //  Get Locale
    if( !$locale )  $locale = app('translator')->getLocale();

    //  Read All
    $all = trans( 'blocks', array(), 'messages', $locale );
    if( $all == 'blocks' )  $all = array();

    //  Set Value
    $all[$key] = $name;

    //  Save Trans
    save_trans( 'blocks', $all, $locale );

    //  Get Block File
    $block_file = trans_file( $name, $locale, true );

    //  Block File Dir
    $block_file_dir = dirname( $block_file );

    //  Check
    if( !file_exists( $block_file_dir ) )   mkdir( $block_file_dir, 0777, true );

    //  Save Output
    return file_put_contents( $block_file, $contents );
}

//  Read Translation Block
function read_trans_block( $id = null, $parameters = [], $locale = null, $domain = null )
{
    //  Get File
    $file = trans_block_file( $id, $parameters, $locale, $domain );

    //  Start
    ob_start();

    //  Include File
    if( $file && file_exists( $file ) )  include $file;

    //  Return
    return ob_get_clean();
}

//  Get Lang Code
function get_lang_code( $key )
{
    //  Get Info
    $info = config( 'langs.' . $key );

    //  Return
    return ( $info ? $info['code'] : $key );
}

//  Get Lang Label
function get_lang_label( $key )
{
    //  Get Info
    $info = config( 'langs.' . $key );

    //  Return
    return ( $info ? $info['label'] : $key );
}

//  Get Related Languages
function get_related_langs( $key )
{
    //  Check
    if( stripos( $key, '_' ) > -1 ) $key = get_lang_code( $key );

    //  List
    $list = array();

    //  Loop Each
    foreach( config( 'langs', array() ) as $k => $info )
    {
        //  Check
        if( $info['code'] == $key )
        {
            //  Store
            $list[$k] = $info['label'];
        }
    }

    //  Return
    return $list;
}

//  Get Supported Languages
function get_supported_langs()
{
    //  Return
    return array_keys( config( 'langs', array() ) );
}

//  Get Default Lang
function default_lang()
{
    //  Static
    static $default_lang;

    //  Check
    if( !$default_lang )
    {
        //  Get Config
        $configs = include base_path( 'config/app.php' );

        //  Set
        $default_lang = $configs['locale'];
    }

    //  Return
    return $default_lang;
}