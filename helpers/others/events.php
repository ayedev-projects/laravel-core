<?php

//  Listen for Model Events
function whenModel( $model, $event, $closure, $priority = 0 )
{
    //  Check
    if( is_object( $model ) )   $model = get_class( $model );

    //  Register the Listener
    return $model::$event( $closure, $priority );
}

//  Helper Function to Trigger Save Files
function saveFilesWhenModel( $model, $priority = 0 )
{
    //  Add Event
    whenModel( $model, 'saved', function( $record )
    {
        //  Check Method Exists
        if( method_exists( $record, 'saveTheFiles' ) )
        {
            //  Save the Files
            $record->saveTheFiles();
        }
    }, $priority );
}

/**
 * Capture Event
 *
 * @param $events
 * @param $listener
 * @param int $priority
 * @return array|null
 */
function capture( $events, $listener, $priority = 0 )
{
    //  Return
    return app('events')->listen( $events, $listener, $priority );
}

/**
 * Fire the Event
 *
 * @param $event
 * @param array $payload
 * @param bool $single
 * @param bool $halt
 * @return array|null
 */
function fire( $event, $payload = [], $single = false, $halt = false )
{
    //  In
    $isIn = ( sessionIsReady() && function_exists( 'isLoggedIn' ) ? isLoggedIn() : false );

    //  Response
    $response = event( $event, $payload, $halt );

    //  Def Event Key
    $defEventKey = '[' . ( $isIn ? 'in' : 'out' ) . ']';

    //  Events
    $events = array();

    //  Check
    if( !$single )
    {
        //  Add Event
        $events[] = $defEventKey . $event;

        //  Route Name
        $routeName = routeName();

        //  Check
        if( $routeName )
        {
            //  Route Event Key
            $rEventKey = '-' . $routeName . '-';

            //  Add Event
            $events[] = $defEventKey . $rEventKey . $event;

            //  Add Events
            $events[] = '[' . currentSubNamespace() . ']' . $rEventKey . $event;
            $events[] = '[' . currentSubNamespace() . ']' . $defEventKey . $rEventKey . $event;
        }
        else
        {
            //  Add Event
            $events[] = '[front_only]' . $event;
        }

        //  Loop Each
        foreach( $events as $evName )
        {
            //  Fire Event
            event( $evName, $payload, false );
        }
    }

    //  Return
    return $response;
}

//  Get Current Sub Namespace
function currentSubNamespace()
{
    //  Sub Namespace
    static $subNamespace = null;

    //  Check
    if( !$subNamespace )
    {
        //  Get Route Action
        $routeAction = ( currentRoute() ? currentRoute()->getAction() : null );

        //  Check
        if( $routeAction )
        {
            //  Namespace
            $namespace = ( is_array( $routeAction ) && isset( $routeAction['namespace'] ) ? str_ireplace( '\\', '/', $routeAction['namespace'] ) : null );

            //  Sub Namespace
            $subNamespace = ( $namespace ? strtolower( basename( $namespace ) ) : null );
            if( !$subNamespace || $subNamespace == 'controllers' )  $subNamespace = 'front';
        }
    }

    //  Return
    return $subNamespace;
}