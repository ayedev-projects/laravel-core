<?php

//  Captcha Field
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Thomaswelton\LaravelGravatar\Facades\Gravatar;

function captchaField() {

    //  Return
    return '<div class="g-recaptcha" data-sitekey="' . config( 'settings.recaptcha_site_key' ) . '"></div><a href="javascript:void(0);" class="goback-to-inputs">Go Back</a>';
}

//  Check is Captcha Valid
function isCaptchaValid( $key = null ) {

    //  Key
    $key || $key = 'g-recaptcha-response';

    //  Get Verification Response
    $response = @json_decode( file_get_contents( "https://www.google.com/recaptcha/api/siteverify?secret=" . config( 'settings.recaptcha_site_secret' ) . "&response=" . Input::get( $key ) ."&remoteip=" . Request::ip() ), true );

    //  Return
    return ( !$response || ( $response && true === $response['success'] ) );
}

//  Gravatar Size Helpers
function gravatar_sizes() {

    //  Return
    return apply_filters( 'gravatar_sizes', array(
        'medium' => '300x300#',
        'thumb' => '120x120#',
        'tiny' => '64x64#'
    ) );
}

//  Gravatar Image Link
function gravatar_image_url( $email, $sizeKey = 'thumb' ) {

    //  Sizes
    $sizes = gravatar_sizes();

    //  Size
    $size = str_ireplace( '#', '', ( isset( $sizes[$sizeKey] ) ? $sizes[$sizeKey] : $sizeKey ) );

    //  Return
    return Gravatar::src( $email, ( stripos( $size, 'x' ) > -1 ? explode( 'x', $size ) : $size ) );
}

//  Convert XML to Array
function xml_to_array( $xml )
{
    //  Return
    return json_decode(json_encode( $xml ), true );
}