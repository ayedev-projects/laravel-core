<?php

//  Detect the Table Name
function detectTableName( $key, $addPrefix = false ) {

    //  Get Prefix
    $prefix = DB::getTablePrefix();

    //  Table
    $table = $key;

    //  Check
    if( substr( $table, 0, 4 ) == 'tbl:' ) {

        //  Read from Config
        $table = config( 'tables.' . substr( $table, 4 ) );
    }

    //  Return
    return ( $addPrefix ? $prefix : '' ) . $table;
}

//  Get Table Name
function tableName( $key, $addPrefix = false ) {

    //  Return
    return detectTableName( 'tbl:' . $key, $addPrefix );
}

//  Enable Query Logs
function db_enable_logs() {

    //  Enable
    DB::connection()->enableQueryLog();
}

//  Disable Query Logs
function db_disable_logs() {

    //  Enable
    DB::connection()->disableQueryLog();
}

//  Get Query Logs
function db_query_logs() {

    //  Enable
    return DB::getQueryLog();
}

//  Extract List from the Results
function extract_list($results, $valIndex, $keyIndex = null) {

    //  Lists
    $lists = array();

    //  Records
    $records = array();

    //  Check
    if( $results instanceof \Illuminate\Database\Eloquent\Collection ) {

        //  Set Records
        $records = $results->all();
    }
    else if( is_array( $results ) ) {

        //  Set Records
        $records = (array) $results;
    }

    //  Loop Each
    foreach($records as $item) {

        //  Get Key and Value
        $key = ($keyIndex ? $item->{$keyIndex} : null);
        $val = $item->{$valIndex};

        //  Check
        if($val) {

            //  Store
            if($key)    $lists[$key] = $val;
            else    $lists[] = $val;
        }
    }

    //  Return
    return $lists;
}

/**
 * Generate random pronounceable words
 *
 * @param int $length Word length
 * @return string Random word
 */
function pronounceable_word( $length = 8 ) {

    // consonant sounds
    $cons = array(
        // single consonants. Beware of Q, it's often awkward in words
        'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm',
        'n', 'p', 'r', 's', 't', 'v', 'w', 'x', 'z',
        // possible combinations excluding those which cannot start a word
        'pt', 'gl', 'gr', 'ch', 'ph', 'ps', 'sh', 'st', 'th', 'wh',
    );

    // consonant combinations that cannot start a word
    $cons_cant_start = array(
        'ck', 'cm',
        'dr', 'ds',
        'ft',
        'gh', 'gn',
        'kr', 'ks',
        'ls', 'lt', 'lr',
        'mp', 'mt', 'ms',
        'ng', 'ns',
        'rd', 'rg', 'rs', 'rt',
        'ss',
        'ts', 'tch',
    );

    // wovels
    $vows = array(
        // single vowels
        'a', 'e', 'i', 'o', 'u', 'y',
        // vowel combinations your language allows
        'ee', 'oa', 'oo',
    );

    // start by vowel or consonant ?
    $current = ( mt_rand( 0, 1 ) == '0' ? 'cons' : 'vows' );

    $word = '';

    while( strlen( $word ) < $length ) {

        // After first letter, use all consonant combos
        if( strlen( $word ) == 2 )
            $cons = array_merge( $cons, $cons_cant_start );

        // random sign from either $cons or $vows
        $rnd = ${$current}[ mt_rand( 0, count( ${$current} ) -1 ) ];

        // check if random sign fits in word length
        if( strlen( $word . $rnd ) <= $length ) {
            $word .= $rnd;
            // alternate sounds
            $current = ( $current == 'cons' ? 'vows' : 'cons' );
        }
    }

    //  Return
    return $word;
}

//  Make Random Username
function random_username()
{
    //  Return
    return ucfirst( pronounceable_word( mt_rand( 4, 6 ) ) ) . ucfirst( pronounceable_word( mt_rand( 5, 8 ) ) );
}