<?php

//	Open Image
function openImage( $path )
{
	//	Return
	return \Gregwar\Image\Image::open( $path )
		->setCacheDir( assets()->cache( null, false ) )
		->setFallback( assets()->image( 'no-image.png') );
}

//  Decamelize
function decamelize($word) {
    return $word = preg_replace_callback(
        "/(^|[a-z])([A-Z])/",
        function($m) { return strtolower(strlen($m[1]) ? "$m[1]_$m[2]" : "$m[2]"); },
        $word
    );
}

//  Camelize
function camelize($word) {
    return $word = preg_replace_callback(
        "/(^|_)([a-z])/",
        function($m) { return strtoupper("$m[2]"); },
        $word
    );
}

//  Format Bytes
function formatBytes($bytes, $precision = 2, $append_units = true) {
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    // Uncomment one of the following alternatives
    // $bytes /= pow(1024, $pow);
    $bytes /= (1 << (10 * $pow));

    return round($bytes, $precision) . ( $append_units ? ' ' . $units[$pow] : '' );
}

//  Extract Shorter
function extract_shorter($str, $count = null, $append = '') {

    //  Check
    if(is_null($count)) $count = 15;

    //  Explode
    $explodes = explode( ' ', trim( $str ) );

    //  Check
    if(sizeof($explodes) > $count) {

        //  Get Chunk
        $chunks = array_chunk($explodes, $count);

        //  Get the String
        $str = implode(' ', $chunks[0]) . $append;
    }

    //  Return
    return $str;
}

//  Number Formatter
function nFormatter($num, $digits = 2) {

    //  Check
    if( is_string( $num ) )  $num = (float)( str_ireplace( ',', '', $num ) );

    //  Mappers
    $si = array(
        array( 'value' => 1E18, 'symbol' => "E" ),
        array( 'value' => 1E15, 'symbol' => "P" ),
        array( 'value' => 1E12, 'symbol' => "T" ),
        array( 'value' => 1E9,  'symbol' => "G" ),
        array( 'value' => 1E6,  'symbol' => "M" ),
        array( 'value' => 1E3,  'symbol' => "k" )
    );

    //  Loop Each
    for( $i = 0; $i < sizeof( $si ); $i++ ) {

        //  Check
        if( $num >= $si[$i]['value'] ) {

            //  Return
            return preg_replace( '/\.0+$|(\.[0-9]*[1-9])0+$/', '$1', round( ( $num / $si[$i]['value'] ), $digits ) ) . $si[$i]['symbol'];
        }
    }

    //  Return
    return $num;
}

/**
 * Get the Icons Library
 *
 * @return \Ayedev\LaravelCore\Extension\IconLibrary
 */
function icons() {

    //  Return
    return app( 'icons' );
}

//  Make Icon
function icon( $str, $location = null )
{
    //  Return
    return icons()->make( $str, $location );
}

//  Get the Part of Matched String
function getSearchMatchContent( $term, $value, $count = 30, $trimLength = 320 )
{
	//	Trim
	$value = trim( $value );

    //  Get Content
    $content = extract_shorter( $value, $count );

    //  Check
    if( $term && ( $searchIndex = stripos( $value, $term ) ) > -1 ) {

        //  Start Index
        $startIndex = $searchIndex - 100;
		$trailBefore = $startIndex > 0;
        if( $startIndex < 0 )   $startIndex = 0;

        //  Set Content
        $content = ( $startIndex > 10 ? substr( $value, 0, 10 ) . ( $trailBefore ? '&hellip;' : '' ) : '' ) . substr( $value, $startIndex, $trimLength );
    }

    //  Return
    return ( $term ? preg_replace( '/(' . $term . ')/i', '<span class="search-term theme-text">$1</span>', $content ) : $content );
}

//  Format Contents
function formatContents( $contents )
{
    //  Explode
    $explodes = explode( "\r\n", $contents );

    //  New Description
    $formatted = '<p>' . implode( '</p><p>', $explodes ) . '</p>';

    //  Return
    return $formatted;
}

//  New Line to Paragraph
function nl2p( $text ) {

    //  Paragraphs
    $paragraphs = array();

    //  Explodes
    $explodes = explode( "\n", $text );

    //  Loop Each
    foreach( $explodes as $exp ) {

        //  Trim
        $str = trim( $exp );

        //  Check
        if( strlen( $str ) > 0 )    $paragraphs[] = $str;
    }

    //  Return
    return ( sizeof( $paragraphs ) > 0 ? '<p>' . implode( '</p><p>', $paragraphs ) . '</p>' : '' );
}

//  New Line to Joins
function nl2j( $text, $joiner = ' - ' ) {

    //  Paragraphs
    $paragraphs = array();

    //  Explodes
    $explodes = explode( "\n", $text );

    //  Loop Each
    foreach( $explodes as $exp ) {

        //  Trim
        $str = trim( $exp );

        //  Check
        if( strlen( $str ) > 0 )    $paragraphs[] = $str;
    }

    //  Return
    return ( sizeof( $paragraphs ) > 0 ? implode( $joiner, $paragraphs ): '' );
}

//  Attrs to HTML
function _attrs_to_html($attrs, $ignore_null = false, $glue = ' ', $use_quote = true, $join_glue = ' ') {

    //  Check for Array
    if(!is_array($attrs) && !is_object($attrs))
        return $attrs;

    //  HTML
    $html = array();

    //  Loop Each
    foreach($attrs as $aKey => $aVal) {

        //  Ignore Null
        if(is_null($aVal) && $ignore_null)  continue;

        //  Check for Array
        if(is_array($aVal)) $aVal = implode($glue, $aVal);

        //  Quote
        $quote = ( ($aVal && substr($aVal, 0, 1) == '{' && substr($aVal, -1) == '}') ? "'" : '"' );

        //  Check Use Quote
        if(!$use_quote) $quote = '';

        //  Append
        $html[] = $aKey . ($aVal ? '=' . $quote . (string)$aVal . $quote : '');
    }

    //  Return
    return implode($join_glue, $html);
}

//  HTML to Attrs
function _html_to_arr($html) {

    //  Attributes
    $attrs = array();

    //  Check not Empty
    if(!empty($html)) {

        //  Create the Element
        $element = new SimpleXMLElement("<div " . $html . "></div>");

        //  Loop Each
        foreach($element->attributes() as $attr_key => $attr_val) {

            //  Store
            $attrs[$attr_key] = (string)$attr_val;
        }
    }

    //  Return
    return $attrs;
}

//	Image Lazy Load Tag
function lazy_img( $src, $alt = null, $append = null, $width = null, $height = null, $class = null, $pretty_anim = false )
{
	//	Go Lazy
	$goLazy = ( app( \Jaybizzle\CrawlerDetect\CrawlerDetect::class )->isCrawler() || isset( $_GET['nolazy'] ) ? false : true );

	//	Return
	return "<img src=\"" . ( $goLazy ? ( $pretty_anim ? '' : assets()->image( "loading.gif" ) ) : $src ) . "\" alt=\"{$alt}\" class=\"quite-lazy " . ( $goLazy && $pretty_anim ? 'with-pretty-anim ' : '' ) . "{$class}\" " . ( $goLazy ? "data-src=\"{$src}\" data-lazy-load=\"true\"" : '' ) . ( $width ? " width=\"{$width}\" " : '' ) . ( $height ? " height=\"{$height}\" " : '' ) . " {$append} />" . ( $goLazy && $pretty_anim ? '<span class="lazy-spinner"></span>' : '' );
}

//	Image Lazy Load Tag with pretty animation
function lazy_img_pretty( $src, $alt = null, $append = null, $width = null, $height = null, $class = null )
{
	//	Return
	return lazy_img( $src, $alt, $append, $width, $height, $class, true );
}

//  Get the Normal Image
function image_url( $record, $fallback = null, $model = null, $key = null )
{
    //  Return
    return ( $record ? ( $record->exists() ? $record->url() : $record->getModel()->file404( $record->getFieldKey() ) ) : ( $model ? $model->file404( $key ) : ( $fallback ?: assets()->image( 'placeholders/no_image.jpg' ) ) ) );
}

//  Get the Cache Image
function cache_url( $record, $size = null, $fallback = null, $model = null, $key = null )
{
    //  Return
    return ( $record ? ( $record->exists() ? $record->cache( $size ) : $record->getModel()->cacheFile404( $record->getFieldKey() ) ) : ( $model ? $model->cacheFile404( $key ) : ( $fallback ?: assets()->image( 'placeholders/no_image.jpg' ) ) ) );
}
