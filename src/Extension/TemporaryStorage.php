<?php namespace Ayedev\LaravelCore\Extension;

class TemporaryStorage {

    //  Name
    private $name = null;

    //  Storage
    private $storage = array();

    //  Working Key
    private $workingKey = null;

    //  Stored Flags
    private $flagStorage = array();

    //  Static Storage
    private $staticStorage = array();

    //  Instance
    private static $instances = array();


    //  Init
    public static function createInstance($name = 'default') {

        //  Create Instance
        self::$instances[$name] = new self($name);
    }

    //  Get Instance
    public static function getInstance($name = 'default') {

        //  Validate
        if(!isset(self::$instances[$name]))  self::createInstance($name);

        //  Return
        return self::$instances[$name];
    }


    //  Constructor
    public function __construct($name = 'default') {

        //  Store Name
        $this->name = $name;

        //  Reset Things
        $this->storage = array();
    }

    //  Start
    public function start($key, $def = array()) {

        //  Check Key Exists
        if(!$this->hasStorage($key))
            $this->validateSubKey($this->storage, $key, $def);

        //  Set the Working Key
        $this->workingKey = $key;

        //  Return
        return $this;
    }

    //  Work with Storage
    public function work($callback, $newOnly = false) {

        //  Check Working Key
        if($this->workingKey) {

            //  Check for Newonly
            if(!$newOnly
                || ($newOnly && !$this->getStorage($this->workingKey, null))) {

                //  Arguments
                $args = array($this->storage[$this->workingKey], $this);

                //  Run the Worker
                $this->storage[$this->workingKey] = call_user_func_array($callback, $args);
            }
        }

        //  Return
        return $this;
    }

    //  Stop
    public function stop() {

        //  Clear the Working Key
        $this->workingKey = null;

        //  Return
        return $this;
    }

    //  Store Data
    public function storeData($pKey, $val, $key = null) {

        //  Check
        if(!$this->hasStorage($pKey)) {

            //  Create
            $this->storage[$pKey] = array();
        }

        //  Check for Key
        if($key) {

            //  Store
            $this->storage[$pKey][$key] = $val;
        } else {

            //  Check If Already Exists
            if(!in_array($val, $this->storage[$pKey])) {

                //  Store
                $this->storage[$pKey][] = $val;
            }
        }
    }

    //  Read the Stored Data Key
    public function storedDataKey($pKey, $val){

        //  Check
        if($this->hasStorage($pKey)) {

            //  Search
            $index = array_search($val, $this->storage[$pKey]);

            //  Check
            if($index > -1) return $index;
        }

        //  Return
        return null;
    }

    //  Store Sub Data
    public function storeSubData($pKey, $spKey, $val, $key = null) {

        //  Validate
        if(!$this->hasStorage($pKey)) {

            //  Create
            $this->storage[$pKey] = array();
        }

        //  Validate Sub
        if(!$this->hasStorageSub($pKey, $spKey)) {

            //  Create
            $this->storage[$pKey][$spKey] = array();
        }

        //  Check for Key
        if($key) {

            //  Store
            $this->storage[$pKey][$spKey][$key] = $val;
        } else {

            //  Check If Already Exists
            if(!in_array($val, $this->storage[$pKey][$spKey])) {

                //  Store
                $this->storage[$pKey][$spKey][] = $val;
            }
        }
    }

    //  Read the Stored Data Key
    public function storedSubDataKey($pKey, $spKey, $val){

        //  Check
        if($this->hasStorageSub($pKey, $spKey)) {

            //  Search
            $index = array_search($val, $this->storage[$pKey][$spKey]);

            //  Check
            if($index > -1) return $index;
        }

        //  Return
        return null;
    }

    //  Validate Sub Key
    public function validateSubKey(&$source, $key, $val = array()) {

        //  Check Parent Key
        if(!isset($source[$key]))
            $source[$key] = $val;
    }

    //  Throw Sub Key
    public function throwSubKey(&$source, $key) {

        //  Check Parent Key
        if(isset($source[$key]))
            unset($source[$key]);
    }

    //  Get the Storage
    public function getStorage($key, $def = array()) {

        //  Return
        return ($this->hasStorage($key) ? $this->storage[$key] : $def);
    }

    //  Get the Storage using Filters
    public function getStorageUsingFilter($key, $filters, $def = array()) {

        //  Storage Data
        $storageData = $this->getStorage($key, $def);

        //  Return
        return $this->_filter_data($storageData, $filters);
    }

    //  Get the Storage
    public function getStorageSub($pKey, $spKey, $def = array(), $key = null) {

        //  Return
        return ($this->hasStorageSub($pKey, $spKey) ? (!is_null($key) && isset($this->storage[$pKey][$spKey][$key]) ? $this->storage[$pKey][$spKey][$key] : $this->storage[$pKey][$spKey]) : $def);
    }

    //  Get the Storage Sub using Filters
    public function getStorageSubUsingFilter($key, $filters, $def = array()) {

        //  Storage Sub Data
        $storageData = $this->getStorageSub($key, $def);

        //  Return
        return $this->_filter_data($storageData, $filters);
    }

    //  Check Has Storage
    public function hasStorage($key) {

        //  Return
        return (isset($this->storage[$key]));
    }

    //  Check Has Storage Sub
    public function hasStorageSub($pKey, $key) {

        //  Return
        return ($this->hasStorage($pKey) && isset($this->storage[$pKey][$key]));
    }

    //  Remove Storage
    public function removeStorage($key, $search = null) {

        //  Check
        if($this->hasStorage($key)) {

            //  Check
            if($search) {

                //  Search
                $idx = array_search($search, $this->storage[$key]);

                //  Check
                if($idx > -1) {

                    //  Remove
                    unset($this->storage[$key][$idx]);
                }
            } else {

                //  Remove
                unset($this->storage[$key]);
            }
        }
    }

    //  Remove Storage Sub
    public function removeStorageSub($pKey, $key, $search = null) {

        //  Check
        if($this->hasStorageSub($pKey, $key)) {

            //  Check
            if($search) {

                //  Search
                $idx = array_search($search, $this->storage[$pKey][$key]);

                //  Check
                if($idx > -1) {

                    //  Remove
                    unset($this->storage[$pKey][$key][$idx]);
                }
            } else {

                //  Remove
                unset($this->storage[$pKey][$key]);
            }
        }
    }

    //  Write Flag
    public function writeFlag($key, $val = true) {

        //  Set
        $this->flagStorage[$key] = (bool)intval($val);
    }

    //  Read Flag
    public function readFlag($key, $def = false) {

        //  Return
        return (isset($this->flagStorage[$key]) ? $this->flagStorage[$key] : $def);
    }

    //  Write Static Storage
    public function writeStorage($key, $val) {

        //  Set
        $this->staticStorage[$key] = $val;
    }

    //  Read Static Storage
    public function readStorage($key, $def = null) {

        //  Return
        return (isset($this->staticStorage[$key]) ? $this->staticStorage[$key] : $def);
    }

    //  Delete Storage
    public function deleteStorage($key) {

        //  Check
        if(isset($this->staticStorage[$key])) {

            //  Delete
            unset($this->staticStorage[$key]);
        }
    }

    //  Filter Data
    private function _filter_data($data, $filters) {

        //  Filters
        $filters = (array)$filters;

        //  Loop Each
        foreach($filters as $filter) {

            //  Loop Each Data
            foreach($data as $key => $value) {

                //  Keep Check
                $keep = call_user_func_array($filter, array($value, $key));

                //  Check not Keep
                if(!$keep) {

                    //  Unset
                    unset($data[$key]);
                }
            }
        }

        //  Return
        return $data;
    }
}