<?php namespace Ayedev\LaravelCore\Extension;

class Collection extends \Illuminate\Support\Collection
{
    /**
     * Add Item
     *
     * @param $item
     * @return $this
     */
    public function add( $item )
    {
        //  Store
        $this->items[] = $item;

        //  Return
        return $this;
    }
}