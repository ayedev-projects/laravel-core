<?php namespace Ayedev\LaravelCore\Extension;

class IconLibrary
{
    /** @var array $_sources */
    protected $_sources = array();


    /**
     * IconLibrary constructor.
     */
    public function __construct()
    {
        //  Add Font Awesome
        $this->setSource( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', 'fa', 'fa fa-%s' );

        //  Add Bootstrap
        $this->setSource( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', 'gl', 'glyphicon glyphicon-%s', array(
            'integrity' => 'sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7',
            'crossorigin' => 'anonymous'
        ), 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js', array(
            'integrity' => 'sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS',
            'crossorigin' => 'anonymous'
        ), null, array( 'jquery' ) );

        //  Add Google Material Icons
        //$this->setSource( 'google-material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons', array( 'mi', 'micon' ), 'material-icons' );
        $this->setSource( 'google-material-icons', function()
		{
			//	Add Material Icons Variation
			assets()->assignFontVariation( 'Material Icons', array() );
		}, array( 'mi', 'micon' ), 'material-icons' );

        //  Add Social Icons
        $this->setSource( 'social-icons', 'https://file.myfontastic.com/n6vo44Re5QaWo8oCKShBs7/icons.css', array( 'social' ), 'socicon-%' );
    }

    /**
     * Set Source
     *
     * @param $name
     * @param $uri
     * @param $mappings
     * @param $output_format
     * @param array $append
     * @param null $script_uri
     * @param array $script_append
     * @param array $deps
     * @param array $script_deps
     * @return $this
     */
    public function setSource( $name, $uri, $mappings, $output_format, $append = array(), $script_uri = null, $script_append = array(), $deps = array(), $script_deps = array() )
    {
        //  Store
        $this->_sources[$name] = array(
            'uri' => $uri,
            'append' => $append,
            'format' => $output_format,
            'script_uri' => $script_uri,
            'mappings' => (array)$mappings,
            'script_append' => $script_append,
            'deps' => $deps,
            'script_deps' => $script_deps,
        );

        //  Return
        return $this;
    }

    /**
     * Get the Source Property
     *
     * @param $name
     * @param $key
     * @param null $def
     * @return $this
     */
    public function getSourceProperty( $name, $key, $def = null )
    {
        //  Check
        if( isset( $this->_sources[$name] ) && isset( $this->_sources[$name][$key] ) )
        {
            //  Return
            return $this->_sources[$name][$key];
        }

        //  Return
        return $def;
    }

    /**
     * Update the Source Property
     *
     * @param $name
     * @param $key
     * @param $value
     * @return $this
     */
    public function updateSourceProperty( $name, $key, $value )
    {
        //  Check
        if( isset( $this->_sources[$name] ) )
        {
            //  Update Property
            $this->_sources[$name][$key] = $value;
        }

        //  Return
        return $this;
    }

    /**
     * Remove Source
     *
     * @param $name
     * @return $this
     */
    public function removeSource( $name )
    {
        //  Check
        if( isset( $this->_sources[$name] ) )
        {
            //  Remove
            unset( $this->_sources[$name] );
        }

        //  Return
        return $this;
    }


    /**
     * Make the Tag
     *
     * @param $icon_txt
     * @param null $location
     * @param null $appendClass
     * @param null $appendHTML
     * @param string $tag_name
     * @return null|string
     */
    public function make( $icon_txt, $location = null, $appendClass = null, $appendHTML = null, $tag_name = 'i' )
    {
        //  Tag
        $tag = null;

        //  Explode
        $explodes = ( $icon_txt ? explode( '-', $icon_txt, 2 ) : null );

        //  Loop Each
        foreach( $this->_sources as $sourceConfig )
        {
            //  Check
            if( in_array( $explodes[0], $sourceConfig['mappings'] ) )
            {
                //  Format
                $format = $sourceConfig['format'];

                //  Has Placeholder
                $hasPlaceholder = ( stripos( $format, '%s' ) > -1 );

                //  Classes
                $classes = array( 'icon' );

                //  Check
                if( $hasPlaceholder ) $classes[] = sprintf( $format, $explodes[1] );
                else    $classes[] = $format;

                //  Check Location
                if( $location ) $classes[] = 'icon-' . $location;

                //  Check Append Class
                if( $appendClass )  $classes[] = $appendClass;

                //  Set Tag
                $tag = "<{$tag_name} class=\"" . implode( ' ', $classes ) . "\"" . ( $appendHTML ? ' ' . $appendHTML : '') . ">" . ( !$hasPlaceholder ? $explodes[1] : '' ) . "</{$tag_name}>";

                //  Break
                break;
            }
        }

        //  Return
        return $tag;
    }

    /**
     * Check is Valid Icon
     *
     * @param $icon_txt
     * @return bool
     */
    public function isValid( $icon_txt )
    {
        //  Valid
        $valid = false;

        //  Explode
        $explodes = ( $icon_txt ? explode( '-', $icon_txt, 2 ) : null );

        //  Loop Each
        foreach( $this->_sources as $sourceConfig )
        {
            //  Check
            if( in_array( $explodes[0], $sourceConfig['mappings'] ) )
            {
                //  Set Valid
                $valid = true;
                break;
            }
        }

        //  Return
        return $valid;
    }

    /**
     * Check for Valid Tag
     *
     * @param $tag
     * @return bool
     */
    public function isValidTag( $tag )
    {
        //  Return
        return ( $tag && substr( $tag, 0, 1 ) == '<' && substr( $tag, -1 ) == '>' && stripos( $tag, 'icon' ) > -1 );
    }

    /**
     * Make the Tag for Icon/Image
     *
     * @param $str
     * @param null $alt
     * @param null $append
     * @return null|string
     */
    public function makeTag( $str, $alt = null, $append = null )
    {
        //  Tag
        $tag = null;

        //  Icon URL
        $iconURL = $str;

        //  Check
        if( $iconURL && !$this->isValidTag( $iconURL ) && !$this->isValid( $iconURL ) )
        {
            //  Check
            if( stripos( $iconURL, '://' ) === false )  $iconURL = assets()->imageUrl( $iconURL );

            //  Create Image Tag
            $tag = '<img src="' . $iconURL . '" alt="' . $alt . '" ' . $append . ' />';
        }
        else
        {
            //  Get Icon
            $tag = $this->make( $iconURL );
        }

        //  Return
        return $tag;
    }


    /**
     * Enqueue to Assets Manager
     *
     * @return $this
     */
    public function toAssets()
    {
        //  Loop Each
        foreach( $this->_sources as $sourceName => $sourceConfig )
        {
            //  Check for CSS
            if( !is_null( $sourceConfig['uri'] ) )
            {
				//	Check is Callback
				if( is_callable( $sourceConfig['uri'] ) )
				{
					//	Run Callback
					call_user_func_array( $sourceConfig['uri'], array( assets(), $this ) );
				}
				else
				{
					//  Add CSS
					assets()
						->register( $sourceName . '-css', $sourceConfig['uri'], array( 'append' => $sourceConfig['append'] ) )
						->enqueueHead( $sourceName . '-css', null, $sourceConfig['deps'] );
				}
            }

            //  Check for JS
            if( !is_null( $sourceConfig['script_uri'] ) )
            {
				//	Check is Callback
				if( is_callable( $sourceConfig['uri'] ) )
				{
					//	Run Callback
					call_user_func_array( $sourceConfig['uri'], array( assets(), $this ) );
				}
				else
				{
					//  Add JS
					assets()
						->register( $sourceName . '-js', $sourceConfig['script_uri'], array( 'append' => $sourceConfig['script_append'] ) )
						->enqueueFooter( $sourceName . '-js', null, $sourceConfig['script_deps'] );
				}
            }
        }

        //  Return
        return $this;
    }
}
