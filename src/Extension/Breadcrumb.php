<?php namespace Ayedev\LaravelCore\Extension;

class Breadcrumb {

    /** @var null $_title */
    private $_title = null;

    /** @var int $_threshold */
    private $_threshold = 1;

    /** @var bool $_compact */
    private $_compact = false;

    /** @var array $_crumbs */
    private $_crumbs = array();

    /** @var int $_maxPriority */
    private $_maxPriority = 0;

    /** @var int $_style */
    private $_style = 'plain';

    /** @var null $instance */
    private static $instance = null;


    //  Init
    public static function createInstance()
    {
        //  Create Instance
        self::$instance = new self;
    }

    /**
     * Get Instance
     *
     * @return Breadcrumb
     */
    public static function getInstance()
    {
        //  Validate
        if(!self::$instance)    self::createInstance();

        //  Return
        return self::$instance;
    }


    /** Constructor */
    public function __construct()
    {
        //  Add Home Crumb
        $this->add( 'Home', 'home', null, true );
    }

    /**
     * Set Title
     *
     * @param $title
     * @param bool $pushToAssets
     * @return $this
     */
    public function setTitle( $title, $pushToAssets = true )
    {
        //  Update
        $this->_title = $title;

        //  Check
        if( $pushToAssets ) assets()->setTitle( $title );

        //  Return
        return $this;
    }

    /**
     * Check has Title
     *
     * @return bool
     */
    public function hasTitle()
    {
        //  Return
        return ( !is_null( $this->_title ) && strlen( $this->_title ) > 0 );
    }

    /**
     * Set Links Threshold
     *
     * @param $threshold
     * @return $this
     */
    public function setThreshold( $threshold )
    {
        //  Update
        $this->_threshold = $threshold;

        //  Return
        return $this;
    }

    /**
     * Set Compact
     *
     * @param $compact
     * @return $this
     */
    public function setCompact( $compact )
    {
        //  Update
        $this->_compact = (bool) intval( $compact );

        //  Return
        return $this;
    }

    /**
     * Set Style
     *
     * @param $style
     * @return $this
     */
    public function setStyle( $style )
    {
        //  Update
        $this->_style = $style;

        //  Return
        return $this;
    }

    /**
     * Add Crumb
     *
     * @param $title
     * @param null|string $link
     * @param null $priority
     * @param bool $is_route
     * @return $this
     */
    public function add($title, $link = 'javascript:void(0);', $priority = null, $is_route = false )
    {
        //  Check for Route
        if( $is_route )
        {
            //  Update Link
            $link = route( ( is_array( $link ) ? $link[0] : $link ), ( is_array( $link ) ? $link[1] : array() ) );
        }

        //  Fix Priority
        $priority || $priority = $this->_maxPriority + 1;

        //  Store
        $this->_crumbs[] = array(
            'title' => $title,
            'link' => $link,
            'priority' => $priority
        );

        //  Update Max Priority
        if( $priority > $this->_maxPriority )   $this->_maxPriority = $priority;

        //  Return
        return $this;
    }

    /**
     * Add Route Crumb
     *
     * @param $title
     * @param $route
     * @param null $priority
     * @return Breadcrumb
     */
    public function route( $title, $route, $priority = null )
    {
        //  Return
        return $this->add( $title, $route, $priority, true );
    }

    /**
     * Clear the Crumbs
     *
     * @return $this
     */
    public function clearCrumbs() {

        //  Clear
        $this->_crumbs = array();

        //  Return
        return $this;
    }

    /**
     * Render the Breadcrumb
     *
     * @param int $style
     * @param null|string $file
     * @return string
     */
    public function render( $style = null, $file = 'front.common.breadcrumb' )
    {
        //  Check
        if( sizeof( $this->_crumbs ) <= $this->_threshold )  return '';

        //  Fix Style
        $style || $style = $this->_style;

        //  Sort
        uasort( $this->_crumbs, function( $a, $b ) {
            return ( $b['priority'] < $a['priority'] );
        } );

        //  Return
        return view()->make( $file, array(
            'compact' => $this->_compact,
            'title' => $this->_title,
            'style_code' => $style,
            'crumbs' => $this->_crumbs
        ) )->render();
    }
}