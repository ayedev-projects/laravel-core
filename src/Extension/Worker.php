<?php namespace Ayedev\LaravelCore\Extension;

class Worker {

    //  Worker Objects
    private $objects = null;

    //  Worker Params
    private $params = array();


    //  Constructor
    public function __construct($object, $params = array()) {

        //  Store Object
        $this->objects = (is_array($object) ? $object : array($object));

        //  Store Params
        $this->params = $params;

        //  Return
        return $this;
    }

    //  Set Param
    public function setParam($key, $val) {

        //  Set
        $this->params[$key] = $val;

        //  Return
        return $this;
    }

    //  Check Has Param
    public function hasParam($key) {
        return (isset($this->params[$key]));
    }

    //  Remove Param
    public function unsetParam($key) {

        //  Check
        if($this->hasParam($key)) {

            //  Unset
            unset($this->params[$key]);
        }

        //  Return
        return $this;
    }

    //  Get Param
    public function getParam($key, $def = null) {

        //  Check
        if($this->hasParam($key)) {

            //  Return
            return $this->params[$key];
        }

        //  Return
        return $def;
    }

    //  Get All Params
    public function getParams() {
        return $this->params;
    }

    //  Work
    public function work($callback) {

        //  Loop Each
        foreach($this->objects as $object) {

            //  Run
            call_user_func_array($callback, array($object, $this));
        }

        //  Return
        return $this;
    }

    //  Work Value
    public function workValue($callback, $def = null) {

        //  Value
        $value = $def;

        //  Loop Each
        foreach($this->objects as $object) {

            //  Check for Null Object
            if($object) {

                //  Run
                $valueNow = call_user_func_array($callback, array($object, $this));

                //  Check for Null
                if (!is_null($valueNow)) {

                    //  Set
                    $value = $valueNow;
                }

                break;
            }
        }

        //  Return
        return $value;
    }
}