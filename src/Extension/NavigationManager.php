<?php namespace Ayedev\LaravelCore\Extension;

use Ayedev\LaravelCore\Decorator\NavigationDecorator;
use Ayedev\LaravelCore\Decorator\SimpleNavigationDecorator;

class NavigationManager
{
    /** @var string $_name */
    private $_name;

    /** @var string $_label */
    private $_label;

    /** @var string $_before_content */
    private $_before_content;

    /** @var string $_after_content */
    private $_after_content;

    /** @var int $_last_sort_order */
    private $_last_sort_order = 0;

    /** @var NavigationDecorator $_decorator */
    private $_decorator;

    /** @var Collection $_collection */
    private $_collection;

    /** @var array $_mergeWithDecorator */
    private $_mergeWithDecorator = array();


    /**
     * Get the Instance
     *
     * @param null $type
     * @return NavigationManager
     */
    public static function instance( $type = null )
    {
        //  Type
        $type = $type ?: 'default';

        //  Key
        $key = 'navigation.instance.' . $type;

        //  Check
        if( !app()->resolved( $key ) )
        {
            //  Create Instance
            app()->singleton( $key, function() use ( $type )
            {
                //  Return
                return new NavigationManager( $type );
            } );
        }

        //  Return
        return app( $key );
    }


    /**
     * NavigationManager constructor.
     *
     * @param string $name
     */
    public function __construct( $name )
    {
        //  Store the Name
        $this->_name = $name;

        //  Create Collection
        $this->_collection = new Collection();
    }


    /**
     * Set Label
     *
     * @param $label
     * @return $this
     */
    public function setLabel( $label )
    {
        //  Store
        $this->_label = $label;

        //  Return
        return $this;
    }

    /**
     * Get Label
     *
     * @return string
     */
    public function getLabel()
    {
        //  Return
        return $this->_label;
    }

    /**
     * Set Before Content
     *
     * @param $content
     * @return $this
     */
    public function setBeforeContent( $content )
    {
        //  Store
        $this->_before_content = $content;

        //  Return
        return $this;
    }

    /**
     * Get Before Content
     *
     * @return string
     */
    public function getBeforeContent()
    {
        //  Return
        return $this->_before_content;
    }

    /**
     * Set After Content
     *
     * @param $content
     * @return $this
     */
    public function setAfterContent( $content )
    {
        //  Store
        $this->_after_content = $content;

        //  Return
        return $this;
    }

    /**
     * Get After Content
     *
     * @return string
     */
    public function getAfterContent()
    {
        //  Return
        return $this->_after_content;
    }

    /**
     * Sort the Collection
     *
     * @return $this
     */
    public function sort()
    {
        //  Sort
        $this->_collection = $this->_collection->sort( function( NavigationItem $b, NavigationItem $a )
        {
            //  Return
            return ( $b->getSortOrder() > $a->getSortOrder() );
        } );

        //  Return
        return $this;
    }

    /**
     * Get Valid Items Only
     *
     * @param bool $use_plain
     * @return array
     */
    public function getValidItems( $use_plain = false )
    {
        //  Sort First
        $this->sort();

        //  Loop Each
        $items = $this->_collection->filter( function( NavigationItem $item ) use ( $use_plain )
        {
            //  Return
            return $item->isValidItem( $use_plain );
        } );

        //  Return
        return $items->all();
    }

    /**
     * Get All Items
     * @return array
     */
    public function getItems()
    {
        //  Sort First
        $this->sort();

        //  Return
        return $this->_collection->all();
    }


    /**
     * Set Config for Merge
     *
     * @param $mergeConfig
     * @param bool $append
     * @return $this
     */
    public function setForMerge( $mergeConfig, $append = true )
    {
        //  Store
        $this->_mergeWithDecorator = ( $append ? array_merge( $this->_mergeWithDecorator, $mergeConfig ) : $mergeConfig );

        //  Return
        return $this;
    }

    /**
     * Get Config for Merge
     *
     * @return array
     */
    public function getForMerge()
    {
        //  Return
        return $this->_mergeWithDecorator;
    }


    /**
     * @param $label
     * @param null $link
     * @param null $callback
     * @param int $sort_index
     * @param null $name
     * @return NavigationManager
     */
    public function add( $label, $link = null, $callback = null, $sort_index = null, $name = null )
    {
        //  Sort Order
        $sort_index = $sort_index ?: $this->_last_sort_order + 1;

        //  Check Name
        $name = $name ?: str_slug( $label );

        //  Create the Navigation Item
        $navItem = new NavigationItem( $name, $label, $link, $sort_index );

        //  Run Callback
        $callback && call_user_func_array( $callback, array( $navItem, $this ) );

        //  Check
        if( $this->_last_sort_order < $sort_index ) $this->_last_sort_order = $sort_index;

        //  Return
        return $this->addObject( $navItem );
    }

    /**
     * Add Navigation Item Object
     *
     * @param NavigationItem $navItem
     * @return $this
     */
    public function addObject( NavigationItem $navItem )
    {
        //  Store
        $this->_collection->add( $navItem );

        //  Return
        return $this;
    }

    /**
     * Get the Navigation Item
     *
     * @param $name
     * @return mixed
     */
    public function get( $name )
    {
        //  Fix Name
        $name = str_slug( $name );

        //  Search
        $found = array_first( $this->_collection->all(), function( $i, NavigationItem $navItem ) use ( $name )
        {
            //  Return
            return ( $navItem->getIdentifier() == $name );
        } );

        //  Return
        return $found;
    }

    /**
     * Check has Current
     *
     * @return bool
     */
    public function hasCurrent()
    {
        //  Has Current
        $hasCurrent = false;

        /**
         * Loop Each
         *
         * @var NavigationItem $item
         */
        foreach( $this->_collection->all() as $item )
        {
            //  Check
            if( $item->isCurrent() )
            {
                //  Set Current
                $hasCurrent = true;
                break;
            }
        }

        //  Return
        return $hasCurrent;
    }


    /**
     * Set Decorator
     *
     * @param NavigationDecorator $decorator
     * @return $this
     */
    public function setDecorator( NavigationDecorator $decorator )
    {
        //  Store
        $this->_decorator = $decorator;

        //  Return
        return $this;
    }

    /**
     * Get the Decorator
     *
     * @return NavigationDecorator
     */
    public function getDecorator()
    {
        //  Return
        return ( $this->_decorator ?: new SimpleNavigationDecorator() );
    }


    /**
     * To String
     *
     * @return mixed
     */
    public function __toString()
    {
        //  Return
        return $this->render();
    }

    /**
     * Render the Navigation
     * @param NavigationDecorator|null $decorator
     * @param array $merge
     * @param bool $force
     * @return mixed
     */
    public function render( NavigationDecorator $decorator = null, $merge = array(), $force = false )
    {
        //  Decorator
        $decorator = $decorator ?: $this->getDecorator();

        //  Return
        return $decorator->output( $this, $merge, $force );
    }
}