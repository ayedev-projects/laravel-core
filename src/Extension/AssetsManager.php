<?php namespace Ayedev\LaravelCore\Extension;

class AssetsManager {

    /** @var string $siteName */
    private $siteName;

    /** @var string $siteNameCompact */
    private $siteNameCompact;

    /** @var string $pageTitle */
    private $pageTitle;

    /** @var bool $titleLoadedFromView */
    private $titleLoadedFromView = false;

    /** @var array $registered */
    private $registered = array();

    /** @var array $collections */
    private $collections = array();

    /** @var boolean $useAsync */
    private $useAsync = false;

    /** @var array $enqueues */
    private $enqueues = array();

    /** @var array $metatags */
    private $metatags = array();

    /** @var array $callbacks */
    private $callbacks = array();

    /** @var bool $return_relative */
    private $return_relative = false;

    /** @var bool $init_done */
    private $init_done = false;

    /** @var \Closure $init_callback */
    private $init_callback = null;

    /** @var \Closure $ready_callback */
    private $ready_callback = null;

    /** @var array $htmlAttrs */
    private $htmlAttrs = array();

    /** @var array $bodyClasses */
    private $bodyClasses = array();

    /** @var array $uses */
    private $uses = array();

	/** @var array $fonts **/
	private $fonts = array();


    /** @var null $instance */
    static protected $instance = null;

    /** Header Location */
    const LOCATION_HEAD = 'head';

    /** Footer Location */
    const LOCATION_FOOTER = 'footer';

    /** Body Location */
    const LOCATION_BODY = 'body';

    /** Before Position */
    const POSITION_BEFORE = 'before';

    /** After Position */
    const POSITION_AFTER = 'after';


    /** Get Instance **/
    static public function instance()
	{
        //  Check
        if( !self::$instance )
        {
            //  Create Instance
            self::$instance = new static();
        }

        //  Return
        return self::$instance;
    }


    /** Constructor **/
    public function __construct()
	{
        //  Assign Site Compact Name
        $this->siteNameCompact = config( 'settings.site_name_compact' );

        //  Listen for Error Init
        capture( 'error.init', function()
	    {
            //  Do Init
            assets()->__doInit();

            //  Do Ready
            assets()->__doReady( null, false );
        } );

        //  Listen for Controller Init
        capture( 'controller.init', function()
	    {
            //  Do Init
            assets()->__doInit();
        } );

        //  Listen for Controller Ready
        capture( 'controller.ready', function( $controller )
	    {
	        //  Do Callback
			assets()->__doReady( $controller );
        } );
    }

    /**
     * Do Init
     *
     * @return $this
     */
    public function __doInit()
    {
        //  Check
        if( !$this->init_done )
        {
            //  Do Callback
            $this->init_callback && call_user_func_array( $this->init_callback, array( $this ) );

            //  Set Init Done
            $this->init_done = true;
        }

        //  Return
        return $this;
    }

    /**
     * Do Ready
     *
	 * @param mixed $controller
	 * @param bool $ignoreReady
     * @return $this
     */
	public function __doReady( $controller, $ignoreReady = false )
	{
		//	Check
		if( !$ignoreReady )
		{
			//	Run Callback
			$this->ready_callback && call_user_func_array( $this->ready_callback, array( $this, $controller ) );
		}

		//	Final Fonts Attrs
		$fontAttrs = array();

		//	Loop Each Fonts
		foreach( $this->fonts as $fontFamily => $fontVariations )
		{
			//	Append
			$fontAttrs[] = urlencode( $fontFamily ) . ( $fontVariations ? ':' . implode( ',', $fontVariations ) : '' );
		}

		//	Check
		if( $fontAttrs )
		{
			//	Enqueue Font
			$this->enqueueHead( 'google-fonts-css', 'http://fonts.googleapis.com/css?family=' . implode( '|', $fontAttrs ), null, null, -1 );
		}
	}


	/**
	 * Add Font Variation
	 *
	 * @param string $family
	 * @param string|mixed $variation
	 * @param bool $clear
	 * @return $this
	 */
	public function assignFontVariation( $family, $variation, $clear = false )
	{
		//	Check Exists
		if( !isset( $this->fonts[$family] ) )	 $this->fonts[$family] = array();

		//	Append the Variation
		if( $clear )	 $this->fonts[$family] = (array)$variation;
		else 	$this->fonts[$family] = array_unique( array_filter( array_merge( $this->fonts[$family], (array)$variation ) ) );

		//	Return
		return $this;
	}

	/**
	 * Remove Font Variation
	 *
	 * @param string $family
	 * @param string|mixed $variation
	 * @return $this
	 */
	public function removeFontVariation( $family, $variation )
	{
		//	Check
		if( isset( $this->fonts[$family] ) )
		{
			//	Variations
			$variations = $this->fonts[$family];

			//	Loop Each
			foreach( (array)$variation as $rV )
			{
				//	Search
				$sIndex = array_search( $rV, $variations );

				//	Check
				if( $sIndex !== false && $sIndex > -1 )
				{
					//	Clear
					unset( $variations[$sIndex] );
				}
			}

			//	Store Fresh Variations
			$this->fonts[$family] = array_values( $variations );
		}

		//	Return
		return $this;
	}

	/**
	 * Remove Font
	 *
	 * @param string|array $family
	 * @return $this
	 */
	public function removeFont( $family )
	{
		//	Loop Each
		foreach( (array)$family as $fm )
		{
			//	Check
			if( isset( $this->fonts[$fm] ) )
			{
				//	Remove
				unset( $this->fonts[$fm] );
			}
		}

		//	Return
		return $this;
	}


    /**
     * Add Uses
     *
     * @param $key
     * @return $this
     */
    public function addUses( $key )
    {
        //  Check
        if( !$this->uses( $key ) )
        {
            //  Store
            $this->uses[] = $key;
        }

        //  Return
        return $this;
    }

    /**
     * Check uses
     *
     * @param $key
     * @return bool
     */
    public function uses( $key )
    {
        //  Return
        return in_array( $key, $this->uses );
    }

    /**
     * Remove uses
     *
     * @param $key
     * @return $this
     */
    public function removeUses( $key )
    {
        //  Search
        $index = array_search( $key, $this->uses );

        //  Check
        if( $index > -1 )
        {
            //  Remove
            unset( $this->uses[$index] );
        }

        //  Return
        return $this;
    }


    /**
     * Set HTML Attribute
     *
     * @param $key
     * @param null $val
     * @return $this
     */
    public function setHTMLAttr($key, $val = null)
	{
	    //  Set
        $this->htmlAttrs[$key] = $val;

        //  Return
        return $this;
    }

    /**
     * Check Has HTML Attribute
     *
     * @param $key
     * @return bool
     */
    public function hasHTMLAttr($key)
	{
	    //  Return
        return (isset($this->htmlAttrs[$key]));
    }

    /**
     * Remove the HTML Attr
     *
     * @param $key
     * @return $this
     */
    public function removeHTMLAttr($key)
	{
	    //  Check
        if($this->hasHTMLAttr($key))
	    {
	        //  Remove
            unset($this->htmlAttrs[$key]);
        }

        //  Return
        return $this;
    }

    /**
     * Get HTML Attributes
     *
     * @return array
     */
    public function getHTMLAttrs()
	{
	    //  Return
        return $this->htmlAttrs;
    }

    /**
     * Print HTML Attributes
     *
     * @return mixed
     */
    public function htmlAttrs()
	{
	    //  Set Lang Attribute
        $this->setHTMLAttr( 'lang', app()->getLocale() );

        //  Print
        return _attrs_to_html($this->htmlAttrs);
    }


    /**
     * Add Body Class
     *
     * @param $class
     * @return $this
     */
    public function addBodyClass($class)
	{
	    //  Get Classes
        $classes = ( is_array( $class ) ? $class : explode( ' ', $class ) );

	    //  Loop Each
        foreach( $classes as $theClass )
        {
            //  Check
            if( !$this->hasBodyClass( $theClass ) )
            {
                //  Add
                $this->bodyClasses[] = $theClass;
            }
        }

        //  Return
        return $this;
    }

    /**
     * Check Has Body Class
     *
     * @param $class
     * @return bool
     */
    public function hasBodyClass($class)
	{
	    //  Return
        return (in_array($class, $this->bodyClasses));
    }

    /**
     * Remove the Body Class
     *
     * @param $class
     * @return $this
     */
    public function removeBodyClass($class)
	{
	    //  Check
        if($this->hasBodyClass($class))
	    {
	        //  Loop Each
            foreach($this->bodyClasses as $i => $c)
            {
                //  Check
                if($class == $c)
                {
                    //  Unset
                    unset($this->bodyClasses[$i]);

                    break;
                }
            }
        }

        //  Return
        return $this;
    }

    /**
     * Get Body Classes
     *
     * @return array
     */
    public function getBodyClasses()
	{
	    //  Return
        return $this->bodyClasses;
    }

    /**
     * Print Body Classes
     *
     * @return string
     */
    public function bodyClass()
	{
	    //  Check
        if(sizeof($this->bodyClasses) > 0)
        {
	        //  Print
            return ' class="' . implode(' ', $this->bodyClasses) . '"';
        }

        //  Return
        return '';
    }


    /**
     * Add Callback for Before Head
     *
     * @param $callback
     * @param null $prefix
     * @return AssetsManager
     */
    public function headBefore( $callback, $prefix = null )
	{
	    //  Return
        return $this->addCallback( $callback, ( $prefix ? $prefix . '_' : '' ) . self::LOCATION_HEAD, self::POSITION_BEFORE );
    }

    /**
     * Add Callback for After Head
     *
     * @param $callback
     * @param null $prefix
     * @return AssetsManager
     */
    public function headAfter( $callback, $prefix = null )
	{
	    //  Return
        return $this->addCallback( $callback, ( $prefix ? $prefix . '_' : '' ) . self::LOCATION_HEAD, self::POSITION_AFTER );
    }

    /**
     * Add Callback for Before Footer
     *
     * @param $callback
     * @param null $prefix
     * @return AssetsManager
     */
    public function footerBefore( $callback, $prefix = null )
	{
	    //  Return
        return $this->addCallback( $callback, ( $prefix ? $prefix . '_' : '' ) . self::LOCATION_FOOTER, self::POSITION_BEFORE );
    }

    /**
     * Add Callback for After Footer
     *
     * @param $callback
     * @param null $prefix
     * @return AssetsManager
     */
    public function footerAfter( $callback, $prefix = null )
	{
	    //  Return
        return $this->addCallback( $callback, ( $prefix ? $prefix . '_' : '' ) . self::LOCATION_FOOTER, self::POSITION_AFTER );
    }

    /**
     * Add Callback for Before Body
     *
     * @param $callback
     * @param null $prefix
     * @return AssetsManager
     */
    public function bodyBefore( $callback, $prefix = null )
	{
	    //  Return
        return $this->addCallback( $callback, ( $prefix ? $prefix . '_' : '' ) . self::LOCATION_BODY, self::POSITION_BEFORE );
    }

    /**
     * Add Callback for After Body
     *
     * @param $callback
     * @param null $prefix
     * @return AssetsManager
     */
    public function bodyAfter( $callback, $prefix = null )
	{
	    //  Return
        return $this->addCallback( $callback, ( $prefix ? $prefix . '_' : '' ) . self::LOCATION_BODY, self::POSITION_AFTER );
    }

    /**
     * Add Callback
     *
     * @param $callback
     * @param $location
     * @param $position
     * @return $this
     */
    public function addCallback( $callback, $location, $position )
	{
	    //  Check
        if( !isset( $this->callbacks[$location] ) ) $this->callbacks[$location] = array();

        //  Check
        if( !isset( $this->callbacks[$location][$position] ) )  $this->callbacks[$location][$position] = array();

        //  Append Callback
        $this->callbacks[$location][$position][] = $callback;

        //  Return
        return $this;
    }

    /**
     * Run Head Before Callbacks
     *
     * @param null $prefix
     * @return $this
     */
    function headBeforeRun( $prefix = null )
	{
	    //  Run Callbacks
        $this->runCallback( ( $prefix ? $prefix . '_' : '' ) . self::LOCATION_HEAD, self::POSITION_BEFORE );

        //  Return
        return $this;
    }

    /**
     * Run Head After Callbacks
     *
     * @param null $prefix
     * @return $this
     */
    function headAfterRun( $prefix = null )
	{
	    //  Run Callbacks
        $this->runCallback( ( $prefix ? $prefix . '_' : '' ) . self::LOCATION_HEAD, self::POSITION_AFTER );

        //  Return
        return $this;
    }

    /**
     * Run Footer Before Callbacks
     *
     * @param null $prefix
     * @return $this
     */
    function footerBeforeRun( $prefix = null )
	{
	    //  Run Callbacks
        $this->runCallback( ( $prefix ? $prefix . '_' : '' ) . self::LOCATION_FOOTER, self::POSITION_BEFORE );

        //  Return
        return $this;
    }

    /**
     * Run Footer After Callbacks
     *
     * @param null $prefix
     * @return $this
     */
    function footerAfterRun( $prefix = null )
	{
	    //  Run Callbacks
        $this->runCallback( ( $prefix ? $prefix . '_' : '' ) . self::LOCATION_FOOTER, self::POSITION_AFTER );

        //  Return
        return $this;
    }

    /**
     * Run Body Before Callbacks
     *
     * @param null $prefix
     * @return $this
     */
    function bodyBeforeRun( $prefix = null )
	{
	    //  Run Callbacks
        $this->runCallback( ( $prefix ? $prefix . '_' : '' ) . self::LOCATION_BODY, self::POSITION_BEFORE );

        //  Return
        return $this;
    }

    /**
     * Run Body After Callbacks
     *
     * @param null $prefix
     * @return $this
     */
    function bodyAfterRun( $prefix = null )
	{
	    //  Run Callbacks
        $this->runCallback( ( $prefix ? $prefix . '_' : '' ) . self::LOCATION_BODY, self::POSITION_AFTER );

        //  Return
        return $this;
    }

    /**
     * Run Callback
     *
     * @param $location
     * @param $position
     * @return $this
     */
    public function runCallback( $location, $position )
	{
	    //  Callbacks
        $callbacks = ( isset( $this->callbacks[$location] ) && isset( $this->callbacks[$location][$position] ) ? $this->callbacks[$location][$position] : array() );

        //  Loop Each
        foreach( $callbacks as $callback )
        {
            //  Run Callback
            call_user_func_array( $callback, array( $this ) );
        }

        //  Return
        return $this;
    }

    /**
     * Set Charset
     *
     * @param $charset
     * @return AssetsManager
     */
    public function setCharset( $charset )
	{
	    //  Return
        return $this->setMetaTag( 'charset', $charset, null, false );
    }

    /**
     * Set Site Name
     *
     * @param $siteName
     * @return $this
     */
    public function setSiteName( $siteName )
	{
	    //  Store
        $this->siteName = $siteName;

        //  Return
        return $this;
    }

    /**
     * Get Site Name
     *
     * @return string
     */
    public function getSiteName()
	{
	    //  Return
        return $this->siteName;
    }

    /**
     * Set Site Name Compact
     *
     * @param $name
     * @return $this
     */
    public function setSiteNameCompact( $name )
	{
	    //  Store
        $this->siteNameCompact = $name;

        //  Return
        return $this;
    }

    /**
     * Get Site Name Compact
     *
     * @return string
     */
    public function getSiteNameCompact()
	{
	    //  Return
        return $this->siteNameCompact;
    }

    /**
     * Set Title
     *
     * @param $title
     * @param bool|true $append
     * @return $this
     */
    public function setTitle( $title, $append = true )
	{
	    //  Check
        if( $title && !empty( $title ) )
        {
            //  Store
            $this->pageTitle = $title . ( $this->pageTitle && $append ? config( 'laravel-core.site_title_sep', ' : ' ) . $this->pageTitle : '' );
        }

        //  Return
        return $this;
    }

    /**
     * Get Title
     *
     * @param string|null $title
     * @return string
     */
    public function getTitle( $title = null )
	{
	    //  Separator
        $separator = config( 'laravel-core.site_title_sep', ' : ' );

        //  Check
        if( !$this->titleLoadedFromView )   $this->setTitle( getSection( 'page_title', '' ) );

        //  Return
        return ( $title ? $title . $separator : '' ) . ( $this->pageTitle ? $this->pageTitle . $separator : '' ) . $this->siteName;
    }

    /**
     * Store Callback for Init
     *
     * @param \Closure|null $callback
     * @return $this
     */
    public function init( \Closure $callback = null )
    {
        //  Store
        $this->init_callback = $callback;

        //  Return
        return $this;
    }

    /**
     * Store Callback for Ready
     *
     * @param \Closure|null $callback
     * @return $this
     */
    public function ready( \Closure $callback = null )
    {
        //  Store
        $this->ready_callback = $callback;

        //  Return
        return $this;
    }

    /**
     * Set OpenGraph Meta
     *
     * @param $key
     * @param $value
     * @param $prefix
     * @return AssetsManager
     */
    public function setOGMeta( $key, $value, $prefix = 'og:' )
	{
	    //  Return
        return $this->setMetaTag( $prefix . $key, $value, 'property' );
    }

    /**
     * Set Facebook OG Meta
     *
     * @param $key
     * @param $value
     * @return AssetsManager
     */
    public function setFacebookOGMeta( $key, $value )
	{
	    //  Return
        return $this->setOGMeta( $key, $value, 'fb:' );
    }

    /**
     * Set Twitter Card OG Meta
     *
     * @param $key
     * @param $value
     * @return AssetsManager
     */
    public function setTwitterOGMeta( $key, $value )
	{
	    //  Return
        return $this->setOGMeta( $key, $value, 'twitter:' );
    }

    /**
     * Set Schema OG Meta
     *
     * @param $key
     * @param $value
     * @return AssetsManager
     */
    public function setSchemaOGMeta( $key, $value )
	{
	    //  Return
        return $this->setMetaTag( $key, $value, 'itemprop' );
    }

    /**
     * Set Meta Tag
     *
     * @param $key
     * @param $value
     * @param string $attr_name
     * @param bool|true $content
     * @return $this
     */
    public function setMetaTag( $key, $value, $attr_name = 'name', $content = true )
	{
	    //  Check
        if( !$content && !$attr_name )  $attr_name = $key;

        //  Store
        $this->metatags[$key . '-' . $attr_name] = array(
            'key' => $key,
            'value' => $value,
            'name' => $attr_name,
            'content' => $content
        );

        //  Return
        return $this;
    }

    /**
     * Clear Meta Tag
     *
     * @param $key
     * @param string $attr_name
     * @return $this
     */
    public function clearMetaTag( $key, $attr_name = 'name' )
	{
	    //  Check
        if( isset( $this->metatags[$key . '-' . $attr_name] ) )    unset( $this->metatags[$key . '-' . $attr_name] );

        //  Return
        return $this;
    }

    /**
     * Set Site Info for OpenGraph
     *
     * @param $name
     * @param string $type
     * @return $this
     */
    public function setSiteOG( $name, $type = 'blog' )
	{
	    //  Write Meta
        $this->setOGMeta( 'site_name', $name );
        $this->setOGMeta( 'type', $type );

        //  Set Site Name
        $this->setSiteName( $name );

        //  Return
        return $this;
    }

    /**
     * Set Page Info for OpenGraph
     *
     * @param $title
     * @param null $desc
     * @param null $image
     * @param null $url
     * @param null $keywords
     * @return $this
     */
    public function setPageOG( $title, $desc = null, $image = null, $url = null, $keywords = null )
	{
	    //  Fix Description
        $desc = ( $desc ? substr( strip_tags( $desc ), 0, 120 ) : null );

        //  Check Desc
        if( $desc ) $this->setMetaTag( 'description', $desc );

        //  Check Keywords
        if( $keywords ) $this->setMetaTag( 'keywords', $keywords );

        //  Write OG Meta
        $this->setOGMeta( 'title', $title );
        if( $desc ) $this->setOGMeta( 'description', $desc );
        if( $image )  $this->setOGMeta( 'image', $image );
        if( $url )  $this->setOGMeta( 'url', $url );

        //  Write Twitter Card Meta
        $this->setTwitterOGMeta( 'card', 'summary' );
        $this->setTwitterOGMeta( 'title', $title );
        if( $desc ) $this->setTwitterOGMeta( 'description', $desc );
        if( $image ) $this->setTwitterOGMeta( 'image', $image );

        //  Write Schema Meta
        $this->setSchemaOGMeta( 'name', $title );
        if( $desc ) $this->setSchemaOGMeta( 'description', $desc );
        if( $image )    $this->setSchemaOGMeta( 'image', $image );

        //  Return
        return $this;
    }

    /**
     * Set Product Info for OpenGraph
     *
     * @param $price
     * @param $currency
     * @param $display
     */
    public function setProductOG( $price, $currency, $display )
	{
	    //  Change Site Type
        $this->setOGMeta( 'type', 'article' );

        //  Write OG Meta
        $this->setOGMeta( 'price:amount', $price );
        $this->setOGMeta( 'price:currency', $currency );

        //  Write Twitter Card Meta
        $this->setTwitterOGMeta( 'card', 'product' );
        $this->setTwitterOGMeta( 'label1', 'Price' );
        $this->setTwitterOGMeta( 'data1', $display );
    }

    /**
     * Set Twitter Publisher
     *
     * @param $publisher
     * @return $this
     */
    public function setTwitterPublisher( $publisher )
	{
	    //  Set
        $this->setTwitterOGMeta( 'site', '@' . $publisher );

        //  Return
        return $this;
    }

    /**
     * Set Twitter Author
     *
     * @param $author
     * @return $this
     */
    public function setTwitterAuthor( $author )
	{
	    //  Set
        $this->setTwitterOGMeta( 'creator', '@' . $author );

        //  Return
        return $this;
    }

    /**
     * Prepare Meta Tags
     *
     * @return string
     */
    public function prepareMetaTags()
	{
	    //  Output
        $output = array();

        //  Loop Each
        foreach( $this->metatags as $key => $metainfo )
        {
            //  Append
            $output[] = '<meta ' . $metainfo['name'] . '="' . ( $metainfo['content'] ? $metainfo['key'] : $metainfo['value'] ) . '"' . ( $metainfo['content'] ? ' content="' . $metainfo['value'] . '"' : '' ) . ' />';
        }

        //  Return
        return implode( "\n", $output );
    }

    /**
     * Mark flag to return relative path
     *
     * @return $this
     */
    public function getRelative()
    {
        //  Set
        $this->return_relative = true;

        //  Return
        return $this;
    }

    /**
     * Generate the generic path
     *
     * @param $path
     * @param bool $url
     * @return string
     */
    public function path( $path, $url = true )
	{
	    //  Prepare
        $output = ( $this->return_relative ? $path : ( $url ? asset( $path ) : public_path( $path ) ) );

        //  Clear Relative
        $this->return_relative = false;

	    //  Return
        return $output;
    }

    /**
     * Generate the asset path
     *
     * @param $path
     * @param bool $url
     * @return string
     */
    public function asset( $path, $url = true )
	{
	    //  Return
        return $this->path( config( 'laravel-core.assets_dir.base' ) . DIRECTORY_SEPARATOR . $path, $url );
    }

    /**
     * Generate Image Path
     *
     * @param $path
     * @param bool $url
     * @return string
     */
    public function image( $path, $url = true )
	{
	    //  Return
        return $this->asset( config( 'laravel-core.assets_dir.image' ) . DIRECTORY_SEPARATOR . $path, $url );
    }

    /**
     * Generate Style Path
     *
     * @param $path
     * @param bool $url
     * @return string
     */
    public function style( $path, $url = true )
	{
	    //  Return
        return $this->asset( config( 'laravel-core.assets_dir.style' ) . DIRECTORY_SEPARATOR . $path, $url );
    }

    /**
     * Generate Script Path
     *
     * @param $path
     * @param bool $url
     * @return string
     */
    public function script( $path, $url = true )
	{
	    //  Return
        return $this->asset( config( 'laravel-core.assets_dir.script' ) . DIRECTORY_SEPARATOR . $path, $url );
    }

    /**
     * Generate Plugin Path
     *
     * @param $path
     * @param bool $url
     * @return string
     */
    public function plugin( $path, $url = true )
	{
	    //  Return
        return $this->asset( config( 'laravel-core.assets_dir.plugin' ) . DIRECTORY_SEPARATOR . $path, $url );
    }

    /**
     * Generate Upload Path
     *
     * @param $path
     * @param bool|true $url
     * @return string
     */
    public function upload( $path, $url = true )
	{
	    //  Return
        return $this->path( config( 'laravel-core.assets_dir.upload' ) . DIRECTORY_SEPARATOR . $path, $url );
    }

    /**
     * Generate Cache Path
     *
     * @param $path
     * @param bool $url
     * @return mixed
     */
    public function cache( $path, $url = true )
	{
	    //  Return
        return $this->path( config( 'laravel-core.assets_dir.cache' ) . DIRECTORY_SEPARATOR . $path, $url );
    }

    /**
     * Generate Cache Path with Fixed
     *
     * @param $path
     * @param $append
     * @return mixed
     */
    public function cacheHandle( $path, $append = null )
	{
		//  Explode
		$explodes = explode( '.', $path );

		//  Return
		return $this->cache( $explodes[0] . ( $append ? '__' . str_ireplace( '=', '', base64_encode( $append ) ) : '' ) . '.' . $explodes[1] );
    }

    /**
     * Get Placeholder Image
     *
     * @param null $size
     * @param bool $url
     * @return string
     */
    public function placeholderImage( $size = null, $url = true )
    {
        //  Default
        $default = config( 'laravel-core.placeholder_image' );

        //  Return
        return $this->image( $size ? config( 'laravel-core.placeholder_image', $default ) : $default, $url );
    }

    /**
     * Register Collection
     *
     * @param $keyName
     * @param $names
     * @return $this
     */
    public function registerCollection( $keyName, $names )
	{
	    //  Filtered Names
        $filteredNames = array();

        //  Loop Each
        foreach( (array)$names as $key => $name )
        {
            //  The Name
            $theName = ( is_int( $key ) ? $name : $key );

            //  Check
            if( !is_int( $name ) && !isset( $this->registered[$theName] ) )
            {
                //  Register
                $this->register( $key, ( is_array( $name ) ? $name['path'] : $name ), ( is_array( $name ) && isset( $name['props'] ) ? $name['props'] : array() ) );
            }

            //  Store
            $filteredNames[] = $theName;
        }

        //  Store
        $this->collections[$keyName] = array_merge( isset( $this->collections[$keyName] ) ? $this->collections[$keyName] : array(), $filteredNames );

        //  Return
        return $this;
    }

    /**
     * Add Assets to Collection
     *
     * @param $keyName
     * @param $name
     * @param null $path
     * @param array $props
     * @return $this
     */
    public function addToCollection( $keyName, $name, $path = null, $props = array() )
	{
	    //  Check
        if( !isset( $this->collections[$keyName] ) )    $this->collections[$keyName] = array();

        //  Check
        if( $path )
        {
            //  Register
            $this->register( $name, $path, $props );
        }

        //  Append
        $this->collections[$keyName][] = $name;

        //  Return
        return $this;
    }

    /**
     * Enqueue Collection to Head
     *
     * @param $name
     * @return AssetsManager
     */
    public function enqueueCollectionHead( $name )
	{
	    //  Return
        return $this->enqueueCollection( $name, self::LOCATION_HEAD );
    }

    /**
     * Enqueue Collection to Footer
     *
     * @param $name
     * @return AssetsManager
     */
    public function enqueueCollectionFooter( $name )
	{
	    //  Return
        return $this->enqueueCollection( $name, self::LOCATION_FOOTER );
    }

    /**
     * Enqueue Collection
     *
     * @param $name
     * @param int|null $sort_index
     * @param $location
     * @return $this
     */
    public function enqueueCollection( $name, $sort_index = null, $location = 'unknown' )
	{
	    //  Get Names
        $names = ( isset( $this->collections[$name] ) ? $this->collections[$name] : array() );

        //  Loop Each
        foreach( $names as $name )
        {
            //  Enqueue
            $this->enqueue( $name, null, array(), array(), $sort_index, $location );
        }

        //  Return
        return $this;
    }

    /**
     * Unqueue Collection from Head
     *
     * @param $name
     * @return AssetsManager
     */
    public function unqueueCollectionHead( $name )
	{
	    //  Return
        return $this->unqueueCollection( $name, self::LOCATION_HEAD );
    }

    /**
     * Unqueue Collection from Footer
     *
     * @param $name
     * @return AssetsManager
     */
    public function unqueueCollectionFooter( $name )
	{
	    //  Return
        return $this->unqueueCollection( $name, self::LOCATION_FOOTER );
    }

    /**
     * Unqueue Collection
     *
     * @param $name
     * @param $location
     * @return $this
     */
    public function unqueueCollection( $name, $location = 'unknown' )
	{
	    //  Get Names
        $names = ( isset( $this->collections[$name] ) ? $this->collections[$name] : array() );

        //  Loop Each
        foreach( $names as $name )
        {
            //  Unqueue
            $this->unqueue( $name, $location );
        }

        //  Return
        return $this;
    }

    /**
     * Register Path
     *
     * @param $name
     * @param $path
     * @param array $props
     * @param bool $inline
     * @param null $location
     * @return $this
     */
    public function register( $name, $path, $props = array(), $inline = false, $location = null )
	{
		//	Check for Append
		if( !isset( $props['append'] ) )	$props['append'] = array();

        //  Check
        if( $this->useAsync )
        {
    		//	Check
    		if( !is_array( $path ) && substr( $path, -3 ) == '.js' && !isset( $props['append']['async'] ) )	$props['append']['async'] = null;

            //  Clear if requested
    		if( isset( $props['async'] ) && $props['async'] === false )	unset( $props['append']['async'] );
        }

	    //  Store Path
        $this->registered[$name] = array_merge( array(
            'path' => $path,
            'enqueued' => false,
            'inline' => $inline,
            'location' => $location
        ), ( is_array( $props ) ? $props : array() ) );

        //  Return
        return $this;
    }

    /**
     * Set Use Async
     *
     * @param boolean $flag
     * @return $this
     */
    public function setUseAsync( $flag = true )
    {
        //  Store
        $this->useAsync = $flag;

        //  Return
        return $this;
    }

    /**
     * Enqueue Asset on Head
     *
     * @param $name
     * @param null $path
     * @param array $deps
     * @param array $props
     * @param int|null $sort_index
     * @return $this
     */
    public function enqueueHead( $name, $path = null, $deps = array(), $props = array(), $sort_index = null )
	{
	    //  Return
        return $this->enqueue( $name, $path, $deps, $props, $sort_index, self::LOCATION_HEAD );
    }

    /**
     * Enqueue Asset on Footer
     *
     * @param $name
     * @param null $path
     * @param array $deps
     * @param array $props
     * @param int|null $sort_index
     * @return $this
     */
    public function enqueueFooter( $name, $path = null, $deps = array(), $props = array(), $sort_index = null )
	{
	    //  Return
        return $this->enqueue( $name, $path, $deps, $props, $sort_index, self::LOCATION_FOOTER );
    }

    /**
     * Enqueue Asset
     *
     * @param $name
     * @param null $path
     * @param array $deps
     * @param array $props
     * @param null $sort_index
     * @param string|null $location
     * @return $this
     */
    public function enqueue( $name, $path = null, $deps = array(), $props = array(), $sort_index = null, $location = 'unknown' )
	{
	    //  Found
        $found = ( isset( $this->registered[ $name ] ) ? $this->registered[$name] : null );

        //  Check
        if( !$found && $path )
        {
            //  Register
            $this->register( $name, $path, $props );

            //  Set Found
            $found = $this->registered[ $name ];
        }

        //  Check
        if( $found )
        {
            //  Check
            if( isset( $found['location'] ) && !is_null( $found['location'] ) ) $location = $found['location'];

            //  Check
            if( !isset( $this->enqueues[$location] ) )  $this->enqueues[$location] = array();

            //  Store Enqueue
            $this->enqueues[$location][$name] = array(
				'sort_index' => ( $sort_index ?: sizeof( $this->enqueues[$location] ) ),
                'deps' => ( $deps ? (array)$deps : array() )
            );
        }

        //  Return
        return $this;
    }

    /**
     * Unqueue from Head
     *
     * @param $name
     * @return AssetsManager
     */
    public function unqueueHead( $name )
	{
	    //  Return
        return $this->unqueue( $name, self::LOCATION_HEAD );
    }

    /**
     * Unqueue from Footer
     *
     * @param $name
     * @return AssetsManager
     */
    public function unqueueFooter( $name )
	{
	    //  Return
        return $this->unqueue( $name, self::LOCATION_FOOTER );
    }

    /**
     * Remove from Queue
     *
     * @param $name
     * @param string $location
     * @return $this
     */
    public function unqueue( $name, $location = 'unknown' )
	{
	    //  Check
        if( isset( $this->enqueues[$location] ) && isset( $this->enqueues[$location][$name] ) )
	{            //  Remove
            unset( $this->enqueues[$location][$name] );
        }

        //  Return
        return $this;
    }

    /**
     * Prepare Head Output
     *
     * @return string
     */
    public function prepareHead()
	{
	    //  Return
        return $this->prepare( self::LOCATION_HEAD );
    }

    /**
     * Prepare Footer Output
     *
     * @return string
     */
    public function prepareFooter()
	{
	    //  Return
        return $this->prepare( self::LOCATION_FOOTER );
    }

    /**
     * Prepare Output
     *
     * @param string $location
     * @return string
     */
    public function prepare( $location )
	{
	    //  Output
        $output = array();

        //  Get Names
        $names = ( isset( $this->enqueues[$location] ) ? $this->enqueues[$location] : array() );

		//	Order by Sort Index
		uasort( $names, function( $b, $a )
		{
			//	Return
			return $b['sort_index'] > $a['sort_index'];
		} );

        //  Loop Each
        foreach( $names as $name => $data )
        {
            //  Result
            $result = $this->handle( $name, $data );

            //  Handle
            if( $result )   $output = array_merge( $output, $result );
        }

        //  Return
        return implode( "\n", $output );
    }

    /**
     * Write the Enqueue Handle Head
     * @param $name
     * @return string
     */
    public function writeHandleHead( $name )
	{
	    //  Return
        return $this->writeHandle( $name, self::LOCATION_HEAD );
    }

    /**
     * Write the Enqueue Handle Footer
     * @param $name
     * @return string
     */
    public function writeHandleFooter( $name )
	{
	    //  Return
        return $this->writeHandle( $name, self::LOCATION_FOOTER );
    }

    /**
     * Write the Enqueue Handle
     *
     * @param $name
     * @param $location
     * @return string
     */
    public function writeHandle( $name, $location )
	{
	    //  Data
        $data = ( isset( $this->enqueues[$location] ) && isset( $this->enqueues[$location][$name] ) ? $this->enqueues[$location][$name] : null );

        //  Output
        $output = ( $data ? $this->handle( $name, $data ) : null );

        //  Return
        return ( $output ? implode( "\n", $output ) : '' );
    }

    /**
     * Handle the Enqueue
     *
     * @param $name
     * @param array $data
     * @return string
     */
    public function handle( $name, $data = null )
	{
	    //  Found
        $found = ( isset( $this->registered[ $name ] ) ? $this->registered[$name] : null );

        //  Check
        if( $found )
        {
            //  Check
            if( $found['enqueued'] === false )
            {
                //  Output
                $output = array();

                //  Set Enqueued
                $this->registered{$name}['enqueued'] = true;

                //  Deps Resolved
                $depsResolved = true;

                //  Dependencies
                $deps = ( $data && isset( $data['deps'] ) ? $data['deps'] : array() );

                //  Resolved Deps
                $resolvedDeps = array();

                //  Loop Each
                foreach( $deps as $dep )
                {
                    //  Check
                    if( isset( $this->registered[$dep] ) )
                    {
                        //  Store
                        $resolvedDeps[] = $dep;
                    }
                    else if( isset( $this->collections[$dep] ) )
                    {
                        //  Store
                        $resolvedDeps = array_merge( $resolvedDeps, $this->collections[$dep] );
                    }
                }

                //  Loop Each
                foreach( $resolvedDeps as $dep )
                {
                    //  Dep Result
                    $depResult = $this->handle( $dep, null );

                    //  Check
                    if( is_array( $depResult ) )
                    {
                        //  Handle
                        $output = array_merge( $output, $depResult );
                    } else {

                        //  Deps Not Resolved
                        $depsResolved = false;
                    }
                }

                //  Prepare Output
                if( $depsResolved )
				{
					//	Get Write Output
					$wOutput = $this->write( $name, $found['path'] );

					//	Store
					$output[] = $wOutput;
				}

                //  Return
                return $output;
            } else {

                //  Return
                return array();
            }
        }

        //  Return
        return null;
    }

    /**
     * Return Write
     *
     * @param $name
     * @param $path
     * @return string
     */
    public function write( $name, $path )
	{
	    //  Props
        $props = ( isset( $this->registered[$name] ) ? $this->registered[$name] : array() );

        //  Is Inline
        $isInline = ( isset( $props['inline'] ) ? $props['inline'] : false );

        //  Is JS
        $isJS = ( !is_array( $path ) && stripos( $path, '.js' ) > -1 ? true : false );

        //  Is CSS
        $isCSS = ( !is_array( $path ) && stripos( $path, '.css' ) > -1 ? true : false );

        //  Check
        if( !$isJS && !$isCSS && !is_array( $path ) && stripos( $path, 'fonts.' ) > -1 )    $isJS = false;
        if( !$isJS && !$isCSS && substr( $name, -3 ) == '-js' )    $isJS = true;
        if( !$isJS && !$isCSS && substr( $name, -4 ) == '-css' )    $isJS = false;

        //  Check
        if( isset( $props['is_script'] ) )  $isJS = (bool)$props['is_script'];

        //  Is Image
        $isImage = ( is_string( $path ) && !$isInline && preg_match( '/\.(jpg|jpeg|png|gif|bmp|ico)/i', $path ) );

        //  URL
        $url = ( !is_array( $path ) ? $path : null );

        //  Check
        if( $url && stripos( $url, '://' ) === false )
        {
            //  Check for Image
            if( $isImage )
            {
                //  Set URL
                $url = $this->image( $url );
            }
            else if( substr( $url, 0, 7 ) != '/build/' )
            {
                //  Check for JS
                if( $isJS ) $url = $this->script( $url );
                else    $url = $this->style( $url );
            } else {

                //  Prepare URL
                $url = url( $url );
            }
        }

        //  HTML
        $html = $this->makeTagLine( $name, $url, $isJS, @$props['ie'], @$props['rel'], @$props['append'], $isInline, $path, @$props['key'] );

        //  Return
        return $html;
    }

	/**
	 * Make the Tag Line
	 *
	 * @return string
	 */
	protected function makeTagLine( $name, $url, $is_script = false, $ie = false, $rel = null, $append = null, $is_inline = false, $path = null, $key = null )
	{
		//	Html
		$html = null;

		//  Check
        if( !$is_script )
        {
            //  Check
            if( $is_inline )
            {
                //  Set Inline
                $html = '<style id="asset-' . $name . '" type="text/css" rel="stylesheet"' . $this->_prepareAppend( @$append ) . '>' . $path . '</style>';
            } else {

                //  Set Link
                $html = '<link id="asset-' . $name . '" href="' . $url . '" ' . ( $rel ? 'rel="' . $rel . '"' : 'rel="stylesheet" type="text/css"' ) . $this->_prepareAppend( @$append ) . ' />';
            }
        } else {

            //  Set Script
            $html = '<script id="asset-' . $name . '" type="text/javascript"' . ( !$is_inline ? ' src="' . $url . '"' : '' ) . $this->_prepareAppend( @$append ) . '>' . ( $is_inline ? ( is_array( $path ) ? 'var ' . $key . ' = ' . json_encode( $path ) . ';' : $path ) : '' ) . '</script>';
        }

        //  Check for IE
        if( $ie )	$html = '<!--[if ' . ( $ie === true ? 'IE' : $ie ) . ']>' . $html . '<![endif]-->';

		//	Return
		return $html;
	}

    /**
     * Prepare Append
     *
     * @param $append
     * @return string
     */
    protected function _prepareAppend( $append )
    {
        //  Return
		return " " . _attrs_to_html( $append );
    }
}
