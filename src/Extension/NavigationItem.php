<?php namespace Ayedev\LaravelCore\Extension;

class NavigationItem extends \stdClass
{
    /** @var string $_identifier */
    private $_identifier;

    /** @var array $_attrs */
    private $_attrs = array();

    /** @var array $_props */
    private $_props = array();

    /** @var array $_classes */
    private $_classes = array();

    /** @var array $_anchor_classes */
    private $_anchor_classes = array();

    /** @var array $_icon */
    private $_icon = array();

    /** @var bool $_hasSub */
    private $_hasSub = false;

    /** @var null $_page_type */
    private $_page_type = null;

    /** @var array $_patterns */
    private $_patterns = array();

    /** @var null $_is_current */
    private $_is_current = null;

    /** @var string $_before_content */
    private $_before_content;

    /** @var string $_after_content */
    private $_after_content;


    /**
     * NavigationItem constructor.
     *
     * @param $name
     * @param $label
     * @param null $link
     * @param int $sort_order
     */
    public function __construct( $name, $label, $link = null, $sort_order = 0 )
    {
        //  Store Name
        $this->_identifier = $name;

        //  Store Label
        $this->setLabel( $label );

        //  Store Link
        $this->setLink( $link );

        //  Store Sort Order
        $this->setSortOrder( $sort_order );
    }

    /**
     * Get Identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        //  Return
        return $this->_identifier;
    }

    /**
     * Set Page Type
     *
     * @param $type
     * @return $this
     */
    public function setPageType( $type )
    {
        //  Store
        $this->_page_type = $type;

        //  Return
        return $this;
    }

    /**
     * Get Page Type
     *
     * @return string|null
     */
    public function getPageType()
    {
        //  Return
        return $this->_page_type;
    }


    /**
     * Set the Icon
     *
     * @param $icon_class
     * @param string $position
     * @param null $content
     * @return $this
     */
    public function setIcon( $icon_class, $position = 'left', $content = null )
    {
        //  Store
        $this->_icon = array(
            'class' => $icon_class,
            'position' => $position,
            'content' => $content
        );

        //  Return
        return $this;
    }

    /**
     * Remove the Icon
     *
     * @return $this
     */
    public function removeIcon()
    {
        //  Clear
        $this->_icon = array();

        //  Return
        return $this;
    }

    /**
     * Get the Icon Info
     *
     * @return array
     */
    public function getIcon()
    {
        //  Return
        return $this->_icon;
    }


    /**
     * Set Before Content
     *
     * @param $content
     * @return $this
     */
    public function setBeforeContent( $content )
    {
        //  Store
        $this->_before_content = $content;

        //  Return
        return $this;
    }

    /**
     * Get Before Content
     *
     * @return string
     */
    public function getBeforeContent()
    {
        //  Return
        return $this->_before_content;
    }

    /**
     * Set After Content
     *
     * @param $content
     * @return $this
     */
    public function setAfterContent( $content )
    {
        //  Store
        $this->_after_content = $content;

        //  Return
        return $this;
    }

    /**
     * Get After Content
     *
     * @return string
     */
    public function getAfterContent()
    {
        //  Return
        return $this->_after_content;
    }


    /**
     * Add the Class
     *
     * @param $class
     * @return $this
     */
    public function addClass( $class )
    {
        //  Store
        if( !in_array( $class, $this->_classes ) )  $this->_classes[] = $class;

        //  Return
        return $this;
    }

    /**
     * Check has Class
     *
     * @param $class
     * @return bool
     */
    public function hasClass( $class )
    {
        //  Return
        return ( array_search( $class, $this->_classes ) > -1 );
    }

    /**
     * Remove the Class
     *
     * @param $class
     * @return $this
     */
    public function removeClass( $class )
    {
        //  Search
        $index = array_search( $class, $this->_classes );

        //  Check
        if( $index > -1 )
        {
            //  Remove
            unset( $this->_classes[$index] );
        }

        //  Return
        return $this;
    }

    /**
     * Get Classes
     *
     * @return array
     */
    public function getClasses()
    {
        //  Return
        return $this->_classes;
    }


    /**
     * Add the Anchor Class
     *
     * @param $class
     * @return $this
     */
    public function addAnchorClass( $class )
    {
        //  Store
        if( !in_array( $class, $this->_anchor_classes ) )  $this->_anchor_classes[] = $class;

        //  Return
        return $this;
    }

    /**
     * Check has Anchor Class
     *
     * @param $class
     * @return bool
     */
    public function hasAnchorClass( $class )
    {
        //  Return
        return ( array_search( $class, $this->_anchor_classes ) > -1 );
    }

    /**
     * Remove the Anchor Class
     *
     * @param $class
     * @return $this
     */
    public function removeAnchorClass( $class )
    {
        //  Search
        $index = array_search( $class, $this->_anchor_classes );

        //  Check
        if( $index > -1 )
        {
            //  Remove
            unset( $this->_anchor_classes[$index] );
        }

        //  Return
        return $this;
    }

    /**
     * Get Anchor Classes
     *
     * @return array
     */
    public function getAnchorClasses()
    {
        //  Return
        return $this->_anchor_classes;
    }


    /**
     * Set the Required Permission
     *
     * @param $permission
     * @return NavigationItem
     */
    public function setPermission( $permission )
    {
        //  Return
        return $this->setProp( 'permission', $permission );
    }

    /**
     * Get Permission
     *
     * @return string|null
     */
    public function getPermission()
    {
        //  Return
        return $this->getProp( 'permission' );
    }

    /**
     * Set the Validation Callback
     *
     * @param $callback
     * @return NavigationItem
     */
    public function setValidationCallback( $callback )
    {
        //  Return
        return $this->setProp( 'validation_callback', $callback );
    }

    /**
     * Get the Validation Callback
     *
     * @return string|null
     */
    public function getValidationCallback()
    {
        //  Return
        return $this->getProp( 'validation_callback' );
    }

    /**
     * Check Passes Permission
     *
     * @return bool
     */
    public function passesPermission()
    {
        //  Return
        return ( !$this->getPermission() || ( isLoggedIn() && user()->canAny( $this->getPermission() ) ) );
    }

    /**
     * Check is Valid Item
     *
     * @param bool $use_plain
     * @return bool|mixed
     */
    public function isValidItem( $use_plain = false )
    {
        //  Is Valid
        $isValid = $this->passesPermission();

        //  Check
        if( $this->getValidationCallback() )
        {
            //  Run
            $isValid = call_user_func_array( $this->getValidationCallback(), array( $this, $use_plain ) );
        }

        //  Return
        return $isValid;
    }

    /**
     * Set ID
     *
     * @param $id
     * @return NavigationItem
     */
    public function setID( $id )
    {
        //  Return
        return $this->setProp( 'id', $id );
    }

    /**
     * Get ID
     *
     * @return string
     */
    public function getID()
    {
        //  Return
        return $this->getProp( 'id' );
    }

    /**
     * Set Label
     *
     * @param $label
     * @return NavigationItem
     */
    public function setLabel( $label )
    {
        //  Return
        return $this->setProp( 'label', $label );
    }

    /**
     * Get Label
     *
     * @return string
     */
    public function getLabel()
    {
        //  Return
        return $this->getProp( 'label' );
    }

    /**
     * Set Link
     *
     * @param $link
     * @return NavigationItem
     */
    public function setLink( $link )
    {
        //  Return
        return $this->setProp( 'link', $link );
    }

    /**
     * Get Link
     *
     * @return string
     */
    public function getLink()
    {
        //  Return
        return $this->getProp( 'link', 'javascript:void(0);' );
    }

    /**
     * Set Sort Order
     *
     * @param $order
     * @return NavigationItem
     */
    public function setSortOrder( $order )
    {
        //  Return
        return $this->setProp( 'sort_order', $order );
    }

    /**
     * Get the Sort Order
     *
     * @return int
     */
    public function getSortOrder()
    {
        //  Return
        return $this->getProp( 'sort_order', 0 );
    }


    /**
     * Set Attribute
     *
     * @param $key
     * @param null $val
     * @param bool $append
     */
    public function setAttr( $key, $val = null, $append = true )
    {
        //  Store
        $this->_attrs[$key] = ( $append && $this->hasAttr( $key ) ? $this->getAttr( $key ) . ' ' : '' ) . $val;
    }

    /**
     * Get Attribute
     *
     * @param $key
     * @param null $def
     * @return mixed|null
     */
    public function getAttr( $key, $def = null )
    {
        //  Check
        if( $this->hasAttr( $key ) )
        {
            //  Return
            return $this->_attrs[$key];
        }

        //  Return
        return $def;
    }

    /**
     * Check Attribute
     *
     * @param $key
     * @return bool
     */
    public function hasAttr( $key )
    {
        //  Return
        return ( isset( $this->_attrs[$key] ) );
    }

    /**
     * Remove Attribute
     *
     * @param $key
     * @return $this
     */
    public function removeAttr( $key )
    {
        //  Check
        if( $this->hasAttr( $key ) )
        {
            //  Remove
            unset( $this->_attrs[$key] );
        }

        //  Return
        return $this;
    }

    /**
     * Get All Attributes
     *
     * @return array
     */
    public function getAllAttrs()
    {
        //  Return
        return $this->_attrs;
    }

    /**
     * Get Final Attributes
     *
     * @return array
     */
    public function getFinalAttrs()
    {
        //  Core Attributes
        $coreAttributes = array(
            'href' => $this->getLink()
        );

        //  Return
        return array_merge( $coreAttributes, $this->_attrs );
    }


    /**
     * Add Match Pattern
     *
     * @param $pattern
     * @return $this
     */
    public function addMatchPattern( $pattern )
    {
        //  Store
        $this->_patterns[] = $pattern;

        //  Return
        return $this;
    }

    /**
     * Clear Match Patterns
     *
     * @return $this
     */
    public function clearMatchPatterns()
    {
        //  Clear
        $this->_patterns = array();

        //  Return
        return $this;
    }

    /**
     * Check is Current Active
     *
     * @return bool
     */
    public function isCurrent()
    {
        //  Check
        if( !is_null( $this->_is_current ) )    return $this->_is_current;

        //  Is Current
        $is_current = false;

        //  Current URL
        $currentURL = current_url();

        //  Current Path
        $currentPath = current_path();

        //  Match Current URL
        if( $currentURL == $this->getLink() )   $is_current = true;

        //  Check Page Type
        if( get_pagetype() == $this->getPageType() )    $is_current = true;

        //  Check
        if( !$is_current )
        {
            //  Loop Each Patterns
            foreach( $this->_patterns as $pattern )
            {
                //  Try
                if( preg_match( $pattern, $currentPath ) )
                {
                    //  Set Current
                    $is_current = true;
                    break;
                }
            }
        }

        //  Check
        if( !$is_current && $this->hasSub() )
        {
            //  Set Current
            //$is_current = $this->getSub()->hasCurrent();
        }

        //  Store
        $this->_is_current = $is_current;

        //  Return
        return $is_current;
    }


    /**
     * Set Prop
     *
     * @param $key
     * @param $val
     * @return $this
     */
    public function setProp( $key, $val )
    {
        //  Store
        $this->_props[$key] = $val;

        //  Return
        return $this;
    }

    /**
     * Get Prop
     *
     * @param $key
     * @param null $def
     * @return mixed|null
     */
    public function getProp( $key, $def = null )
    {
        //  Check
        if( $this->hasProp( $key ) )
        {
            //  Return
            return $this->_props[$key];
        }

        //  Return
        return $def;
    }

    /**
     * Check Prop Exists
     *
     * @param $key
     * @return bool
     */
    public function hasProp( $key )
    {
        //  Return
        return ( isset( $this->_props[$key] ) );
    }

    /**
     * Remove Prop
     *
     * @param $key
     * @return $this
     */
    public function removeProp( $key )
    {
        //  Check
        if( $this->hasProp( $key ) )
        {
            //  Remove
            unset( $this->_props[$key] );
        }

        //  Return
        return $this;
    }

    /**
     * Get All Props
     *
     * @return array
     */
    public function getAllProp()
    {
        //  Return
        return $this->_props;
    }


    /**
     * Get the Sub Navigation
     *
     * @return NavigationManager
     */
    public function getSub()
    {
        //  Set has Sub
        $this->_hasSub = true;

        //  Return
        return navigation( $this->_identifier. '.sub' );
    }

    /**
     * Check Sub Navigation Exists
     *
     * @return bool
     */
    public function hasSub()
    {
        //  Return
        return $this->_hasSub;
    }

    /**
     * Get the Sub Navigation Items Validation Count
     */
    public function getSubValidCount()
    {
        //  Count
        $count = 0;

        //  Get Sub
        $sub = ( $this->hasSub() ? $this->getSub() : null );

        //  Check
        if( $sub )
        {
            //  Get Count
            $count = sizeof( $sub->getValidItems() );
        }

        //  Return
        return $count;
    }
}