<?php namespace Ayedev\LaravelCore\Core;

class Closure
{
	/** @var \Closure $closure */
	private $closure;


    /**
     * Make the Closure
     *
     * @param \Closure $closure
     * @param null $bindTo
     * @return static
     */
	public static function make( \Closure $closure, $bindTo = null )
	{
		//	Return
		return new static( $closure, $bindTo );
	}


    /**
     * Construct
     *
     * @param \Closure $closure
     * @param null $bindTo
     */
	public function __construct( \Closure $closure, $bindTo = null )
	{
		//	Check
		if( $bindTo )
		{
			//	Change Closure
			$closure = $closure->bindTo( $bindTo, $bindTo );
		}

		//	Store
		$this->closure = $closure;
	}

	/**
	 * Get the Closure
	 */
	public function getClosure()
	{
		//	Return
		return $this->closure;
	}

    /**
     * Call the Closure
     *
     * @param null $args
     * @return mixed|null
     */
	public function call( $args = null )
	{
		//	Fix
		$args || $args = array();

		//	Check
		if( sizeof( func_get_args() ) > 1 )	$args = func_get_args();

		//	Return
		return ( $this->closure ? call_user_func_array( $this->closure, $args ) : null );
	}

	/**
	 * Handle Invoke
	 */
	public function __invoke()
	{
		//	Return
		return $this->call( func_get_args() );
	}
}