<?php namespace Ayedev\LaravelCore\Core;

use Swift_Message;

class Mailer extends \Illuminate\Mail\Mailer
{
    /**
     * Create the Message
     *
     * @return EmailMessage
     */
    protected function createMessage()
    {
        //  Create Message
        $message = new EmailMessage( new Swift_Message );

        // If a global from address has been specified we will set it on every message
        // instances so the developer does not have to repeat themselves every time
        // they create a new message. We will just go ahead and push the address.
        if (! empty($this->from['address'])) {
            $message->from($this->from['address'], $this->from['name']);
        }

        return $message;
    }

    /**
     * Core Extension
     */
    public function _core()
    {
        //  Set Container
        $this->setContainer( app() );

        //  Check for Queue Bound
        if( app()->bound( 'queue' ) )
        {
            //  Set Queue
            $this->setQueue( app( 'queue' ) );
        }

        //  Read Sender
        $from = config( 'mail.from' );

        //  Check
        if( is_array( $from ) && isset( $from['address'] ) )
        {
            //  Assign Always From
            $this->alwaysFrom( $from['address'], $from['name'] );
        }

        //  Read Receiver
        $to = config( 'mail.to' );

        //  Check
        if( is_array( $to ) && isset( $to['address'] ) )
        {
            //  Assign Always To
            $this->alwaysTo( $to['address'], $to['name'] );
        }
    }
}
