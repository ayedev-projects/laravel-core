<?php namespace Ayedev\LaravelCore\Core;

use Illuminate\Routing\Redirector as BaseRedirector;

class Redirector extends BaseRedirector {

    /** Override Create Redirect **/
    protected function createRedirect( $path, $status, $headers ) {

        //  Create Instance
        $redirect = new RedirectResponse( $path, $status, $headers );

        //  Assign Session
        if( isset( $this->session ) )   $redirect->setSession($this->session);

        //  Assign Request
        $redirect->setRequest( $this->generator->getRequest() );

        //  Return
        return $redirect;
    }
}