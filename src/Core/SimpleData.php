<?php namespace Ayedev\LaravelCore\Core;

use Ayedev\LaravelCore\Traits\DataTrait;

class SimpleData implements \ArrayAccess
{
    use DataTrait;


    /**
     * SimpleData constructor.
     *
     * @param null $data
     */
    public function __construct( $data = null )
    {
        //  Check
        if( $data ) $this->setData( $data );
    }
}