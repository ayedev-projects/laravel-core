<?php namespace Ayedev\LaravelCore\Core;

use Gregwar\Image\Image;
use Illuminate\Mail\Message;
use Swift_Attachment;
use Swift_Image;
use Ayedev\LaravelCore\Traits\MailInlinerTrait;

class EmailMessage extends Message
{
    use MailInlinerTrait;

    /**
     * Attach File
     *
     * @param $path
     * @param string $disposition
     * @return $this
     */
    public function attachFile( $path, $disposition = 'inline' )
    {
        //  Fix Path
        if( basename( $path ) == $path )    $path = assets()->asset( $path, false );

        //  Attachment
        $attachment = Swift_Attachment::fromPath( $path )->setDisposition( $disposition );

        //  Attach File
        $this->swift->attach( $attachment );

        //  Return
        return $this;
    }

    /**
     * Embed Image
     *
     * @param $path
     * @param bool $make_path
     * @param bool $optimize
     * @param int $quality
     * @param null $disposition
     * @return string
     */
    public function embedImage( $path, $make_path = false, $optimize = false, $quality = null, $disposition = null )
    {
        //  Fix Path
        if( basename( $path ) == $path || $make_path )    $path = assets()->image( $path, false );

        //  Check
        if( $optimize )
        {
            //  File Size
            $fileSize = filesize( $path );

            //  Check
            if( $fileSize > 800 )
            {
                //  Cache Path
                $cache_path = storage_path( 'mail_cache/optimized_' . basename( $path ) );

                //  Check
                if( !file_exists( $cache_path ) )
                {
                    //  Check
                    if( !$quality )
                    {
                        //  Quality
                        $quality = ( $fileSize > 5500 ? 35 : 60 );
                    }

                    //  Open Image
                    $img = openImage( $path );

                    //  Create Directory is not exists
                    if( !file_exists( dirname( $cache_path ) ) )    mkdir( dirname( $cache_path ), 0777, true );

                    //  Save
                    $img->save( $cache_path, pathinfo( $cache_path, PATHINFO_EXTENSION ), $quality );
                }

                //  Assign
                $path = $cache_path;
            }
        }

        //  Attachment
        $image = Swift_Image::fromPath( $path );

        //  Check
        if( $disposition )  $image->setDisposition( $disposition );

        //  Return
        return $this->swift->embed( $image );
    }
}
