<?php namespace Ayedev\LaravelCore\Core;

use Illuminate\Http\RedirectResponse as BaseRedirectResponse;

class RedirectResponse extends BaseRedirectResponse
{
    /**
     * Add Message
     *
     * @param $type
     * @param $msg
     * @param array $points
     * @return BaseRedirectResponse
     */
    public function addMessage( $type, $msg, $points = array() ) {

        //  Return
        return addMessage( $this, $type, $msg, $points );
    }

    /**
     * Add Success Message
     * @param $msg
     * @param array $points
     * @return BaseRedirectResponse
     */
    public function successMsg( $msg, $points = array() ) {

        //  Return
        return addSuccessMessage( $this, $msg, $points );
    }

    /**
     * Add Error Message
     *
     * @param $msg
     * @param array $points
     * @return BaseRedirectResponse
     */
    public function errorMsg( $msg, $points = array() ) {

        //  Return
        return addErrorMessage( $this, $msg, $points );
    }

    /**
     * Add Warning Message
     *
     * @param $msg
     * @param array $points
     * @return BaseRedirectResponse
     */
    public function warningMsg( $msg, $points = array() ) {

        //  Return
        return addWarningMessage( $this, $msg, $points );
    }

    /**
     * Add Info Message
     *
     * @param $msg
     * @param array $points
     * @return BaseRedirectResponse
     */
    public function infoMsg( $msg, $points = array() ) {

        //  Return
        return addInfoMessage( $this, $msg, $points );
    }
}