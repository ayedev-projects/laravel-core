<?php namespace Ayedev\LaravelCore;

//  Define
define( 'LARAVEL_CORE_VENDOR_PATH', dirname( dirname( __FILE__ ) ) . DIRECTORY_SEPARATOR );

use Illuminate\Support\ServiceProvider;
use Ayedev\LaravelCore\Core\Mailer;
use Ayedev\LaravelCore\Core\Redirector;
use Ayedev\LaravelCore\Extension\IconLibrary;

class LaravelCoreProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //  Load Helper Files
        require_once LARAVEL_CORE_VENDOR_PATH . 'helpers/helper.php';
        require_once LARAVEL_CORE_VENDOR_PATH . 'helpers/session.php';
        require_once LARAVEL_CORE_VENDOR_PATH . 'helpers/router.php';

        //  Extend Redirect
        $this->app->singleton( 'redirect', function ( $app )
        {
            //  Create Instance
            $redirector = new Redirector( $app->make( 'url' ) );

            //  Check
            if( isset( $app['session.store'] ) )    $redirector->setSession( $app['session.store'] );

            //  Return
            return $redirector;
        } );

        //  Bind the Icon Library
        $this->app->singleton( 'icons', function()
        {
            //  Return
            return new IconLibrary();
        } );

        //  Add Custom Swift Mailer Library
        $this->app->singleton( 'custom_mailer', function( $app )
        {
            //  Instance
            $mailer = new Mailer( $app['view'], $app['swift.mailer'], $app['events'] );

            //  Run Core
            $mailer->_core();

            //  Return
            return $mailer;
        } );

        //  Extend Swift Mailer Library
        $this->app->extend( 'mailer', function( $mailer, $app )
        {
            //  Return
            return $app['custom_mailer'];
        } );

        //  Set Alias
        $this->app->alias( 'icons', IconLibrary::class );
    }

    /**
     * On System Boot
     */
    public function boot()
    {
        //  Load Overrides
        require_once LARAVEL_CORE_VENDOR_PATH . 'helpers/boot.php';

        //  Config File
        $config_file = LARAVEL_CORE_VENDOR_PATH . 'config/laravel-core.php';

        //  Define Config Publish
        $this->publishes( [
            $config_file => config_path( 'laravel-core.php' )
        ], 'config' );

        //  Merge Config
        $this->mergeConfigFrom(
            $config_file, 'laravel-core'
        );

        //  Load Routes
        $this->loadRoutesFrom( LARAVEL_CORE_VENDOR_PATH . 'routes/web.php' );

        //  Define Image Cache Publish
        $this->publishes( [
            LARAVEL_CORE_VENDOR_PATH . 'image_cache' => assets()->cache( null, false )
        ], 'image_cache' );
    }
}
