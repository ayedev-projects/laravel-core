<?php namespace Ayedev\LaravelCore\Decorator;

use Ayedev\LaravelCore\Extension\NavigationManager;

abstract class NavigationDecorator
{
    /** @var array $_classes */
    protected $_classes = array(
        'plain' => false,
        'ul_class' => '',
        'sub_ul_class' => '',
        'li_class' => '',
        'sub_li_class' => '',
        'use_wrapper' => true,
        'wrapper_id' => '',
        'wrapper_tag' => 'nav',
        'wrapper_class' => '',
        'use_sub_wrapper' => false,
        'sub_wrapper_tag' => 'div',
        'sub_wrapper_class' => '',
        'anchor_class' => '',
        'sub_anchor_class' => '',
        'active_anchor_class' => 'active',
        'use_anchor_sub' => false,
        'anchor_text_class' => 'title',
        'use_subnav_arrow' => false,
        'subnav_arrow_class' => 'arrow'
    );


    /**
     * Set the Class
     *
     * @param $key
     * @param $value
     * @param bool $append
     * @return $this
     */
    public function setClass( $key, $value, $append = false )
    {
        //  Store
        $this->_classes[$key] = ( $append && isset( $this->_classes[$key] ) ? $this->_classes[$key] . ' ' : '' ) . $value;

        //  Return
        return $this;
    }

    /**
     * Output the Navigation
     *
     * @param NavigationManager $navigationManager
     * @param array $merge
     * @param bool $force
     * @return string
     */
    abstract public function output( NavigationManager $navigationManager, $merge = array(), $force = false );
}