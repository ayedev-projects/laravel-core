<?php namespace Ayedev\LaravelCore\Decorator;

use Ayedev\LaravelCore\Extension\NavigationItem;
use Ayedev\LaravelCore\Extension\NavigationManager;

class SimpleNavigationDecorator extends NavigationDecorator
{

    /**
     * Output the Navigation
     *
     * @param NavigationManager $navigationManager
     * @param array $merge
     * @param bool $force
     * @return string
     */
    public function output( NavigationManager $navigationManager, $merge = array(), $force = false )
    {
        //  Get Config
        $config = array_merge( $this->_classes, $merge, $navigationManager->getForMerge() );

        //  Check
        if( $force )    $config = array_merge( $config, $merge );

        //  Start
        $output = ( $config['use_wrapper'] ? '<' . $config['wrapper_tag'] . ( $config['wrapper_id'] ? ' id="' . $config['wrapper_id'] . '"' : '' ) . ( $config['wrapper_class'] ? ' class="' . $config['wrapper_class'] . '"' : '' ) . '>' . PHP_EOL : '' );

        //  Run the Loop
        $output .= $this->do_loop( $navigationManager, $merge, $force );

        //  End
        $output .= ( $config['use_wrapper'] ? '</' . $config['wrapper_tag'] . '>' : '' );

        //  Return
        return $output;
    }

    /**
     * Run the Loop
     *
     * @param NavigationManager $navigationManager
     * @param $merge
     * @param bool $force
     * @param int $level
     * @return string
     */
    private function do_loop( NavigationManager $navigationManager, $merge, $force = false, $level = 0 )
    {
        //  Get Config
        $config = array_merge( $this->_classes, $merge, $navigationManager->getForMerge() );

        //  Check
        if( $force )    $config = array_merge( $config, $merge );

        //  Use Plain
        $use_plain = $config['plain'];

        //  Get Items
        $items = $navigationManager->getValidItems( $use_plain );

        //  Check
        if( sizeof( $items ) < 1 )  return PHP_EOL;

        //  UL Class
        $ul_class = $config[ ( $level > 0 ? 'sub_' : '' ) . 'ul_class' ];

        //  Output
        $output = '';

        //  Check
        if( !$use_plain && $level > 0 && $config['use_sub_wrapper'] )
        {
            //  Start Wrapper
            $output .= "<{$config['sub_wrapper_tag']}" . ( $config['sub_wrapper_class'] ? " class=\"{$config['sub_wrapper_class']}\"": '' ) . ">" . PHP_EOL;

            //  Check
            if( $navigationManager->getLabel() )    $output .= '<span>' . $navigationManager->getLabel() . '</span>' . PHP_EOL;

            //  Check
            if( $navigationManager->getBeforeContent() ) $output .= $navigationManager->getBeforeContent() . PHP_EOL;
        }

        //  Start
        $output .= '<ul' . ( $ul_class ? ' class="' . $ul_class . '"' : '' ) . '>' . PHP_EOL;

        /**
         * Loop Each
         *
         * @var NavigationItem $item
         */
        foreach( $items as $i => $item )
        {
            //  Get the Icon
            $icon = $item->getIcon();

            //  LI Class
            $li_class = trim( $config[ ( $level > 0 ? 'sub_' : '' ) . 'li_class' ] . ' ' . implode( ' ', $item->getClasses() ) );

            //  Check
            if( $item->hasSub() )   $li_class .= ' has-subnav';

            //  Check
            if( $item->hasSub() && $item->getLink() == 'javascript:void(0);' && $item->getSubValidCount() < 1 ) continue;

            //  Check if First
            if( $i == 0 )   $li_class .= ' first-child';

            //  Check if Last
            if( ( $i + 1 ) == sizeof( $items ) )   $li_class .= ' last-child';

            //  Start
            $output .= '<li' . ( trim( $li_class ) ? ' class="' . trim( $li_class ) . '"' : '' ) . ( $item->getID() ? ' id="' . $item->getID() . '"' : '' ) . '>' . PHP_EOL;

            //  Anchor Class
            $anchor_class = trim( $config[ ( $level > 0 ? 'sub_' : '' ) . 'anchor_class' ] . ' ' . implode( ' ', $item->getAnchorClasses() ) );

            //  Check
            if( $item->isCurrent() )    $anchor_class .= ' ' . $config['active_anchor_class'];

            //  Check
            if( !$use_plain && $item->getBeforeContent() ) $output .= $item->getBeforeContent() . PHP_EOL;

            //  Start Anchor Tag
            $output .= '<a ' . _attrs_to_html( array_merge( $item->getFinalAttrs(), ( trim( $anchor_class ) ? array( 'class' => trim( $anchor_class ) ) : array() ) ) ) . '>';

            //  Check Before
            if( !$use_plain && $icon && $icon['position'] == 'before' )    $output .= '<span class="icon icon-before ' . $icon['class'] . '">' . ( (string)$icon['content'] ) . '</span>' . PHP_EOL;

            //  Add the Label
            if( !$use_plain && $config['use_anchor_sub'] ) $output .= '<span class="' . $config['anchor_text_class'] . '">' . $item->getLabel() . '</span>' . PHP_EOL;
            else    $output .= $item->getLabel();

            //  Check After
            if( !$use_plain && $icon && $icon['position'] == 'after' )    $output .= '<span class="icon icon-after ' . $icon['class'] . '">' . ( (string)$icon['content'] ) . '</span>' . PHP_EOL;

            //  Check
            if( !$use_plain && $item->hasSub() && $config['use_subnav_arrow'] )    $output .= '<span class="' . $config['subnav_arrow_class'] . '"></span>';

            //  End Anchor Tag
            $output .= '</a>';

            //  Check
            if( !$use_plain && $item->getAfterContent() ) $output .= $item->getAfterContent() . PHP_EOL;

            //  Check Has Sub Menu
            if( $item->hasSub() )
            {
                //  Base Merge
                $baseMerge = array(
                    'plain' => $use_plain,
                    'use_wrapper' => $config['use_wrapper'],
                    'use_sub_wrapper' => $config['use_sub_wrapper'],
                    'use_anchor_sub' => $config['use_anchor_sub'],
                    'use_subnav_arrow' => $config['use_subnav_arrow'],
                    'anchor_text_class' => $config['anchor_text_class'],
                    'subnav_arrow_class' => $config['subnav_arrow_class'],
                    'sub_ul_class' => $config['sub_ul_class'],
                    'sub_li_class' => $config['sub_li_class']
                );

                //  Continue the Loop
                $output .= $this->do_loop( $item->getSub(), $baseMerge, false, $level + 1 );
            }

            //  End
            $output .= '</li>' . PHP_EOL;
        }

        //  End
        $output .= '</ul>' . PHP_EOL;

        //  Check
        if( !$use_plain && $level > 0 && $config['use_sub_wrapper'] )
        {
            //  Check
            if( $navigationManager->getAfterContent() ) $output .= $navigationManager->getAfterContent() . PHP_EOL;

            //  End Wrapper
            $output .= "</{$config['sub_wrapper_tag']}>" . PHP_EOL;
        }

        //  Return
        return $output;
    }
}