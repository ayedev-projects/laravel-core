<?php namespace Ayedev\LaravelCore\Traits;

use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Schema;
use Watson\Rememberable\Rememberable;

trait ModelTrait
{
    use ModelEventTrait, Rememberable,
        ProgressCalculationTrait, DispatchesJobs;


    /** @var array $_columns */
    protected $_columns;

    /** @var string $_sluggable_source */
    protected $_sluggable_source = null;

    /** @var string $_sluggable_method */
    protected $_sluggable_method = null;

    /** @var string $_sluggable_field */
    protected $_sluggable_field = 'slug';

    /** @var string $_sluggable_group_type_field */
    protected $_sluggable_group_type_field = null;

    /** @var string $_sluggable_separator */
    protected $_sluggable_separator = '-';

    /** @var string $_sluggable_suffix */
    protected $_sluggable_suffix = null;

    /** @var array $changes_log */
    public $_changes_log = array();

    /** @var string $_model_namespace */
    protected $_model_namespace = "\\App\\Model";

    /** @var int $rememberFor */
    protected $rememberFor = 60;

    /** @var int $rememberCacheTag */
	protected $rememberCacheTag = null;

    /** @var string $NODATA */
    public static $NODATA = '-NULL-';


    /**
     * Get the Table Name
     *
     * @return string
     */
    public static function tableName()
    {
        //  Return
        return with( new static() )->getTable();
    }

    /**
     * Check Column Exists
     *
	 * @param string $column
     * @return string
     */
    public static function hasColumn( $column )
    {
        //  Return
        return with( new static() )->columnExists( $column );
    }

    /**
     * Load Row
     *
     * @param $val
     * @param string $key
     * @return mixed
     */
    public static function loadRow( $val, $key = 'id' )
    {
        //  Query
        $query = self::query();

        //  Fix
        $oldKey = $key;
        $key = (array)$key;
        $val = ( !is_array( $oldKey ) ? array( $val ) : $val );

        //  Loop Each
        foreach( $key as $i => $k )
        {
            //  The Value
            $theVal = $val[$i];

            //  Set Query
            if( is_array( $theVal ) )   $query->whereIn( $k, $theVal );
            else    $query->where( $k, $theVal );
        }

        //  Return
        return $query->first();
    }


    /**
     * Init the Construct
     *
     * @param array $attributes
     */
    protected function _initConstruct( array $attributes = [] )
    {
        //  Call Self Before Init
        $this->_beforeInit();

        //  Before Init Model
        $this->_beforeInitModel();

        //  Call parent
        parent::__construct( $attributes );

        //  Set the Cache Tags
        if( Cache::getStore() instanceof \Illuminate\Cache\TaggableStore )  $this->rememberCacheTag = $this->uniqueCacheTags();

        //  Check
        if( getTS()->readStorage( 'no_eloquent_cache' ) )  $this->rememberFor = null;

        //  Init Model
        $this->_initModel();

        //  Call Self Init
        $this->_init();
    }

    /**
     * Make Unique Cache Tags for Table
     *
     * @return array
     */
    public function uniqueCacheTags()
    {
        //  Return
        return array( 'eloquent:table:' . $this->getTable() );
    }

    /**
     * Init Before Internal
     */
    protected function _beforeInit()
    {
        //  To Override if required
    }

    /**
     * Init Internal
     */
    protected function _init()
    {
        //  To Override if required
    }

    /**
     * Before Init Model
     */
    protected function _beforeInitModel()
    {
        //  Trigger Methods
        $this->_triggerMethods( '__beforeInit' );
    }

    /**
     * Boot Model Trait
     */
    public static function bootModelTrait()
    {
        //  Listen Saving
        whenModel( static::class, 'saving', function( $record )
        {
            //  Get Original
            $original = $record->getOriginal();

            //  Get Attributes
            $attributes = $record->getAttributes();

            //  Changes
            $changes = array();

            //  Loop Each
            foreach( $attributes as $key => $val ) {

                //  Check
                if( !isset( $original[$key] ) || $original[$key] != $val ) {

                    //  Store
                    $changes[$key] = array(
                        'old' => ( isset( $original[$key] ) ? $original[$key] : null ),
                        'new' => $val
                    );
                }
            }

            //  Loop Each
            foreach( $original as $key => $val ) {

                //  Check
                if( !isset( $changes[$key] ) && !$attributes[$key] ) {

                    //  Store
                    $changes[$key] = array(
                        'old' => $original[$key],
                        'new' => null
                    );
                }
            }

            //  Store Log
            $record->_changes_log = $changes;
        }, 100 );

        //  Listen Saved
        whenModel( static::class, 'saved', function( $record ) {

            //  Loop Each Changes Log
            foreach( $record->_changes_log as $key => $values ) {

                //  Check
                if( ( empty( $values['old'] ) || is_null( $values['old'] ) ) && is_null( $values['new'] ) ) continue;

                //  Method
                $method = 'prop' . ucfirst( camel_case( $key ) ) . ( is_null( $values['old'] ) ? 'Added' : ( is_null( $values['new'] ) ? 'Deleted' : 'Changed' ) );

                //  Check
                if( method_exists( $record, $method ) ) {

                    //  Make Callback
                    call_user_func_array( array( $record, $method ), array( $values['new'], $values['old'] ) );
                }
            }

            //  Clear Changes Log
            $record->_changes_log = array();
        }, 100 );
    }

    /**
     * Init Model
     */
    protected function _initModel()
    {
        //  Record
        $record = $this;

        //  Read the Columns
        $this->_columns = Cache::remember( strtolower( str_ireplace( "\\", '_', get_called_class() ) . '_columns' ), 180, function() use ( $record ) {

            //  Return
            return Schema::getColumnListing( $record->getTable() );
        } );

        //  Trigger Methods
        $this->_triggerMethods( '__init' );
    }

    /**
     * Detect the Table Name
     */
    public function getTable()
    {
        //  Check
        if( isset( $this->table ) ) return detectTableName( $this->table );

        //  Key
        $key = rtrim( class_basename( $this ), 'Model' );

        //  Get Table Name
        $tableName = str_replace( '\\', '', snake_case( str_plural( $key ) ) );

        //  Return
        return $tableName;
    }

	/**
	 * Check Column Exists
	 */
	public function columnExists( $column )
	{
		//	Return
		return in_array( $column, $this->_columns );
	}


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return ( $this->_sluggable_source ? [
            'slug' => [
                'source' => $this->_sluggable_source,
                'separator' => $this->_sluggable_separator,
                'method' => ( $this->_sluggable_method ? array( $this, $this->_sluggable_method ) : null ),
                'uniqueSuffix' => ( $this->_sluggable_suffix ? array( $this, $this->_sluggable_suffix ) : null )
            ]
        ] : null );
    }


    /**
     * Scope by Slug
     *
     * @param $query
     * @param $slug
     * @return mixed
     */
    public function scopeBySlug( $query, $slug ) {

        //  Return
        return $query->active()->where( 'slug', $slug );
    }

    /**
     * Scope by Active
     *
     * @param $query
     * @param int $active
     * @return mixed
     */
    public function scopeActive( $query, $active = 1 ) {

        //  Return
        return $query->where( $this->getTable() . '.active', $active );
    }

    /**
     * Scope by Blocked
     *
     * @param $query
     * @return mixed
     */
    public function scopeBlocked( $query ) {

        //  Return
        return $query->where( $this->getTable() . '.blocked', '1' );
    }

    /**
     * Scope by Not Blocked
     *
     * @param $query
     * @return mixed
     */
    public function scopeNotBlocked( $query ) {

        //  Return
        return $query->where( $this->getTable() . '.blocked', '0' );
    }

    /**
     * Scope by Featured
     *
     * @param $query
     * @param int $featured
     * @return mixed
     */
    public function scopeFeatured( $query, $featured = 1 ) {

        //  Return
        return $query->where( $this->getTable() . '.featured', $featured );
    }

    /**
     * Scope by Parent
     *
     * @param $query
     * @param $parent_id
     * @return mixed
     */
    public function scopeParent( $query, $parent_id )
    {
        //  Return
        return $query->where( $this->parentField(), $parent_id );
    }

    /**
     * Order by Sort
     *
     * @param $query
     * @param string $dir
     * @return mixed
     */
    public function scopeOrderBySort( $query, $dir = 'ASC' )
    {
        //  Return
        return $query->orderBy( $this->sortField(), $dir );
    }


    /**
     * Get Primary Key
     *
     * @return string
     */
    public function pKey() {

        //  Return
        return $this->primaryKey;
    }

    /**
     * Get Primary Value
     *
     * @return mixed
     */
    public function pVal() {

        //  Return
        return $this->getAttribute( $this->primaryKey );
    }

    /**
     * Get the Parent Field Index
     *
     * @return string
     */
    public function parentField()
    {
        //  Return
        return 'parent_id';
    }

    /**
     * Get the Sort Field Index
     *
     * @return string
     */
    public function sortField()
    {
        //  Return
        return 'sort_order';
    }

    /**
     * Check is New
     *
     * @return bool
     */
    public function isNew()
    {
        //  Return
        return ( ( time() - strtotime( $this->getAttribute( self::CREATED_AT ) ) ) <= ( 60 * 60 * 24 * 7 ) );
    }

    /**
     * Check Attribute Exists
     *
     * @param $key
     * @return bool
     */
    public function hasAttribute( $key )
    {
        //  Return
        return isset( $this->attributes[$key] );
    }


    /**
     * Make the Slug based on Slug Group
     *
     * @param $str
     * @param $seperator
     * @return string
     */
    public function makeSlugByGroup( $str, $seperator )
    {
        //  Get Config
        $config = array_merge( config( 'sluggable' ), $this->sluggable()[$this->_sluggable_field] );

        //  Slug
        $slug = str_slug( $str, $seperator );

        //  Check
        if( $this->slug_group && $this->slug_group_types && $this->_sluggable_group_type_field )
        {
            //  Get Existings
            $existings = static::findSimilarSlugs( $this, $this->_sluggable_field, $config, $slug )->whereIn( $this->_sluggable_group_type_field, (array)$this->slug_group_types )->where( 'id', '!=', $this->getID() );

            //  Check
            if( $existings->count() > 0 )
            {
                //  Add Prefix
                $slug = $this->slug_group . $seperator . $slug;
            }
        }

        //  Return
        return $slug;
    }

    /**
     * Get Slug Group Attribute
     *
     * @return string|null
     */
    public function getSlugGroupAttribute()
    {
        //  Return
        return null;
    }

    /**
     * Get Slug Group Types Attribute
     *
     * @return string|null
     */
    public function getSlugGroupTypesAttribute()
    {
        //  Return
        return null;
    }


    /**
     * Generates the Unique Value for Field
     *
     * @param $field
     * @param null $value
     * @param bool $username
     * @param int $length
     * @param int $index
     * @return null|string
     */
    public static function generateUnique( $field, $value = null, $username = true, $length = 10, $index = 0 )
    {
        //  Value
        $value || $value = ( $username ? random_username() : str_random( $length ) );

        //  Check
        if( stripos( $value, '@' ) > -1 )
        {
            //  Explode
            $explodes = explode( '@', $value );

            //  Set New Value
            $value = $explodes[0] . '.' . substr( $explodes[1], 0, stripos( $explodes[1], '.' ) );
        }

        //  Convert to Slug
        $value = str_slug( $value . ( $index > 0 ? $index : '' ), '' );

        //  Check Valid
        if( self::where( $field, $value )->count() > 0 )
        {
            //  Create New Again
            $value = self::generateUnique( $field, $value, $username, $length, $index + 1 );
        }

        //  Return
        return $value;
    }

    /**
     * Assign the Unique
     *
     * @param $field
     * @param null $value
     * @param null $source
     * @param bool $username
     * @param bool $raw
     * @return $this
     */
    public function assignUnique( $field, $value = null, $source = null, $username = true, $raw = false )
    {
        //  Get the New Value
        $newValue = self::generateUnique( $field, ( $value ? $value : $this->getAttribute( $source ) ), $username );

        //  Store
        if( $raw )  $this->attributes[$field] = $newValue;
        else    $this->setAttribute( $field, $newValue );

        //  Return
        return $this;
    }


    /**
     * Manipulate DateTime with Timezone
     *
     * @param $value
     * @param null $tz
     * @return mixed
     */
    public function manipulateDateTime( $value, $tz = null ) {

        //  Get Timezone
        $timezone = ( $tz ?: ( $this->timezone ?: default_timezone() ) );

        //  Get Datetime
        $datetime = ( $value instanceof Carbon ? $value : $this->asDateTime( $value ) );

        //  Return
        return $datetime->timezone( $timezone );
    }

    /** Get Created At **/
    public function created_at( $tz = null ) {

        //  Return
        return ( $this->timestamps ? $this->manipulateDateTime( $this->getAttribute( self::CREATED_AT ), $tz ) : null );
    }

    /** Get Updated At **/
    public function updated_at( $tz = null ) {

        //  Return
        return ( $this->timestamps ? $this->manipulateDateTime( $this->getAttribute( self::UPDATED_AT ), $tz ) : null );
    }

    /** Get Deleted At **/
    public function deleted_at( $tz = null ) {

        //  Return
        return ( method_exists( $this, 'getDeletedAtColumn' ) && is_array( $this->dates ) && in_array( $this->getDeletedAtColumn(), $this->dates ) ? $this->manipulateDateTime( $this->getAttribute( $this->getDeletedAtColumn() ), $tz ) : null );
    }


    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        //  Return
        return 'slug';
    }


    /**
     * Resolve the Class Name
     *
     * @param $className
     * @return string
     */
    public function resolveClassName( $className )
    {
        //  Check
        if( substr( $className, 0, 1 ) == '\\' )    return $className;

        //  Return
        return $this->_model_namespace . '\\' . $className;
    }


    /**
     * Listen Calls
     *
     * @param $method
     * @param $parameters
     * @return mixed
     * @throws \Exception
     */
    public function __call( $method, $parameters )
    {
        //  Output
        $output = null;

        //  Exception
        $exception = null;

        //  Try
        try
        {
            //  Run Parent
            $output = parent::__call( $method, $parameters );
        }
        catch( \Exception $e )
        {
            //  Store Exception
            $exception = $e;

            //  Trigger Methods
            $output = $this->_triggerMethods( '__magicCall', $method, $parameters, true );

            //  Check
            if( !is_null( $output ) )
            {
                //  Clear Exception
                $exception = null;

                //  Fix Output
                $output = ( $output === self::$NODATA ? null : $output );
            }
            else
            {
                //  Fixer for ID
                if( $method == 'getID' )    $method = 'getId';
                else if( $method == 'setID' )    $method = 'setId';

                //  Check for Getter
                if( substr( $method, 0, 3 ) == 'get' && ( !$this->exists || $this->hasAttribute( snake_case( substr( $method, 3 ) ) ) ) )
                {
                    //  Set Output
                    $output = $this->getAttribute( snake_case( substr( $method, 3 ) ) );

                    //  Clear Exception
                    $exception = null;
                }
                //  Check for Setter
                else if( substr( $method, 0, 3 ) == 'set' && ( !$this->exists || $this->hasAttribute( snake_case( substr( $method, 3 ) ) ) ) )
                {
                    //  Set Attribute
                    $this->setAttribute( snake_case( substr( $method, 3 ) ), $parameters[0] );

                    //  Set Output
                    $output = $this;

                    //  Clear Exception
                    $exception = null;
                }
            }
        }

        //  Check
        if( $exception )    throw $exception;

        //  Return
        return $output;
    }
}
