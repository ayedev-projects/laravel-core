<?php namespace Ayedev\LaravelCore\Traits;

trait RoleTrait
{
    /**
     * Boot Trait
     */
    public static function bootRoleTrait()
    {
        //  Listen Deleting
        static::deleting( function( $role )
        {
            //  Check for Delete
            if( !method_exists( $role, 'isForceDeleting' ) || $role->isForceDeleting() )
            {
                //  Check Permissions
                if( method_exists( $role, 'perms' ) )   $role->perms()->sync( [] );

                //  Sync Roles
                $role->syncRolesBeforeDelete();
            }
        } );
    }

    /** Sync Roles */
    abstract public function syncRolesBeforeDelete();


    /**
     * Get Name
     *
     * @return string
     */
    public function getName()
    {
        //  Return
        return $this->attribute( 'name' );
    }

    /**
     * Get Label
     *
     * @return string
     */
    public function getLabel()
    {
        //  Return
        return $this->attribute( 'display_name' );
    }


    /**
     * Get the Role
     *
     * @param $role
     * @return static
     */
    public static function getRole( $role )
    {
        //  Return
        return self::whereName( $role )->first();
    }

    /**
     * Save ROle
     *
     * @param $name
     * @param null $label
     * @param null $desc
     * @return static
     */
    public static function saveRole( $name, $label = null, $desc = null )
    {
        //  Search First
        $role = static::getRole( $name );

        //  Check
        if( !$role )
        {
            //  Create Instance
            $role = new static();
            $role->name = $name;
            if( $label ) $role->display_name = $label;
            if( $desc ) $role->description = $desc;

            //  Save
            $role->save();
        }

        //  Return
        return $role;
    }
}