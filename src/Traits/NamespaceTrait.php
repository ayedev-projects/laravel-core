<?php namespace Ayedev\LaravelCore\Traits;

use Illuminate\Console\AppNamespaceDetectorTrait;

trait NamespaceTrait
{
    use AppNamespaceDetectorTrait;

    /**
     * Read the Namespace
     *
     * @return string
     */
    public function getNamespace()
    {
        //  Return
        return $this->getAppNamespace();
    }

    /**
     * Resolve Class Name
     *
     * @param $name
     * @return string
     */
    public function resolveClassWithNamespace( $name )
    {
        //  Return
        return "\\" . $this->getNamespace() . "\\" . $name;
    }
}