<?php namespace Ayedev\LaravelCore\Traits;

use Ayedev\LaravelCore\Core\EloquentBuilder;

trait ModelCacheTrait
{
    /**
     * Get the Cache Base Key
     */
    public function cacheBaseKey()
    {
        //  Return
        return 'eloquent:' . $this->getTable();
    }

    /**
     * Get the Cache Minutes
     *
     * @return int
     */
    public function cacheMinutes()
    {
        //  Return
        return 180;
    }

    /**
     * Prepare the Cache Key
     *
     * @param EloquentBuilder $query
     * @return string
     */
    public function prepareCacheKey( EloquentBuilder $query )
    {
        //  Return
        return md5( $query->getQuery()->getConnection()->getName() . $query->getQuery()->toSql() . serialize( $query->getQuery()->getBindings() ) );
    }


    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function newEloquentBuilderBak( $query )
    {
        //  Return
        return new EloquentBuilder( $query );
    }
}