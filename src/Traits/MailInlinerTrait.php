<?php namespace Ayedev\LaravelCore\Traits;

trait MailInlinerTrait
{
    /**
     * Set the Subscription Info
     */
    public function setSubscriptionInfo( $email, $name = null, $extras = array() )
    {
        //  Share
        view()->share( 'subscriptionData', array_merge( array(
            'name' => $name,
            'email' => $email
        ), $extras ) );
    }

    /**
     * Check has subscription info attached
     */
    public function hasSubscriptionInfo()
    {
        //  Return
        return view()->has( 'subscriptionData' );
    }

    /**
     * Image Tag for Email
     *
     * @param $src
     * @param null $alt
     * @param null $styles
     * @param null $append
     * @return string
     */
    public function image_tag( $src, $alt = null, $styles = null, $append = null )
    {
        //  Return
        return '<img src="' . $src . '" alt="' . $alt . '" style="' . $styles . '" ' . $append . ' />';
    }

    /**
     * Anchor Tag for Email
     *
     * @param $link
     * @param $text
     * @param null $title
     * @param bool $block
     * @param null $styles
     * @param string $target
     * @param null $append
     * @param bool $convertLink
     * @return string
     */
    public function anchor_tag( $link, $text, $title = null, $block = false, $styles = null, $target = null, $append = null, $convertLink = true )
    {
        //  Change Link
        $link = ( $convertLink ? route( 'email_redirector', array( 'any' => str_ireplace( '=', '', base64_encode( $link ) ) ) ) : $link );

        //  Return
        return '<a href="' . $link . '" title="' . $title . '" target="' . ( $target ?: '_blank' ) . '" style="text-decoration:none;' . ( $block ? 'display:block;' : '' ) . $styles . '" ' . $append . '>' . $text . '</a>';
    }

    /**
     * Anchor Tag for Email without Converting
     *
     * @param $link
     * @param $text
     * @param null $title
     * @param bool $block
     * @param null $styles
     * @param string $target
     * @param null $append
     * @return string
     */
    public function anchor_tag2( $link, $text, $title = null, $block = false, $styles = null, $target = null, $append = null )
    {
        //  Return
        return $this->anchor_tag( $link, $text, $title, $block, $styles, $target, $append, false );
    }

    /**
     * Strong Tag for Email
     *
     * @param $text
     * @param null $styles
     * @param null $append
     * @return string
     */
    public function strong_tag( $text, $styles = null, $append = null )
    {
        //  Return
        return '<strong style="font-weight:500;' . $styles . '" ' . $append . '>' . $text . '</strong>';
    }

    /**
     * Make Nice Link
     *
     * @param $link
     * @param $text
     * @param null $title
     * @param null $styles
     * @param null $target
     * @param null $append
     * @param bool $convertLink
     * @param bool $bgColor
     * @return string
     */
    public function nice_link( $link, $text, $title = null, $styles = null, $target = null, $append = null, $convertLink = true, $bgColor = null, $addMargin = true )
    {
        //  Styles
        $styles = $this->styles( 'background:' . ( $bgColor ? $bgColor : '#1CBBB4' ) . ';height:40px;line-height:40px;padding:0 20px;font-size:14px;text-align:center;' . ( $addMargin ? 'margin:15px 0 25px;' : '' ) . (string)$styles, '#FFF', '300', 'inline-block' );

        //  Return
        return $this->anchor_tag( $link, $text, $title, false, $styles, $target, $append, $convertLink );
    }

    /**
     * Make Nice Link 2
     *
     * @param $link
     * @param $text
     * @param null $title
     * @param null $styles
     * @param null $target
     * @param null $append
     * @return string
     */
    public function nice_link2( $link, $text, $title = null, $styles = null, $target = null, $append = null )
    {
        //  Return
        return $this->nice_link( $link, $text, $title, false, $styles, $target, $append, false );
    }

    /**
     * Make Level Link
     *
     * @param $level
     * @param $link
     * @param $text
     * @param null $title
     * @param null $styles
     * @param null $target
     * @param null $append
     * @return string
     */
    public function level_link( $level, $link, $text, $title = null, $styles = null, $target = null, $append = null, $convertLink = false )
    {
        //  Return
        return $this->nice_link( $link, $text, $title, $styles, $target, $append, $convertLink, ( $level == 'error' ? '#fb5d5d' : ( $level == 'success' ? '#71ca67' : null ) ), false );
    }

    /**
     * Generate basic styles
     *
     * @param null $append
     * @param bool $color
     * @param bool $weight
     * @param bool $display
     * @param bool $font
     * @return string
     */
    public function styles( $append = null, $color = false, $weight = false, $display = false, $font = true )
    {
        //  Properties
        $properties = array();

        //  Color
        if( $color )    $properties[] = 'color:' . ( $color === true ? '#07c4d8' : $color ) . ';';

        //  Font Weight
        if( $weight )    $properties[] = 'font-weight:' . ( $weight === true ? '500' : $weight ) . ';';

        //  Block
        if( $display )    $properties[] = 'display:' . ( $display === true ? 'block' : $display ) . ';';

        //  Font Family
        if( $font )    $properties[] = "font-family:'" . ( $font === true ? 'Roboto' : $font ) . "', Arial, 'Helvetica Neue', Helvetica, sans-serif;";

        //  Return
        return implode( '', $properties ) . $append;
    }
}