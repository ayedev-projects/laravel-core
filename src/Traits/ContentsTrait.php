<?php namespace Ayedev\LaravelCore\Traits;

trait ContentsTrait
{
    /** Get the Keyword Fields */
    abstract public function getKeywordFields();

    /**
     * Get Row Type Field
     *
     * @return null
     */
    public function getRowTypeField()
    {
        //  Return
        return null;
    }


    /**
     * Scope by Keyword
     *
     * @param $query
     * @param $term
     * @param bool $search_type
     * @param bool $deep_search
     * @return mixed
     */
    public function scopeKeyword( $query, $term, $search_type = false, $deep_search = true )
    {
        //  Mappings
        $mappings = array();

        //  Keyword Fields
        $keywordFields = $this->getKeywordFields();

        //  Check
        if( $keywordFields )
        {
            //  Loop Each
            foreach( (array)$this->getKeywordFields() as $kField )
            {
                //  Append
                $mappings[] = "`{$kField}` = '{$term}'";
                $mappings[] = "`{$kField}` LIKE '%{$term}%'";
                $mappings[] = "`{$kField}` = '" . str_slug( $term ) . "'";
                $mappings[] = "`{$kField}` LIKE '%" . str_slug( $term ) . "%'";

                //  Check
                if( !$deep_search )  break;
            }

            //  Check
            if( ( $row_type_field = $this->getRowTypeField() ) && $search_type )
            {
                //  Add Mappings
                $mappings[] = "`{$row_type_field}` = '{$term}'";
                $mappings[] = "`{$row_type_field}` LIKE '%{$term}%'";
                $mappings[] = "`{$row_type_field}` = '" . str_slug( $term ) . "'";
                $mappings[] = "`{$row_type_field}` LIKE '%" . str_slug( $term ) . "%'";
            }

            //  Check
            if( $mappings && sizeof( $mappings ) > 0 ) $query->whereRaw( "( " . implode( " OR ", $mappings ) . " )" );
        }

        //  Return
        return $query;
    }


    //  Get Search Excerpt
    public function getSearchMatchContent( $term, $field = 'description', $count = 30 )
    {
        //  Value
        $value = $this->getAttribute( $field );

        //  Return
        return getSearchMatchContent( $term, $value, $count );
    }

    //  Get Excerpt
    public function getExcerpt( $count = null, $field = 'excerpt', $fallback = 'description' )
    {
        //  Get the Excerpt
        $excerpt = (string)$this->getAttribute( $field );

        //  Check
        if( !$excerpt || empty( $excerpt ) )
        {
            //  Get the Description
            $desc = $this->getAttribute( $fallback );

            //  Check
            if($desc && !empty($desc))
            {
                //  Extract Excerpt
                $excerpt = extract_shorter( $desc, $count );
            }
        }
        else
        {
            //  Extract Shorter
            $excerpt = extract_shorter( $excerpt, $count );
        }

        //  Return
        return (string)$excerpt;
    }

    //  Get Description
    public function getDescription()
    {
        //  Return
        return (string)$this->getAttribute( 'description' );
    }

    //  Get HTML Formatted Description
    public function formattedDescription()
    {
        //  Description
        $description = $this->getDescription();

        //  Return
        return formatContents( $description );
    }
}