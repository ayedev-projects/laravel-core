<?php namespace Ayedev\LaravelCore\Traits;

use Illuminate\Support\Facades\Cache;

trait ModelEventTrait
{
    /** @var string $BEFORE_CREATE */
    public static $BEFORE_CREATE = 'before_create';

    /** @var string $AFTER_CREATE */
    public static $AFTER_CREATE = 'after_create';

    /** @var string $BEFORE_UPDATE */
    public static $BEFORE_UPDATE = 'before_update';

    /** @var string $AFTER_UPDATE */
    public static $AFTER_UPDATE = 'after_update';

    /** @var string $BEFORE_SAVE */
    public static $BEFORE_SAVE = 'before_save';

    /** @var string $AFTER_SAVE */
    public static $AFTER_SAVE = 'after_save';

    /** @var string $BEFORE_DELETE */
    public static $BEFORE_DELETE = 'before_delete';

    /** @var string $AFTER_DELETE */
    public static $AFTER_DELETE = 'after_delete';

    /** @var string $BEFORE_TRASH */
    public static $BEFORE_TRASH = 'before_trash';

    /** @var string $AFTER_TRASH */
    public static $AFTER_TRASH = 'after_trash';

    /** @var string $BEFORE_TRASH */
    public static $BEFORE_RESTORE = 'before_restore';

    /** @var string $AFTER_TRASH */
    public static $AFTER_RESTORE = 'after_restore';

    /** @var array $_event_bindings */
    protected $_event_bindings = array();


    /**
     * Boot Trait
     */
    public static function bootModelEventTrait()
    {
        //  Class
        $class = static::class;

        //  Listen Events
        whenModel( $class, 'creating', array( $class, 'listenWhenCreating' ) );
        whenModel( $class, 'created', array( $class, 'listenWhenCreated' ) );
        whenModel( $class, 'updating', array( $class, 'listenWhenUpdating' ) );
        whenModel( $class, 'updated', array( $class, 'listenWhenUpdated' ) );
        whenModel( $class, 'saving', array( $class, 'listenWhenSaving' ) );
        whenModel( $class, 'saved', array( $class, 'listenWhenSaved' ) );
        whenModel( $class, 'deleting', array( $class, 'listenWhenDeleting' ) );
        whenModel( $class, 'deleted', array( $class, 'listenWhenDeleted' ) );

        //  Check
        if( method_exists( $class, 'restore' ) )
        {
            //  Listen Events
            whenModel( $class, 'restoring', array( $class, 'listenWhenRestoring' ) );
            whenModel( $class, 'restored', array( $class, 'listenWhenRestored' ) );
        }
    }

    /**
     * When Record Creating
     */
    public static function listenWhenCreating( $record )
    {
        //  Trigger Event
        return $record->triggerEvent( static::$BEFORE_CREATE );
    }

    /**
     * When Record Created
     */
    public static function listenWhenCreated( $record )
    {
        //  Trigger Event
        $record->triggerEvent( static::$AFTER_CREATE );

        //  Clear Cache
        $record->clearCache();
    }

    /**
     * When Record Updating
     */
    public static function listenWhenUpdating( $record )
    {
        //  Trigger Event
        return $record->triggerEvent( static::$BEFORE_UPDATE );
    }

    /**
     * When Record Updated
     */
    public static function listenWhenUpdated( $record )
    {
        //  Trigger Event
        $record->triggerEvent( static::$AFTER_UPDATE );

        //  Clear Cache
        $record->clearCache();
    }

    /**
     * When Record Saving
     */
    public static function listenWhenSaving( $record )
    {
        //  Trigger Event
        return $record->triggerEvent( static::$BEFORE_SAVE );
    }

    /**
     * When Record Saved
     */
    public static function listenWhenSaved( $record )
    {
        //  Trigger Event
        $record->triggerEvent( static::$AFTER_SAVE );

        //  Clear Cache
        $record->clearCache();
    }

    /**
     * When Record Deleting
     */
    public static function listenWhenDeleting( $record )
    {
        //  Trashing
        $trashing = ( method_exists( $record, 'trashed' ) && property_exists( $record, 'forceDeleting' ) && $record->forceDeleting === true );

        //  Trigger Event
        return $record->triggerEvent( $trashing ? self::$BEFORE_TRASH : self::$BEFORE_DELETE );
    }

    /**
     * When Record Deleted
     */
    public static function listenWhenDeleted( $record )
    {
        //  Trashing
        $trashing = ( method_exists( $record, 'trashed' ) && property_exists( $record, 'forceDeleting' ) && $record->forceDeleting === true );

        //  Trigger Event
        $record->triggerEvent( $trashing ? self::$AFTER_TRASH : self::$AFTER_DELETE );

        //  Clear Cache
        $record->clearCache();
    }

    /**
     * When Record Restoring
     */
    public static function listenWhenRestoring( $record )
    {
        //  Trigger Event
        return $record->triggerEvent( static::$BEFORE_RESTORE );
    }

    /**
     * When Record Restored
     */
    public static function listenWhenRestored( $record )
    {
        //  Trigger Event
        $record->triggerEvent( static::$AFTER_RESTORE );

        //  Clear Cache
        $record->clearCache();
    }


    /**
     * Map the Methods
     *
     * @param $prefix
     * @param null $suffix
     * @return array
     */
    protected function _mapMethods( $prefix, $suffix = null )
    {
        //  Cache Key
        $cacheKey = 'mapped_methods_' . basename( get_called_class() ) . '_' . $prefix;

        //  Methods
        $methods = getTS()->readStorage( $cacheKey );

        //  Check
        if( is_null( $methods ) )
        {
            //  Get Class
            $class = static::class;

            //  Recursive Classes
            $classes = class_uses_recursive( $class );

            //  Append Self
            $classes[$class] = $class;

            //  Loop the Traits
            foreach( $classes as $trait )
            {
                //  Class Method
                $classMethod = $prefix . class_basename( $trait ) . ( $suffix ? $suffix : '' );

                //  Check Method
                if( method_exists( $this, $classMethod ) )
                {
                    //  Append to List
                    $methods[] = $classMethod;
                }
            }

            //  Write Storage
            getTS()->writeStorage( $cacheKey, $methods );
        }

        //  Check
        if( is_null( $methods ) )   $methods = array();

        //  Return
        return $methods;
    }

    /**
     * Trigger the Methods
     *
     * @param $prefix
     * @param $method
     * @param array $parameters
     * @param bool $return
     * @param null $suffix
     * @return mixed
     */
    public function _triggerMethods( $prefix, $method = null, $parameters = null, $return = false, $suffix = null )
    {
        //  Result
        $result = null;

        //  Fix Parameters
        $parameters || $parameters = array();

        //  Get Methods
        $methods = $this->_mapMethods( $prefix, $suffix );

        //  Loop the Methods
        foreach( $methods as $classMethod )
        {
            //  Forward the Call
            $response = call_user_func_array( array( $this, $classMethod ), array( $method, $parameters ) );

            //  Check
            if( !is_null( $response ) && $return )
            {
                //  Result
                $result = $response;

                //  Break
                break;
            }
        }

        //  Return
        return ( $return ? $result : $this );
    }

    /**
     * Check Needs to be Triggered
     */
    public function canBeTriggered( $event )
    {
        //  Cache Key
        $cacheKey = 'triggerable_' . basename( get_called_class() ) . '_' . $event;

        //  Can Be Triggered
        $canBeTriggered = getTS()->readStorage( $cacheKey );

        //  Check
        if( is_null( $canBeTriggered ) )
        {
            //  Create Prefix
            $prefix = '__' . camel_case( $event );

            //  Set
            $canBeTriggered = ( sizeof( $this->_mapMethods( $prefix ) ) > 0 || isset( $this->_event_bindings[$event] ) );

            //  Store
            getTS()->writeStorage( $cacheKey, $canBeTriggered );
        }

        //  Return
        return $canBeTriggered;
    }

    /**
     * Trigger the Events
     */
    public function triggerEvent( $event, $force = false )
    {
        //  Check
        if( $force || $this->canBeTriggered( $event ) )
        {
            //  Create Prefix
            $prefix = '__' . camel_case( $event );

            //  Trigger Methods
            $this->_triggerMethods( $prefix );

            //  Fire Events
            return $this->fireEvent( $event );
        }
    }


    /**
     * Clear Cache
     *
     * @return $this
     */
    public function clearCache()
    {
        //  Check if Tags Supported
        if( method_exists( Cache::getStore(), 'tags') )
        {
            //  Clear Cache
            self::flushCache( $this->uniqueCacheTags() );
        }
        else
        {
            //  Clear Cache
            //\Artisan::call( 'cache:clear' );
        }

        //  Return
        return $this;
    }


    /**
     * Store Event for Before Create
     *
     * @param callable $callback
     * @return $this
     */
    public function beforeCreate( callable $callback )
    {
        //  Store
        return $this->captureEvent( self::$BEFORE_CREATE, $callback );
    }

    /**
     * Store Event for After Create
     *
     * @param callable $callback
     * @return $this
     */
    public function afterCreate( callable $callback )
    {
        //  Store
        return $this->captureEvent( self::$AFTER_CREATE, $callback );
    }

    /**
     * Store Event for Before Update
     *
     * @param callable $callback
     * @return $this
     */
    public function beforeUpdate( callable $callback )
    {
        //  Store
        return $this->captureEvent( self::$BEFORE_UPDATE, $callback );
    }

    /**
     * Store Event for After Update
     *
     * @param callable $callback
     * @return $this
     */
    public function afterUpdate( callable $callback )
    {
        //  Store
        return $this->captureEvent( self::$AFTER_UPDATE, $callback );
    }

    /**
     * Store Event for Before Save
     *
     * @param callable $callback
     * @return $this
     */
    public function beforeSave( callable $callback )
    {
        //  Store
        return $this->captureEvent( self::$BEFORE_SAVE, $callback );
    }

    /**
     * Store Event for After Save
     *
     * @param callable $callback
     * @return $this
     */
    public function afterSave( callable $callback )
    {
        //  Store
        return $this->captureEvent( self::$AFTER_SAVE, $callback );
    }

    /**
     * Store Event for Before Delete
     *
     * @param callable $callback
     * @return $this
     */
    public function beforeDelete( callable $callback )
    {
        //  Store
        return $this->captureEvent( self::$BEFORE_DELETE, $callback );
    }

    /**
     * Store Event for After Delete
     *
     * @param callable $callback
     * @return $this
     */
    public function afterDelete( callable $callback )
    {
        //  Store
        return $this->captureEvent( self::$AFTER_DELETE, $callback );
    }

    /**
     * Store Event for Before Trash
     *
     * @param callable $callback
     * @return $this
     */
    public function beforeTrash( callable $callback )
    {
        //  Store
        return $this->captureEvent( self::$BEFORE_TRASH, $callback );
    }

    /**
     * Store Event for After Trash
     *
     * @param callable $callback
     * @return $this
     */
    public function afterTrash( callable $callback )
    {
        //  Store
        return $this->captureEvent( self::$AFTER_TRASH, $callback );
    }

    /**
     * Store Event for Before Restore
     *
     * @param callable $callback
     * @return $this
     */
    public function beforeRestore( callable $callback )
    {
        //  Store
        return $this->captureEvent( self::$BEFORE_RESTORE, $callback );
    }

    /**
     * Store Event for After Restore
     *
     * @param callable $callback
     * @return $this
     */
    public function afterRestore( callable $callback )
    {
        //  Store
        return $this->captureEvent( self::$AFTER_RESTORE, $callback );
    }

    /**
     * Capture Event
     *
     * @param $event
     * @param $callback
     * @return $this
     */
    public function captureEvent( $event, $callback )
    {
        //  Check
        if( !isset( $this->_event_bindings[$event] ) )  $this->_event_bindings[$event] = array();

        //  Store the Callback
        $this->_event_bindings[$event][] = $callback;

        //  Return
        return $this;
    }

    /**
     * Fire Event
     *
     * @param $event
     * @param array $params
     * @return $this
     */
    public function fireEvent( $event, $params = array() )
    {
        //  Get Callbacks
        $callbacks = ( isset( $this->_event_bindings[$event] ) ? $this->_event_bindings[$event] : array() );

        //  Response
        $response = null;

        //  Loop Each
        foreach( $callbacks as $callback )
        {
            //  Run Callback
            $response = call_user_func_array( $callback, array_merge( array( $this ), $params ) );

            //  Check
            if( $response === false )   break;
        }

        //  Return
        return $response;
    }
}
