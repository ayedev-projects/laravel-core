<?php namespace Ayedev\LaravelCore\Traits;

trait AclTrait
{
    /**
     * Boot Trait
     */
    public static function bootAclTrait()
    {
        //  Listen Deleting
        static::deleting( function( $user )
        {
            //  Check for Delete
            if( !method_exists( $user, 'isForceDeleting' ) || $user->isForceDeleting() )
            {
                //  Sync Roles & Permissions
                $user->roles()->sync( [] );
                $user->perms()->sync( [] );
            }
        } );
    }


    /**
     * Get Permissions IDs
     *
     * @return mixed
     */
    public function permissions_ids()
    {
        //  Permissions Conf
        $permsConf = $this->permsConf();

        //  Permission IDs
        $permission_ids = $this->perms()->pluck( $permsConf['key2'] )->all();

        //  Loop Each Roles
        foreach( $this->roles()->get() as $role )
        {
            //  Merge
            $permission_ids = array_merge( $permission_ids, $role->perms()->pluck( $permsConf['key2'] )->all() );
        }

        //  Return
        return array_unique( array_filter( $permission_ids ) );
    }

    /**
     * Get Permissions
     *
     * @return mixed
     */
    public function permissions()
    {
        //  Permission Class
        $permissionClass = $this->permissionModelClass();

        //  Permission IDs
        $permission_ids = $this->permissions_ids();

        //  Check
        if( !$permission_ids )  $permission_ids = array( -1 );

        //  Return
        return $permissionClass::whereIn( 'id', $permission_ids );
    }

    /**
     * Get Permissions Attribute
     *
     * @return mixed
     */
    public function getPermissionsAttribute()
    {
        //  Permissions Conf
        $permsConf = $this->permsConf();

        //  Return
        return $this->perms()->pluck( $permsConf['key2'] )->all();
    }



    /**
     * Check User Can
     *
     * @param string $ability
     * @param array $arguments
     * @return bool
     */
    public function can( $ability, $arguments = [] )
    {
        //  Check From ACL First
        $allowed = $this->aclCan( $ability );

        //  Check
        if( !$allowed )
        {
            //  Load from Gate
            $allowed = parent::can( $ability, $arguments );
        }

        //  Return
        return $allowed;
    }

    /**
     * Check Can ANy
     *
     * @param string $permissions
     * @return bool
     */
    public function canAny( $permissions )
    {
        //  Return
        return $this->aclCanAny( $permissions );
    }

    /**
     * Check Can Access Any
     *
     * @param string|array $permissions
     * @return bool
     */
    public function aclCanAny( $permissions )
    {
        //  Check
        if( !is_array( $permissions ) ) $permissions = explode( '|', $permissions );

        //  Return
        return call_user_func_array( array( $this, 'aclCan' ), $permissions );
    }

    /**
     * Check Can Access
     *
     * @return bool
     */
    public function aclCan()
    {
        //  Master Override
        if( $this->isAdminUser() )  return true;

        //  Valid
        $valid = false;

        //  IDs
        $ids = $this->permissions_ids();

        //  Loop Each
        foreach( func_get_args() as $permissions_list )
        {
            //  Permission IDs
            $permission_ids = $this->extractPermissionIDs( $permissions_list );

            //  Intersect
            $i = array_intersect( $ids, $permission_ids );

            //  Check
            if( sizeof( $i ) == sizeof( $permission_ids ) )
            {
                //  Set Valid
                $valid = true;
                break;
            }
        }

        //  Return
        return $valid;
    }
}