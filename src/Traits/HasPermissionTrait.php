<?php namespace Ayedev\LaravelCore\Traits;

trait HasPermissionTrait
{
    /** Get Permissions Configurations */
    abstract protected function permsConf();

    /**
     * Get Permissions Class
     *
     * @return string
     */
    protected function permissionModelClass()
    {
        //  Get Conf
        $conf = $this->permsConf();

        //  Return
        return $conf['model'];
    }


    /**
     * Get the Permissions
     *
     * @return mixed
     */
    public function perms()
    {
        //  Conf
        $conf = $this->permsConf();

        //  Return
        return $this->belongsToMany( $conf['model'], $conf['table'] , $conf['key1'] , $conf['key2'] );
    }

    /**
     * Get Permission Info
     *
     * @param $name
     * @return mixed
     */
    public function getPermissionInfo( $name )
    {
        //  Model Class
        $class = $this->permissionModelClass();

        //  Return
        return $class::getPermission( $name );
    }

    /**
     * Extract the Permission ID
     *
     * @param $obj
     * @return mixed
     */
    protected function extractPermissionID( $obj )
    {
        //  ID
        $id = $obj;

        //  Check Object
        if( is_object( $obj ) )
        {
            //  Get the ID
            $id = $obj->getID();
        }
        //  Check for Array
        else if( is_array( $obj ) )
        {
            //  Get the ID
            $id = $obj['id'];
        }
        //  Check for Non-Numeric
        else if( !is_numeric( $obj ) )
        {
            //  Search
            $s = $this->getPermissionInfo( $obj );

            //  Check
            if( $s )    $id = $s->getID();
            //else    $id = null;
        }

        //  Return
        return $id;
    }

    /**
     * Extract Permission IDs
     *
     * @param $list
     * @return array
     */
    protected function extractPermissionIDs( $list )
    {
        //  IDs
        $ids = array();

        //  Loop Each
        foreach( (array)$list as $val )
        {
            //  Push to List
            array_push( $ids, $this->extractPermissionID( $val ) );
        }

        //  Return
        return array_unique( array_filter( $ids ) );
    }

    /**
     * Check against any Permission
     *
     * @param $permissions
     * @return mixed
     */
    public function hasAnyPermission( $permissions )
    {
        //  Check
        if( !is_array( $permissions ) ) $permissions = explode( ',', $permissions );

        //  Return
        return call_user_func_array( array( $this, 'hasPermission' ), $permissions );
    }

    /**
     * Check Has Permission
     *
     * @return bool
     */
    public function hasPermission()
    {
        //  Valid
        $valid = false;

        //  Get the Permissions IDs
        $ids = $this->perms()->pluck( 'id' )->all();

        //  Loop Each
        foreach( func_get_args() as $permissions_list )
        {
            //  Permission IDs
            $permission_ids = $this->extractPermissionIDs( $permissions_list );

            //  Intersect
            $i = array_intersect( $ids, $permission_ids );

            //  Check
            if( sizeof( $i ) == sizeof( $permission_ids ) )
            {
                //  Set Valid
                $valid = true;
                break;
            }
        }

        //  Return
        return $valid;
    }

    /**
     * Attach Permission
     *
     * @param $permission
     * @return $this
     */
    public function attachPermission( $permission )
    {
        //  Attach
        $this->perms()->attach( $this->extractPermissionID( $permission ) );

        //  Return
        return $this;
    }

    /**
     * Detach Permission
     *
     * @param $permission
     * @return $this
     */
    public function detachPermission( $permission )
    {
        //  Attach
        $this->perms()->detach( $this->extractPermissionID( $permission ) );

        //  Return
        return $this;
    }

    /**
     * Attach multiple permissions to current role.
     *
     * @param mixed $permissions
     *
     * @return $this
     */
    public function attachPermissions( $permissions )
    {
        //  Loop Each
        foreach( $permissions as $permission )
        {
            //  Attach Permission
            $this->attachPermission( $permission );
        }

        //  Return
        return $this;
    }

    /**
     * Detach multiple permissions from current role
     *
     * @param mixed $permissions
     *
     * @return $this
     */
    public function detachPermissions( $permissions )
    {
        //  Loop Each
        foreach( $permissions as $permission )
        {
            //  Detach Permission
            $this->detachPermission( $permission );
        }

        //  Return
        return $this;
    }

    /**
     * Sync Permissions
     *
     * @param $permissions
     * @param bool $withoutDetach
     * @return $this
     */
    public function syncPermissions( $permissions, $withoutDetach = false )
    {
        //  IDs
        $ids = $this->extractPermissionIDs( $permissions );

        //  Sync
        if( $withoutDetach )    $this->perms()->syncWithoutDetaching( $ids );
        else    $this->perms()->sync( $ids );

        //  Return
        return $this;
    }

    /**
     * Set Permission
     *
     * @param $name
     * @param null $label
     * @param null $desc
     * @return $this
     */
    public function setPermission( $name, $label = null, $desc = null )
    {
        //  Model Class
        $class = $this->permissionModelClass();

        //  Get Permission
        $permission = $class::savePermission( $name, $label, $desc );

        //  Check
        if( !$this->hasPermission() )
        {
            //  Attach Permission
            $this->attachPermission( $permission );
        }

        //  Return
        return $permission;
    }
}