<?php namespace Ayedev\LaravelCore\Traits;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

trait ScopesTrait
{
    /**
     * Select by Count
     *
     * @param Builder $query
     * @param $relClass
     * @param $relCol
     * @param string $index
     * @param string $selfCol
     * @param null $append
     * @return mixed
     */
    protected function orderSelectCountScope( $query, $relClass, $relCol, $index, $selfCol = 'id', $append = null )
    {
        //  Return
        return $query->selectRaw( "( SELECT COUNT(*) FROM " . DB::getTablePrefix() . $relClass::tableName() . " WHERE {$relCol} = " . DB::getTablePrefix() . $this->getTable() . ".{$selfCol} {$append} ) as " . $index );
    }

    /**
     * Order by Count
     *
     * @param Builder $query
     * @param $relClass
     * @param $relCol
     * @param string $dir
     * @param string $selfCol
     * @param null $append
     * @return mixed
     */
    protected function orderCountScope( $query, $relClass, $relCol, $dir = null, $selfCol = 'id', $append = null )
    {
        //  Return
        return $query->orderByRaw( "( SELECT COUNT(*) FROM " . DB::getTablePrefix() . $relClass::tableName() . " WHERE {$relCol} = " . DB::getTablePrefix() . $this->getTable() . ".{$selfCol} {$append} ) " . ( $dir ? $dir : 'ASC' ) );
    }

    /**
     * Query by Count
     *
     * @param Builder $query
     * @param $relClass
     * @param $relCol
     * @param string $dir
     * @param string $selfCol
     * @param null $append
     * @return mixed
     */
    protected function queryCountScope( $query, $relClass, $relCol, $dir = null, $selfCol = 'id', $append = null, $count = 0 )
    {
        //  Return
        return $query->whereRaw( "( SELECT COUNT(*) FROM " . DB::getTablePrefix() . $relClass::tableName() . " WHERE {$relCol} = " . DB::getTablePrefix() . $this->getTable() . ".{$selfCol} {$append} ) > " . $count );
    }

    /**
     * Order by Field
     *
     * @param Builder $query
     * @param $relClass
     * @param $relCol
     * @param $field
     * @param $field_value
     * @param $order_field
     * @param string $dir
     * @param null $cast_as
     * @param string $selfCol
     * @return mixed
     */
    protected function orderFieldScope( $query, $relClass, $relCol, $field, $field_value, $order_field, $dir = null, $cast_as = null, $selfCol = 'id' )
    {
        //  Return
        return $query->orderByRaw( "( SELECT " . ( $cast_as ? 'CAST( ' : '' ) . "{$order_field}" . ( $cast_as ? " AS {$cast_as} ) " : '' ) . " FROM " . DB::getTablePrefix() . $relClass::tableName() . " WHERE {$relCol} = " . DB::getTablePrefix() . $this->getTable() . ".{$selfCol} AND {$field} = '{$field_value}' ) " . ( $dir ? $dir : 'ASC' ) );
    }

    /**
     * Scope by Field
     *
     * @param Builder $mainQuery
     * @param $term
     * @param $condCol
     * @param $relClass
     * @param $relCol
     * @param null $queryCondition
     * @param string $queryConditionPosition
     * @return mixed
     */
    protected function queryScope( $mainQuery, $term, $condCol, $relClass, $relCol, $queryCondition = null, $queryConditionPosition = 'before' )
    {
        //  Method
        $method = ( $queryCondition ? ( $queryConditionPosition == 'before' ? $queryCondition . 'Where' : 'where' . camel_case( $queryCondition ) ) : 'where' );

        //  Add Condition
        $mainQuery->$method( function ( $query ) use ( $mainQuery, $term, $condCol, $relClass, $relCol )
        {
            //  Unique Key
            $uKey = uniqid();

            //  Add Joins
            $query->join( $relClass::tableName() . " as {$uKey}", $this->getTable() . '.id', '=', "{$uKey}." . $relCol );

            //  Add Condition
            $query->whereIn( "{$uKey}.{$condCol}", (array)$term );
        } );

        //  Return
        return $mainQuery;
    }

    /**
     * Scope by Multi Fields
     *
     * @param Builder $mainQuery
     * @param $field
     * @param $key
     * @param $relClass
     * @param $relCol
     * @param array $conds
     * @param callable $callback
     * @param string $self_field
     * @param null $queryCondition
     * @param string $queryConditionPosition
     * @return mixed
     * @internal param $term
     */
    protected function queryMultiScope( $mainQuery, $field, $key, $relClass, $relCol, $conds = array(), $callback = null, $self_field = 'id', $queryCondition = null, $queryConditionPosition = 'before' )
    {
        //  Method
        $method = ( $queryCondition ? ( $queryConditionPosition == 'before' ? $queryCondition . 'Where' : 'where' . camel_case( $queryCondition ) ) : 'where' );

        //  Add Condition
        $mainQuery->$method( function( $query ) use ( $mainQuery, $field, $key, $relClass, $relCol, $conds, $callback, $self_field )
        {
            //  Unique Key
            $uKey = uniqid();

            //  Add Joins
            $mainQuery->join( $relClass::tableName() . " as {$uKey}", $this->getTable() . ".{$self_field}", '=', "{$uKey}." . $relCol );

            //  Add Query
            if( $field && $key )    $query->where( "{$uKey}." . $field, $key );

            //  Check
            if( $callback )
            {
                //  Run Callback
                call_user_func_array( $callback, array( $query, $uKey, $conds, $key ) );
            }
            else if( !is_null( $conds ) && sizeof( $conds ) > 0 )
            {
                //  Loop Each
                foreach( $conds as $sField => $sParams )
                {
                    //  Check
                    if( !is_array( $sParams ) ) $sParams = array( 'value' => $sParams );

                    //  Value
                    $sValue = $sParams['value'];

                    //  Check
                    if( $sValue === false ) continue;

                    //  Condition
                    $sCondition = strtoupper( isset( $sParams['condition'] ) ? $sParams['condition'] : null );

                    //  Cast As
                    $sCastAs = ( isset( $sParams['cast_as'] ) ? strtoupper( $sParams['cast_as'] ) : null );

                    //  Add Condition
                    if( $sCondition == 'LIKE' ) $query->where( "{$uKey}." . $sField, "LIKE", "%{$sValue}%" );
                    else if( is_array( $sValue ) )  $query->whereIn( "{$uKey}." . $sField, (array)$sValue );
                    else
                    {
                        //  Check
                        if( $sCastAs )
                        {
                            //  Key With Cast
                            $sFieldWithCast = "CAST( " . DB::getTablePrefix() . "{$uKey}.{$sField} AS {$sCastAs} )";

                            //  Add Condition
                            if( $sCondition )   $query->whereRaw( $sFieldWithCast . ' ' . $sCondition . ' ' . $sValue );
                            else    $query->whereRaw( $sFieldWithCast . ' = ' . $sValue );
                        }
                        else
                        {
                            //  Add Condition
                            if( $sCondition )   $query->where( $sField, $sCondition, $sValue );
                            else    $query->where( $sField, $sValue );
                        }
                    }
                }
            }
        } );

        //  Return
        return $mainQuery;
    }

    /**
     * Scope by Multi Fields Inner
     *
     * @param Builder $mainQuery
     * @param $field
     * @param $key
     * @param $relClass
     * @param $relCol
     * @param $relClass2
     * @param $relCol2
     * @param $connectCol
     * @param array $conds
     * @param callable $callback
     * @param string $self_field
     * @param null $queryCondition
     * @param string $queryConditionPosition
     * @return mixed
     * @internal param $term
     */
    protected function queryMultiScopeInner( $mainQuery, $field, $key, $relClass, $relCol, $relClass2, $relCol2, $connectCol, $conds = array(), $callback = null, $self_field = 'id', $queryCondition = null, $queryConditionPosition = 'before' )
    {
        //  Method
        $method = ( $queryCondition ? ( $queryConditionPosition == 'before' ? $queryCondition . 'Where' : 'where' . camel_case( $queryCondition ) ) : 'where' );

        //  Add Condition
        $mainQuery->$method( function( $query ) use ( $mainQuery, $field, $key, $relClass, $relCol, $relClass2, $relCol2, $connectCol, $conds, $callback, $self_field )
        {
            //  Unique Key
            $uKey = uniqid();
            $uKey2 = uniqid();

            //  Add Joins
            $mainQuery->join( $relClass::tableName() . " as {$uKey}", $this->getTable() . ".{$self_field}", '=', "{$uKey}." . $relCol );
            $mainQuery->join( $relClass2::tableName() . " as {$uKey2}", $uKey . ".{$relCol2}", '=', "{$uKey2}." . $connectCol );

            //  Add Query
            if( $field && $key )    $query->where( "{$uKey2}." . $field, $key );

            //  Check
            if( $callback )
            {
                //  Run Callback
                call_user_func_array( $callback, array( $query, $uKey, $conds, $key ) );
            }
            else if( !is_null( $conds ) && sizeof( $conds ) > 0 )
            {
                //  Loop Each
                foreach( $conds as $sField => $sParams )
                {
                    //  Check
                    if( !is_array( $sParams ) ) $sParams = array( 'value' => $sParams );

                    //  Value
                    $sValue = $sParams['value'];

                    //  Check
                    if( $sValue === false ) continue;

                    //  Condition
                    $sCondition = strtoupper( isset( $sParams['condition'] ) ? $sParams['condition'] : null );

                    //  Cast As
                    $sCastAs = ( isset( $sParams['cast_as'] ) ? strtoupper( $sParams['cast_as'] ) : null );

                    //  Add Condition
                    if( $sCondition == 'LIKE' ) $query->where( "{$uKey2}." . $sField, "LIKE", "%{$sValue}%" );
                    else if( is_array( $sValue ) )  $query->whereIn( "{$uKey2}." . $sField, (array)$sValue );
                    else
                    {
                        //  Check
                        if( $sCastAs )
                        {
                            //  Key With Cast
                            $sFieldWithCast = "CAST( " . DB::getTablePrefix() . "{$uKey2}.{$sField} AS {$sCastAs} )";

                            //  Add Condition
                            if( $sCondition )   $query->whereRaw( $sFieldWithCast . ' ' . $sCondition . ' ' . $sValue );
                            else    $query->whereRaw( $sFieldWithCast . ' = ' . $sValue );
                        }
                        else
                        {
                            //  Add Condition
                            if( $sCondition )   $query->where( $sField, $sCondition, $sValue );
                            else    $query->where( $sField, $sValue );
                        }
                    }
                }
            }
        } );

        //  Return
        return $mainQuery;
    }

    /**
     * Scope Pivot by Field
     *
     * @param Builder $mainQuery
     * @param $term
     * @param $condCol
     * @param $relClass
     * @param $relClass2
     * @param $relCol
     * @param callable $subCallback
     * @param string $compareField
     * @param string $pKey
     * @param null $queryCondition
     * @param string $queryConditionPosition
     * @return mixed
     */
    protected function queryPivotScope( $mainQuery, $term, $condCol, $relClass, $relClass2, $relCol, $subCallback = null, $compareField = 'slug', $pKey = 'id', $queryCondition = null, $queryConditionPosition = 'before' )
    {
        //  Method
        $method = ( $queryCondition ? ( $queryConditionPosition == 'before' ? $queryCondition . 'Where' : 'where' . camel_case( $queryCondition ) ) : 'where' );

        //  Add Condition
        $mainQuery->$method( function( $query ) use ( $mainQuery, $term, $condCol, $relClass, $relClass2, $relCol, $subCallback, $compareField, $pKey )
        {
            //  Unique Key
            $uKey = uniqid();

            //  Add Joins
            $mainQuery->join( $relClass::tableName() . " as {$uKey}", $this->getTable() . '.id', '=', "{$uKey}." . $relCol );

            //  Check for Numeric
            if( is_numeric( $term ) || ( is_array( $term ) && is_numeric( $term[0] ) ) )
            {
                //  Add Condition
                $query->whereIn( "{$uKey}.{$condCol}", (array)$term );
            }
            else
            {
                //  Add Condition
                $query->whereIn( "{$uKey}.{$condCol}", function( $subQuery ) use ( $pKey, $relClass2, $compareField, $term, $subCallback )
                {
                    //  Do Select
                    $subQuery->select( $pKey );

                    //  Assign Table
                    $subQuery->from( $relClass2::tableName() );

                    //  Add Condition
                    $subQuery->whereIn( $compareField, (array)$term );

                    //  Check
                    if( $subCallback )  call_user_func_array( $subCallback, array( $subQuery ) );

                    //  Return
                    return $subQuery;
                } );
            }
        } );

        //  Return
        return $mainQuery;
    }
}
