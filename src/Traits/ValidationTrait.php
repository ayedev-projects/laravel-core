<?php namespace Ayedev\LaravelCore\Traits;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

trait ValidationTrait
{
    /** @var Validator $_validator */
    protected $_validator;


    /** Get the Rules */
    abstract public function rules();

    /** Get the Rule Messages */
    abstract public function ruleMessages();

    /**
     * Get the Validator
     *
     * @param array $rules
     * @param array $messages
     * @return Validator
     */
    public function getValidator( $rules = array(), $messages = array() )
    {
        //  Check
        if( !$this->_validator && $rules )
        {
            //  Create Instance
            $this->_validator = Validator::make( $this->attributesToArray(), $rules, $messages );
        }

        //  Return
        return $this->_validator;
    }

    /**
     * Check Validator Throws Exception
     *
     * @return bool
     */
    public function validatorThrowsException()
    {
        //  Return
        return true;
    }


    /**
     * Boot the Trait
     */
    public static function bootValidationTrait()
    {
        //  Listen Saving
        self::saving( function( $model )
        {
            //  Get the Rules
            $rules = $model->rules();

            //  Get the Messages
            $messages = $model->ruleMessages();

            //  Get Validator
            $validator = $model->getValidator( $rules, $messages );

            //  Passed
            $passed = $validator->passes();

            //  Check
            if( !$passed && $model->validatorThrowsException() )    throw new ValidationException( $validator );

            //  Return
            return $passed;
        } );
    }
}