<?php namespace Ayedev\LaravelCore\Traits;

trait MessageItemTrait
{
    use HasRelationTrait;

	/** Get Class for User */
	abstract protected function detectUserClass();

	/** Get Class for Temporary User */
	abstract protected function detectTemporaryUserClass();

	/** Get Class for Message Thread */
	abstract protected function detectMessageThreadClass();

    /** Dispatch Auto Respod */
    abstract public function dispatchAutoRespond();

    /** Dispatch Alert */
    abstract public function dispatchAlert();


    /**
     * Define the Relations
     *
     * @return array
     */
    protected function relations()
    {
        //  Return
        return array(
            'thread' => array(
                'class' => $this->detectMessageThreadClass(),
                'key' => 'thread_id',
                'single' => true
            ),
            'sender' => array(
                'class' => $this->detectUserClass(),
                'key' => 'sender_id',
                'single' => true
            ),
            'temp_sender' => array(
                'class' => $this->detectTemporaryUserClass(),
                'key' => 'sender_id',
                'single' => true
            )
        );
    }


    /**
     * Reply to Message
     *
     * @param $messageTxt
     * @return UserMessageModel
     */
    public function reply( $messageTxt )
    {
		//	Return
		return $this->thread()->reply( $messageTxt, $this->sender_id );
    }


	/**
     * Check is Read
     *
     * @return bool
     */
    public function isRead()
    {
        //  Return
        return !is_null( $this->read_at );
    }

    /**
     * Mark as Read
     *
     * @return $this
     */
    public function markRead()
    {
        //  Check
        if( !$this->isRead() )
        {
            //  Set Date
            $this->read_at = carbon_now();

            //  Save
            $this->save();
        }

        //  Return
        return $this;
    }

    /**
     * Mark Unread
     *
     * @return $this
     */
    public function markUnread()
    {
        //  Check
        if( $this->isRead() )
        {
            //  Clear Date
            $this->read_at = null;

            //  Save
            $this->save();
        }

        //  Return
        return $this;
    }

    /**
     * Get the Message Index
     *
     * @return int
     */
    public function messageIndex()
    {
        //  Index
        $index = null;

        //  Loop Each
        foreach( $this->thread()->messages()->orderBy( 'created_at', 'ASC' )->get() as $i => $msg )
        {
            //  Compare
            if( $msg->getID() == $this->getID() )
            {
                //  Assign
                $index = $i;
                break;
            }
        }

        //  Return
        return $index;
    }

    /**
     * Check if is First Message
     *
     * @return bool
     */
    public function isFirstMessage()
    {
        //  Return
        return ( $this->messageIndex() == 0 );
    }

    /**
     * Check if is Reply
     *
     * @return bool
     */
    public function isReply()
    {
        //  Return
        return ( $this->messageIndex() > 0 );
    }


    /**
     * Check is for Source
     *
     * @return bool
     */
    public function isForSource()
    {
        //  First Message
        $firstMessage = $this->thread();

        //  Return
        return ( $firstMessage->sender_id == $this->sender_id );
    }

    /**
     * Get the Email for Alert
     *
     * @return string
     */
    public function emailForAlert()
    {
        //  First Message
        $firstMessage = $this->thread();

        //  Return
        return ( $firstMessage->isForItem() && $this->isForSource() ? $firstMessage->item()->contactEmail() : $this->getReceiverEmail() );
    }


	/**
	 * Get Receiver Details
	 *
	 * @return array
	 */
	public function getReceiverDetails()
	{
		//  Return
		return ( $this->isForSource() ? $this->thread()->getSenderDetails() : $this->thread()->getReceiverDetails() );
	}

	/**
	 * Get Receiver Name
	 *
	 * @return string
	 */
	public function getReceiverName()
	{
		//	Details
		$details = $this->getReceiverDetails();

		//	Return
		return @$details['name'];
	}

	/**
	 * Get Receiver Email
	 *
	 * @return string
	 */
	public function getReceiverEmail()
	{
		//	Details
		$details = $this->getReceiverDetails();

		//	Return
		return @$details['email'];
	}

	/**
	 * Get Sender Details
	 *
	 * @return array
	 */
	public function getSenderDetails()
	{
		//  Return
		return ( $this->isForSource() ? $this->thread()->getReceiverDetails() : $this->thread()->getSenderDetails() );
	}

	/**
	 * Get Sender Name
	 *
	 * @return string
	 */
	public function getSenderName()
	{
		//	Details
		$details = $this->getSenderDetails();

		//	Return
		return @$details['name'];
	}

	/**
	 * Get Sender Email
	 *
	 * @return string
	 */
	public function getSenderEmail()
	{
		//	Details
		$details = $this->getSenderDetails();

		//	Return
		return @$details['email'];
	}

	/**
	 * Check is for Item
	 *
	 * @return bool
	 */
	public function isForItem()
	{
		//	Return
		return $this->thread()->isForItem();
	}


    /**
     * Check is Valid Sender
     *
     * @return bool
     */
    public function isValidSender()
    {
        //  Return
        return ( $this->temp_sender == '0' );
    }

    /**
     * Check is Temporary Sender
     *
     * @return bool
     */
    public function isTemporarySender()
    {
        //  Return
        return !$this->isValidSender();
    }

    /**
     * Check is Valid Receiver
     *
     * @return bool
     */
    public function isValidReceiver()
    {
        //  Return
        return ( $this->temp_receiver == '0' );
    }

    /**
     * Check is Temporary Receiver
     *
     * @return bool
     */
    public function isTemporaryReceiver()
    {
        //  Return
        return !$this->isValidReceiver();
    }


    /**
     * Get Sender
     *
     * @param bool $use_temp
     * @return mixed
     */
	public function getSender( $use_temp = false )
	{
	    //  Thread
        $thread = $this->thread();

        //	Get the Temporary User Class
        $tempUserClass = $this->detectTemporaryUserClass();

        //  Sender
        $sender = $this->sender();

        //  Check
        if( $thread->isTemporarySender() && $this->sender_id == $thread->sender_id )    $sender = $this->temp_sender();
        if( $thread->isTemporaryReceiver() && $this->sender_id == $thread->receiver_id )    $sender = $this->temp_receiver();

        //  Check
        if( !$use_temp && $sender instanceof $tempUserClass )   $sender = null;

		//	Return
		return $sender;
	}

    /**
     * Get Receiver
     *
     * @param bool $use_temp
     * @return mixed
     */
	public function getReceiver( $use_temp = false )
	{
		//	Get the Temporary User Class
		$tempUserClass = $this->detectTemporaryUserClass();

		//	Get Sender
		$sender = $this->getSender( true );

		//	Get Thread Sender
		$tSender = $this->thread()->getSender( true );

		//	Get Thread Receiver
		$tReceiver = $this->thread()->getReceiver( true );

		//	Receiver
		$receiver = ( typeOf( $sender ) == typeOf( $tSender ) && $sender->getID() == $tSender->getID() ? $tReceiver : $tSender );

		//	Check
		if( !$use_temp && $receiver instanceof $tempUserClass )	$receiver = null;

		//	Return
		return $receiver;
	}

	/**
     * Generate the Reply Link
     *
     * @param bool $forReceiver
     * @return string
     */
    public function replyLink( $forReceiver = true )
    {
        //  Link
        $link = null;

		//	Thread
		$thread = $this->thread();

        //  Item
        $item = ( $this->isForItem() ? $thread->item() : null );

        //  Get Sender
        $sender = $this->getSender();

        //  Get Receiver
        $receiver = $this->getReceiver();

        //  Check
        if( $sender && !$forReceiver )
        {
            //  Set Link
            $link = $sender->dashboardLink() . '#thread-' . $thread->key;;

            //  Check Item
            if( $item && $item->user_id == $sender->getID() )
            {
                //  Change Link
                $link = $thread->item()->editLink() . '#thread-' . $thread->key;
            }
        }
        else if( $receiver && $forReceiver )
        {
            //  Set Link
            $link = $receiver->dashboardLink() . '#thread-' . $thread->key;;

            //  Check Item
            if( $item && $item->user_id == $receiver->getID() )
            {
                //  Change Link
                $link = $thread->item()->editLink() . '#thread-' . $thread->key;
            }
        }
        else if( $item )
        {
            //  Set Link
            $link = $thread->item()->viewLink( array( '_thread' => $thread->key ) ) . ( $thread->isReservationQuery() ? '#book-reservation' : '#send-message' );
        }
        else
        {
            //  Set Link
            $link = $thread->getReceiver()->profileLink( array( '_thread' => $thread->key ) ) . '#send-message';
        }

        //  Return
        return $link;
    }
}
