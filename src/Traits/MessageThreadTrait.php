<?php namespace Ayedev\LaravelCore\Traits;

use Illuminate\Database\Eloquent\SoftDeletes;

trait MessageThreadTrait
{
	use HasRelationTrait, ScopesTrait, SoftDeletes;

	/** Get Class for User */
	abstract protected function detectUserClass();

	/** Get Class for Temporary User */
	abstract protected function detectTemporaryUserClass();

	/** Get Class for Item */
	abstract protected function detectItemClass();

	/** Get Class for Message */
	abstract protected function detectMessageClass();

	/** Get Class for Newsletter */
	abstract protected function detectNewsletterClass();

    /**
     * Define the Relations
     *
     * @return array
     */
    protected function relations()
    {
        //  Return
        return array(
            'sender' => array(
                'class' => $this->detectUserClass(),
                'key' => 'sender_id',
                'single' => true
            ),
            'temp_sender' => array(
                'class' => $this->detectTemporaryUserClass(),
                'key' => 'sender_id',
                'single' => true
            ),
            'receiver' => array(
                'class' => $this->detectUserClass(),
                'key' => 'receiver_id',
                'single' => true
            ),
            'temp_receiver' => array(
                'class' => $this->detectTemporaryUserClass(),
                'key' => 'receiver_id',
                'single' => true
            ),
            'item' => array(
                'class' => $this->detectItemClass(),
                'key' => 'item_id',
                'single' => true
            ),
            'message' => array(
                'class' => $this->detectMessageClass(),
                'key' => 'thread_id',
				'relation' => 'hasMany'
            )
        );
    }


    /**
     * Filter by Item
     *
     * @param $query
     * @param $item_id
     * @return mixed
     */
    public function scopeForItem( $query, $item_id )
    {
        //  Return
        return $query->where( 'item_id', $item_id );
    }

	/**
	 * Filter for User
	 *
	 * @param mixed $query
	 * @param mixed $user
	 * @return mixed
	 */
	public function scopeForUser( $query, $user )
	{
		//	Return
		return $query->where( function( $sQuery ) use ( $user )
		{
			//	Add Sender Query
			$sQuery
				->where( function( $subQuery ) use ( $user )
				{
					//	Add Query
					$subQuery->where( 'sender_id', $user->getID() )->where( 'temp_sender', '0' );
				} )
				->orWhere( function( $subQuery ) use ( $user )
				{
					//	Add Query
					$subQuery->where( 'receiver_id', $user->getID() )->where( 'temp_receiver', '0' );
				} );
		} );
	}

	/**
	 * Get Reservation Ones Only
	 *
	 * @param mixed $query
	 * @return mixed
	 */
	public function scopeOnlyReservations( $query )
	{
		//	Return
		return $query->where( 'reservation', '1' );
	}

	/**
	 * Ignore Reservation Ones
	 *
	 * @param mixed $query
	 * @return mixed
	 */
	public function scopeIgnoreReservations( $query )
	{
		//	Return
		return $query->where( 'reservation', '!=', '1' );
	}

	/**
	 * Only Archived Ones
	 *
	 * @param mixed $query
	 * @return mixed
	 */
	public function scopeOnlyArchived( $query )
	{
		//	Return
		return $query->whereNotNull( 'archived_at' );
	}

	/**
	 * Only Fresh Ones
	 *
	 * @param mixed $query
	 * @return mixed
	 */
	public function scopeOnlyFresh( $query )
	{
		//	Return
		return $query->whereNull( 'archived_at' );
	}

	/**
	 * Add the Extra Selects
	 *
	 * @param mixed $query
	 * @param int|null $user_id
	 * @return mixed
	 */
	public function scopeAddExtraSelects( $query, $user_id = null )
	{
		//	Message Class
		$messageClass = $this->detectMessageClass();

		//	Check
		if( is_null( $user_id ) && isLoggedIn() )	$user_id = currentUserID();

		//	Add Select All
		$query->select( $this->getTable() . '.*' );

		//	Add the Total Messages Count
		$this->orderSelectCountScope( $query, $this->detectMessageClass(), 'thread_id', 'total_messages', 'id' );

		//	Add the Unread Messages Count
		$this->orderSelectCountScope( $query, $this->detectMessageClass(), 'thread_id', 'unread_messages', 'id', ( $user_id ? "AND sender_id != " . $user_id : null ) );

		//	Add the Last Message Time
		$query->selectRaw( "( SELECT created_at FROM " . \DB::getTablePrefix() . $messageClass::tableName() . " WHERE thread_id = " . \DB::getTablePrefix() . $this->getTable() . ".id ORDER BY created_at LIMIT 1 ) as last_message_at" );

		//	Return
		return $query;
	}

	/**
	 * Scope with New Messages Only
	 *
	 * @param mixed $query
	 * @param int|null $user_id
	 * @return mixed
	 */
	public function scopeNewOnly( $query, $user_id = null )
	{
		//	Check
		if( is_null( $user_id ) && isLoggedIn() )	$user_id = currentUserID();

		//	Add Query
		$this->queryCountScope( $query, $this->detectMessageClass(), 'thread_id', 'unread_messages', 'id', ( $user_id ? "AND read_at IS NULL AND sender_id != " . $user_id : null ) );

		//	Return
		return $query;
	}

    /**
     * Order By Last Message At
     *
     * @param mixed $query
     * @param string $dir
     * @return mixed
     */
	public function scopeOrderByRecent( $query, $dir = 'DESC' )
	{
		//	Message Class
		$messageClass = $this->detectMessageClass();

		//	Add Query
		$query->orderByRaw( "( SELECT created_at FROM " . \DB::getTablePrefix() . $messageClass::tableName() . " WHERE thread_id = " . \DB::getTablePrefix() . $this->getTable() . ".id ORDER BY created_at LIMIT 1 ) " . $dir );

		//	Return
		return $query;
	}


	/**
     * Validate Thread ID
     *
     * @param $thread_id
     * @return string
     */
    public static function validateThread( $thread_id )
    {
        //  Found
        $found = self::findThread( $thread_id );

        //  Return
        return !is_null( $found );
    }

    /**
     * Find the Thread
     *
     * @param string $thread_id
     * @param null $callback
     * @return MessageThreadModel|null
     */
	public static function findThread( $thread_id, $callback = null )
	{
	    //  Query
        $query = self::whereKey( $thread_id );

        //  Run Callback
        $callback && call_user_func_array( $callback, array( $query ) );

        //  Return
        return $query->first();
	}

    /**
     * Send Message
     *
     * @param $name
     * @param $country
     * @param $phone_number
     * @param $email_address
     * @param $messageText
     * @param string $newsletter
     * @param null $receiver
     * @param null $sender
     * @param array $extras
     * @param null $item_id
     * @param null $lastMessage
     * @param null $receiver_name
     * @param null $receiver_email
     * @param null $reservation
     * @return mixed
     */
    public static function sendMessage( $name, $country, $phone_number, $email_address, $messageText, $newsletter = 'yes', $receiver = null, $sender = null, $extras = array(), $item_id = null, $receiver_name = null, $receiver_email = null, $reservation = 0 )
    {
		//	Demo Instance
		$ins = new static;

		//	Message Class
		$messageClass = $ins->detectMessageClass();

		//	Temporary User Class
		$tempUserClass = $ins->detectTemporaryUserClass();

        //  Fix Sender
        if( !$sender && isLoggedIn() )  $sender = user();

        //  Check
        if( $newsletter == 'yes' )
        {
			//	Newsletter Class
			$newsletterClass = $ins->detectNewsletterClass();

            //  Add Subscription
            $newsletterClass::addSubscription( $email_address, $name );
        }

		//	Create New Thread
		$thread = new static();

		//	Assign Thread ID
		$thread->key = md5( uniqid() );

		//	Assign Other Properties
		if( $item_id )	$thread->item_id = $item_id;
		$thread->reservation = (int)$reservation;
		$thread->country = $country;
		$thread->phone_number = $phone_number;
		$thread->extras = serialize( $extras ? $extras : array() );

        //  Check Receiver
        if( $receiver )
        {
            //  Set Receiver ID
            $thread->receiver_id = $receiver->getID();
        }
		else
		{
			//	Create Temporary User
			$tempReceiver = $tempUserClass::getUser( $receiver_name, $receiver_email, 'message' );

			//	Set the Receiver Details
			$thread->receiver_id = $tempReceiver->getID();
			$thread->temp_receiver = true;
		}

        //  Check Sender
        if( $sender )
        {
            //  Set Sender ID
            $thread->sender_id = $sender->getID();
        }
		else
		{
			//	Create Temporary User
			$tempSender = $tempUserClass::getUser( $name, $email_address, 'message' );

			//	Set the Sender Details
			$thread->sender_id = $tempSender->getID();
			$thread->temp_sender = true;
		}

		//	Save the Thread
		$thread->save();

        //  Create Instance
        $message = new $messageClass;

		//	Set Thread ID
		$message->thread_id = $thread->getID();

		//	Set Sender
		$message->sender_id = $thread->sender_id;

		//	Set the Message
		$message->message = $messageText;

        //  Save
        $message->save();

        //  Dispatch Auto Respond
        $message->dispatchAutoRespond();

        //  Dispatch Alert
        $message->dispatchAlert();

        //  Return
        return $message;
    }


    /**
     * Reply to Message
     *
     * @param $messageTxt
     * @param bool $toSender
     * @return UserMessageModel
     */
    public function reply( $messageTxt, $sender = null, $sender_name = null )
    {
		//	User Class
		$userClass = $this->detectUserClass();

		//	Message Class
		$messageClass = $this->detectMessageClass();

		//	Temporary User Class
		$tempUserClass = $this->detectTemporaryUserClass();

		//	Check Sender
		if( !$sender )
		{
			//	Check
			if( isLoggedIn() && !$sender_name )
			{
				//	Get Sender
				$sender = user();
			}
			else if( $sender && $sender_name )
			{
				//	Get Temporary User
				$sender = $tempUserClass::getUser( $sender_name, $sender, 'message' );
			}
		}

		//	Check Numeric
		if( is_numeric( $sender ) )
		{
			//	Get User
			$sender = $userClass::find( $sender );
		}

		//	Check Again
		if( !$sender )	$sender = $this->receiver();

		//	Create New Message
		$message = new $messageClass;

		//	Set Thread ID
		$message->thread_id = $this->getID();

		//	Set Sender
		$message->sender_id = $sender->getID();

		//	Set Message
		$message->message = $messageTxt;

        //  Save
        $message->save();

        //  Dispatch Alert
        $message->dispatchAlert();

		//	Return
		return $message;
    }


    /**
     * Check is Read
     *
     * @return bool
     */
    public function isRead()
    {
        //  Return
        return ( $this->messages()->addExtraSelect()->having( 'unread_messages' ) == 0 );
    }

    /**
     * Mark Thread Messages Read
     *
     * @return $this
     */
    public function markRead()
    {
        //  Mark Read
		$this->messages()->update( array( 'read_at' => carbon_now() ) );

        //  Return
        return $this;
    }

    /**
     * Mark Thread Messages Unread
     *
     * @return $this
     */
    public function markUnread()
    {
        //  Mark Read
		$this->messages()->update( array( 'read_at' => null ) );

        //  Return
        return $this;
    }

	/**
	 * Archive message
	 *
	 * @return $this
	 */
	public function archive()
	{
		//	Update
		$this->update( array( 'archived_at' => carbon_now() ) );

        //  Mark All Messages Read
        $this->markRead();

		//	Return
		return $this;
	}

	/**
	 * Unarchive message
	 *
	 * @return $this
	 */
	public function unarchive()
	{
		//	Update
		$this->update( array( 'archived_at' => null ) );

		//	Return
		return $this;
	}


	/**
     * Check is For Item
     *
     * @return bool
     */
    public function isForItem()
    {
        //  Return
        return ( $this->item_id > 0 );
    }

    /**
     * Check has Replies
     *
     * @return bool
     */
    public function hasReplies()
    {
        //  Return
        return ( $this->messages()->count() > 1 );
    }

    /**
     * Check is Reservation Query
     *
     * @return bool
     */
	public function isReservationQuery()
	{
        //  Return
        return ( $this->item_id > 0 && $this->reservation == '1' );
	}


	/**
	 * Check is Valid Sender
	 *
	 * @return bool
	 */
	public function isValidSender()
	{
		//  Return
		return ( $this->temp_sender == '0' );
	}

	/**
	 * Check is Temporary Sender
	 *
	 * @return bool
	 */
	public function isTemporarySender()
	{
		//  Return
		return !$this->isValidSender();
	}

	/**
	 * Check is Valid Receiver
	 *
	 * @return bool
	 */
	public function isValidReceiver()
	{
		//  Return
		return ( $this->temp_receiver == '0' );
	}

	/**
	 * Check is Temporary Receiver
	 *
	 * @return bool
	 */
	public function isTemporaryReceiver()
	{
		//  Return
		return !$this->isValidReceiver();
	}


    /**
     * Get the First Message in Thread
     *
     * @return UserMessageModel|null
     */
    public function firstMessage()
    {
        //  Return
        return $this->messages()->orderBy( 'created_at', 'ASC' )->first();
    }

    /**
     * Get the Last Message in Thread
     *
     * @return UserMessageModel|null
     */
    public function lastMessage()
    {
        //  Return
        return $this->messages()->orderBy( 'created_at', 'DESC' )->first();
    }


	/**
	 * Check User is Sender
	 *
	 * @param UserModel|null $user
	 * @return bool
	 */
	public function isSender( UserModel $user = null )
	{
		//  Fix
		$user = $user ?: user();

		//  Get First Message
		$firstMessage = $this->firstMessage();

		//  Get Sender
		$sender = $firstMessage->getSender();

		//  Return
		return ( $sender && $user && $sender->getID() == $user->getID() );
	}

	/**
	 * Check User is Receiver
	 *
	 * @param UserModel|null $user
	 * @return bool
	 */
	public function isReceiver( UserModel $user = null )
	{
		//  Return
		return !$this->isSender( $user );
	}

	/**
	 * Get Receiver
	 *
	 * @return mixed
	 */
	public function getReceiver( $use_temp = false )
	{
		//	Is Temporary
		$isTemporary = $this->isTemporaryReceiver();

		//	Return
		return ( $use_temp && $isTemporary ? $this->temp_receiver() : ( !$isTemporary ? $this->receiver() : null ) );
	}

	/**
	 * Get Receiver Details
	 *
	 * @return array
	 */
	public function getReceiverDetails()
	{
		//	Get Receiver
		$receiver = $this->getReceiver( true );

		//	Get Temporary User Class
		$tempUserClass = $this->detectTemporaryUserClass();

		//  Return
		return array(
			'id' => $this->getAttribute( 'receiver_id' ),
			'name' => $receiver->name,
			'email' => $receiver->email_address,
			'temporary' => $receiver instanceOf $tempUserClass
		);
	}

	/**
	 * Get Receiver Name
	 *
	 * @return string
	 */
	public function getReceiverName()
	{
		//  Get Receiver Details
		$details = $this->getReceiverDetails();

		//  Return
		return $details['name'];
	}

	/**
	 * Get Receiver Email
	 *
	 * @return string
	 */
	public function getReceiverEmail()
	{
		//  Get Receiver Details
		$details = $this->getReceiverDetails();

		//  Return
		return $details['email'];
	}

	/**
	 * Get Sender
	 *
	 * @return mixed
	 */
	public function getSender( $use_temp = false )
	{
		//	Is Temporary
		$isTemporary = $this->isTemporarySender();

		//	Return
		return ( $use_temp && $isTemporary ? $this->temp_sender() : ( !$isTemporary ? $this->sender() : null ) );
	}

	/**
	 * Get Sender Details
	 *
	 * @return array
	 */
	public function getSenderDetails()
	{
		//	Get Sender
		$sender = $this->getSender( true );

		//	Get Temporary User Class
		$tempUserClass = $this->detectTemporaryUserClass();

		//  Return Sender Details
		return array(
			'id' => $this->getAttribute( 'sender_id' ),
			'name' => $sender->name,
			'email' => $sender->email_address,
			'temporary' => $sender instanceOf $tempUserClass
		);
	}

	/**
	 * Get Sender Name
	 *
	 * @return string
	 */
	public function getSenderName()
	{
		//  Get Sender Details
		$details = $this->getSenderDetails();

		//  Return
		return $details['name'];
	}

	/**
	 * Get Sender Email
	 *
	 * @return string
	 */
	public function getSenderEmail()
	{
		//  Get Sender Details
		$details = $this->getSenderDetails();

		//  Return
		return $details['email'];
	}


    /**
     * Get Country
     *
     * @return mixed
     */
    public function getCountry()
    {
        //  Return
        return $this->country;
    }

    /**
     * Get Phone Number
     *
     * @return mixed
     */
    public function getPhoneNumber()
    {
        //  Return
        return $this->phone_number;
    }

    /**
     * Get the Extras
     *
     * @return array
     */
    public function getExtras()
    {
        //  Read Extras
        $extras = ( @unserialize( $this->extras ) ? unserialize( $this->extras ) : array() );

        //  Return
        return $extras;
    }

    /**
     * Check has Extras
     *
     * @return bool
     */
    public function hasExtras()
    {
        //  Get Extras
        $extras = $this->getExtras();

        //  Return
        return ( $extras && sizeof( $extras ) > 0 );
    }

    /**
     * Get Arrival Date
     *
     * @return string
     */
    public function getArrivalDate()
    {
        //  Get Date
        $date = array_get( $this->getExtras(), 'arrival_date' );

        //  Return
        return ( $date ? carbon( $date ) : null );
    }

    /**
     * Get Departure Date
     *
     * @return string
     */
    public function getDepartureDate()
    {
        //  Get Date
        $date = array_get( $this->getExtras(), 'departure_date' );

        //  Return
        return ( $date ? carbon( $date ) : null );
    }

    /**
     * Arrival is flexible
     *
     * @return string
     */
    public function arrivalIsFlexible()
    {
        //  Get Flexible
        $flexible = array_get( $this->getExtras(), 'arrival_date_flexible' );

        //  Return
        return ( $flexible && $flexible == '1' );
    }

    /**
     * Departure is flexible
     *
     * @return string
     */
    public function departureIsFlexible()
    {
        //  Get Flexible
        $flexible = array_get( $this->getExtras(), 'departure_date_flexible' );

        //  Return
        return ( $flexible && $flexible == '1' );
    }

    /**
     * Get Adults Count
     *
     * @return int
     */
    public function getAdultsCount()
    {
        //  Return
        return array_get( $this->getExtras(), 'adults_count', 0 );
    }

    /**
     * Get Children Count
     *
     * @return int
     */
    public function getChildrenCount()
    {
        //  Return
        return array_get( $this->getExtras(), 'children_count', 0 );
    }

    /**
     * Get Infants Count
     *
     * @return int
     */
    public function getInfantsCount()
    {
        //  Return
        return array_get( $this->getExtras(), 'infants_count', 0 );
    }
}
