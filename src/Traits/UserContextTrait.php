<?php namespace Ayedev\LaravelCore\Traits;

trait UserContextTrait
{
    /** @var int $_user_context */
    protected $_user_context = null;

    /** @var bool $strict_user_context */
    static protected $strict_user_context = true;

    /** @var int $global_user_context */
    static protected $global_user_context = null;


    /**
     * Boot Trait
     */
    public static function bootUserContextTrait()
    {
        //  When Saving
        static::saving( function( $record )
        {
            //  Validate User Context
            $context_user = $record->validateUserContext();

            //  Set Created By
            if( !$record->exists )  $record->created_by = $context_user;

            //  Set Updated By
            $record->updated_by = $context_user;
        } );

        //  When Deleting
        static::deleting( function( $record )
        {
            //  Validate User Context
            $context_user = $record->validateUserContext();

            //  Check
            if( !method_exists( $record, 'isForceDeleting' ) )
            {
                //  Set Deleting By
                $record->deleted_by = $context_user;
            }
        } );
    }


    /**
     * Validate User Context
     *
     * @return int
     * @throws \Exception
     */
    public function validateUserContext()
    {
        //  Get Context User
        $context_user = $this->user_context();

        //  Check
        if( is_null( $context_user ) )
        {
            //  Set Nothing
            $context_user = 0;

            //  Check
            if( self::$strict_user_context )
            {
                //  Throw Exception
                throw new \Exception( 'No user context available for model: ' . get_called_class() );
            }
        }

        //  Return
        return $context_user;
    }


    /**
     * Set User Context
     *
     * @param $id
     * @return $this
     */
    public function set_user_context( $id )
    {
        //  Store
        $this->_user_context = $id;

        //  Return
        return $this;
    }

    /**
     * Set Global User Context
     *
     * @param $id
     */
    static public function set_global_user_context( $id )
    {
        //  Store
        self::$global_user_context = $id;
    }

    /**
     * Set Global User Context
     *
     * @param bool $flag
     */
    static public function set_strict_user_context( $flag )
    {
        //  Store
        self::$strict_user_context = (bool)$flag;
    }

    /**
     * Get User Context
     *
     * @return int|mixed|null
     */
    public function user_context()
    {
        //  Read Context
        $context = ( $this->_user_context ? $this->_user_context : ( static::$global_user_context ? static::$global_user_context : user_context() ) );

        //  Check
        if( !$context && $this->user_id )
        {
            //  Assign the Context
            $context = $this->user_id;
        }

        //  Return
        return $context;
    }


    /**
     * User Context Relation Model
     *
     * @return mixed
     */
    protected function user_context_model()
    {
        //  Return
        return $this->resolveClassName( 'UserModel' );
    }

    /**
     * Get Created By
     *
     * @return mixed
     */
    public function created_by()
    {
        //  Return
        return $this->belongsTo( $this->user_context_model(), 'created_by' )->first();
    }

    /**
     * Get Updated By
     *
     * @return mixed
     */
    public function updated_by()
    {
        //  Return
        return $this->belongsTo( $this->user_context_model(), 'updated_by' )->first();
    }

    /**
     * Get Created By
     *
     * @return mixed
     */
    public function deleted_by()
    {
        //  Return
        return ( method_exists( $this, 'isForceDeleting' ) ? $this->belongsTo( $this->user_context_model(), 'deleted_by' )->first() : null );
    }
}