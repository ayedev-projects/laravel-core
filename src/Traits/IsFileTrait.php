<?php namespace Ayedev\LaravelCore\Traits;

use League\Flysystem\Util\MimeType;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;

trait IsFileTrait
{
    /**
     * Set Featured
     *
     * @param bool $flag
     * @return mixed
     */
    public function setFeatured( $flag = true )
    {
        //  Set
        $this->setAttribute( 'featured', (int)( (bool)$flag ) );

        //  Save
        $this->save();
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function relID()
    {
        //  Return
        return $this->getAttribute( 'rel_id' );
    }

    /**
     * Get Type
     *
     * @return mixed
     */
    public function relType()
    {
        //  Return
        return $this->getAttribute( 'rel_type' );
    }

    /**
     * Get Category
     *
     * @return mixed
     */
    public function relCategory()
    {
        //  Return
        return $this->getAttribute( 'category' );
    }

    /**
     * Get Identifier
     *
     * @return mixed
     */
    public function relIdentifier()
    {
        //  Return
        return $this->getAttribute( 'identifier' );
    }


    /**
     * Detect the Type
     *
     * @param $ext
     * @return string
     */
    public function detectType( $ext )
    {
        //  Return
        if( in_array( $ext, array( 'jpg', 'jpeg', 'png', 'bmp', 'gif' ) ) ) return 'image';
        else if( in_array( $ext, array( 'txt', 'doc', 'docx', 'rtf', 'pdf', 'xls', 'xlsx' ) ) ) return 'document';
        else    return 'file';
    }

    /**
     * Sync the File Info
     *
     * @return $this
     */
    public function syncFileInfo()
    {
        //  File Path
        $filePath = $this->path();

		//	Check for Image
		if( $this->isImage() && !$this->isHDFile() && $this->getFileSize( 'kb', false ) > 150 )
		{
			//	Manipulate the Image
			openImage( $filePath )->save( $filePath, 'guess', 85 );
		}

        //  Set File Type
        $this->setAttribute( 'filetype', $this->detectType( pathinfo( $filePath, PATHINFO_EXTENSION ) ) );

        //  Set File Size
        $this->setAttribute( 'filesize', filesize( $filePath ) );

        //  Set File Extension
        $this->setAttribute( 'fileext', pathinfo( $filePath, PATHINFO_EXTENSION ) );

        //  Set File MIME
        $this->setAttribute( 'mime_type', MimeType::detectByFilename( $filePath ) );

        //  Check
        if( $this->isImage() )
        {
            //  Get Image Info
            list( $width, $height ) = getimagesize( $filePath );

            //  Set Dimensions
            $this->setAttribute( 'width', $width );
            $this->setAttribute( 'height', $height );
        }
    }

	/**
	 * Sync File
	 *
	 * @return $this
	 */
	public function syncFile()
	{
		//	Sync Info
		$this->syncFileInfo();

		//	Save
		$this->save();

		//	Return
		return $this;
	}

    /**
     * Check Sync Required
     */
    public function needsSync()
    {
        //  Return
        return ( !$this->filetype || !$this->filesize );
    }

    /**
     * Sync File Info if Required
     */
    public function syncIfRequired()
    {
        //  Check
        if( $this->needsSync() )
        {
            //  Run Sync
            $this->syncFile();
        }

        //  Return
        return $this;
    }

	/**
	 * Get File Size
	 */
	public function getFileSize( $unit = 'b', $toStr = true )
	{
		//	Bytes
		$bytes = ( (float)$this->getAttribute( 'filesize' ) );

		//	Fix the Factor
		if( in_array( $unit, array( 'kb', 'mb', 'gb', 'tb' ) ) )	$bytes /= 1024;
		if( in_array( $unit, array( 'mb', 'gb', 'tb' ) ) )	$bytes /= 1024;
		if( in_array( $unit, array( 'gb', 'tb' ) ) )	$bytes /= 1024;
		if( in_array( $unit, array( 'tb' ) ) )	$bytes /= 1024;

		//	Round
		$bytes = round( $bytes, 2 );

		//	Check to String
		if( $toStr )	$bytes .= ( $toStr ? ' ' . strtoupper( $unit ) : '' );

		//	Return
		return $bytes;
	}

	/**
	 * Get Image Details
	 *
	 * @param bool $to_attr
	 * @param bool $responsive
	 * @param string $accessAlignment
	 * @return array
	 */
	public function getImageInfo( $to_attr = false, $responsive = true, $accessAlignment = 'center' )
	{
		//	Data
		$data = array();

        //  Sync if Required
        $this->syncIfRequired();

		//	Check
		if( $this->isImage() )
		{
			//	Attribute Prefix
			$attrPrefix = ( $to_attr ? 'data-image-' : '' );

            //  Get Width & Height
            $width = $this->width;
            $height = $this->height;

			//	Set Width & Height
			$data[ $attrPrefix . 'width' ] = $width;
			$data[ $attrPrefix . 'height' ] = $height;

			//	Set Shape
			$data[ $attrPrefix . 'shape' ] = ( $width == $height ? 'square' : 'rectangle' );

			//	Set Orientation
			$data[ $attrPrefix . 'orientation' ] = ( $width > $height ? 'wide' : 'tall' );

			//	Set Image Ratio
			$data[ $attrPrefix . 'ratio1' ] = $height / $width;
			$data[ $attrPrefix . 'ratio2' ] = $width / $height;

			//	Check to Attr
			if( $to_attr )
			{
				//	Check Responsive
				if( $responsive )
				{
					//	Set Responsive
					$data[ $attrPrefix . 'responsive' ] = null;

					//	Store Access ALignment
					$data[ $attrPrefix . 'alignment' ] = $accessAlignment;
				}
			}
		}

		//	Return
		return ( $to_attr ? _attrs_to_html( $data ) : $data );
	}


	/**
	 * Check is HD File
	 *
	 * @return bool
	 */
	public function isHDFile()
	{
		//	Return
		return ( substr( $this->getAttribute( 'filename' ), 0, 3 ) == 'hd-' );
	}

	/**
	 * Check is Image
	 *
	 * @return bool
	 */
	 public function isImage()
	 {
		 //	Return
		 return ( $this->getAttribute( 'filetype' ) == 'image' );
	 }

    /**
     * Check File Exists
     *
     * @return bool
     */
    public function fileExists()
    {
        //  Return
        return ( $this->relPath() && file_exists( $this->path() ) );
    }

    /**
     * fileExists ALIAS
     *
     * @return bool
     */
    public function exists()
    {
        //  Return
        return $this->fileExists();
    }

    /**
     * Check file is Safe
     *
     * @return bool
     */
    public function isSafe()
    {
        //  Return
        return ( $this->fileExists() && $this->getAttribute( 'filesize' ) == filesize( $this->path() ) );
    }

    /**
     * Check file is Tampered
     *
     * @return bool
     */
    public function isTampered()
    {
        //  Return
        return !$this->isSafe();
    }

    /**
     * Check has Preview
     *
     * @return bool
     */
    public function hasPreview()
    {
        //  Return
        return ( $this->exists() && $this->getAttribute( 'filetype' ) == 'image' );
    }


    /**
     * Get the Relative Path for File
     *
     * @return string
     */
    public function relPath()
    {
        //  Return
        return $this->getAttribute( 'filepath' );
    }

    /**
     * Get the Path for No Image File
     *
     * @return string
     */
    public function noImageURL()
    {
        //  Return
        return image_url( null );
    }

    /**
     * Get the Path for File
     *
     * @return string
     */
    public function path()
    {
        //  Return
        return assets()->upload( $this->relPath(), false );
    }

    /**
     * Get the URL for File
     *
     * @return string
     */
    public function url()
    {
        //  Check
        if( !$this->exists() )  return $this->noImageURL();

        //  Return
        return assets()->upload( $this->relPath() );
    }

    /**
     * Get Cache URL for File
     *
     * @param $append
     * @return string
     */
    public function cache( $append = null )
    {
        //  Check for Image
        if( $this->getAttribute( 'filetype' ) != 'image' )    return $this->url();

        //  Check
        if( !$this->fileExists() )  return $this->noImageURL();

        //  Return
        return assets()->cacheHandle( $this->relPath(), $append );
    }
}
