<?php namespace Ayedev\LaravelCore\Traits;

trait HasRoleTrait
{
    /** Get Roles Configurations */
    abstract protected function rolesConf();

    /**
     * Get Roles Class
     *
     * @return string
     */
    protected function roleModelClass()
    {
        //  Get Conf
        $conf = $this->rolesConf();

        //  Return
        return $conf['model'];
    }


    /**
     * Get the Roles
     *
     * @return mixed
     */
    public function roles()
    {
        //  Conf
        $conf = $this->rolesConf();

        //  Return
        return $this->belongsToMany( $conf['model'], $conf['table'] , $conf['key1'] , $conf['key2'] );
    }

    /**
     * Get Role Info
     *
     * @param $name
     * @return mixed
     */
    public function getRoleInfo( $name )
    {
        //  Model Class
        $class = $this->roleModelClass();

        //  Return
        return $class::getRole( $name );
    }

    /**
     * Extract the Role ID
     *
     * @param $obj
     * @return mixed
     */
    protected function extractRoleID( $obj )
    {
        //  ID
        $id = $obj;

        //  Check Object
        if( is_object( $obj ) )
        {
            //  Get the ID
            $id = $obj->getID();
        }
        //  Check for Array
        else if( is_array( $obj ) )
        {
            //  Get the ID
            $id = $obj['id'];
        }
        //  Check for Non-Numeric
        else if( !is_numeric( $obj ) )
        {
            //  Search
            $s = $this->getRoleInfo( $obj );

            //  Check
            if( $s )    $id = $s->getID();
            //else    $id = null;
        }

        //  Return
        return $id;
    }

    /**
     * Extract Role IDs
     *
     * @param $list
     * @return array
     */
    protected function extractRoleIDs( $list )
    {
        //  IDs
        $ids = array();

        //  Loop Each
        foreach( (array)$list as $val )
        {
            //  Push to List
            array_push( $ids, $this->extractRoleID( $val ) );
        }

        //  Return
        return array_unique( array_filter( $ids ) );
    }

    /**
     * Check against any Role
     *
     * @param $roles
     * @return mixed
     */
    public function hasAnyRole( $roles )
    {
        //  Check
        if( !is_array( $roles ) ) $roles = explode( ',', $roles );

        //  Return
        return call_user_func_array( array( $this, 'hasRole' ), $roles );
    }

    /**
     * Check Has Role
     *
     * @return bool
     */
    public function hasRole()
    {
        //  Valid
        $valid = false;

        //  Get the Role IDs
        $ids = $this->roles()->pluck( 'id' )->all();

        //  Loop Each
        foreach( func_get_args() as $roles_list )
        {
            //  Role IDs
            $role_ids = $this->extractRoleIDs( $roles_list );

            //  Intersect
            $i = array_intersect( $ids, $role_ids );

            //  Check
            if( sizeof( $i ) == sizeof( $role_ids ) )
            {
                //  Set Valid
                $valid = true;
                break;
            }
        }

        //  Return
        return $valid;
    }

    /**
     * Attach Role
     *
     * @param $role
     * @return $this
     */
    public function attachRole( $role )
    {
        //  Attach
        $this->roles()->attach( $this->extractRoleID( $role ) );

        //  Return
        return $this;
    }

    /**
     * Detach Role
     *
     * @param $role
     * @return $this
     */
    public function detachRole( $role )
    {
        //  Attach
        $this->roles()->detach( $this->extractRoleID( $role ) );

        //  Return
        return $this;
    }

    /**
     * Attach multiple roles to current role.
     *
     * @param mixed $roles
     *
     * @return $this
     */
    public function attachRoles( $roles )
    {
        //  Loop Each
        foreach( $roles as $role )
        {
            //  Attach Role
            $this->attachRole( $role );
        }

        //  Return
        return $this;
    }

    /**
     * Detach multiple roles from current role
     *
     * @param mixed $roles
     *
     * @return $this
     */
    public function detachRoles( $roles )
    {
        //  Loop Each
        foreach( $roles as $role )
        {
            //  Detach Permission
            $this->detachRole( $role );
        }

        //  Return
        return $this;
    }

    /**
     * Sync Roles
     *
     * @param $roles
     * @param bool $withoutDetach
     * @return $this
     */
    public function syncRoles( $roles, $withoutDetach = false )
    {
        //  IDs
        $ids = $this->extractRoleIDs( $roles );

        //  Sync
        if( $withoutDetach )    $this->roles()->syncWithoutDetaching( $ids );
        else    $this->roles()->sync( $ids );

        //  Return
        return $this;
    }

    /**
     * Set Role
     *
     * @param $name
     * @param null $label
     * @param null $desc
     * @return $this
     */
    public function setRole( $name, $label = null, $desc = null )
    {
        //  Model Class
        $class = $this->roleModelClass();

        //  Get Role
        $role = $class::saveRole( $name, $label, $desc );

        //  Check
        if( !$this->hasRole() )
        {
            //  Sync
            $this->roles()->sync( [ $role->getID() ] );
        }

        //  Return
        return $role;
    }
}