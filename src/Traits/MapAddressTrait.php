<?php namespace Ayedev\LaravelCore\Traits;

use Illuminate\Support\Facades\DB;
use KamranAhmed\Geocode\Geocode;
use Illuminate\Support\Facades\Cache;

trait MapAddressTrait
{
    /** @var float $DISTANCE_UNIT_KILOMETERS */
    public static $DISTANCE_UNIT_KILOMETERS = 111.045;

    /** @var float $DISTANCE_UNIT_KILOMETERS2 */
    public static $DISTANCE_UNIT_KILOMETERS2 = 6371;

    /** @var float $DISTANCE_UNIT_MILES */
    public static $DISTANCE_UNIT_MILES      = 69.0;

    /** @var float $DISTANCE_UNIT_MILES2 */
    public static $DISTANCE_UNIT_MILES2     = 3959;

    /** @var float $DISTANCE_UNIT_METRES */
    public static $DISTANCE_UNIT_METRES = 111045.0;


    /**
     * Read the Data from Coordinates
     *
     * @param $address
     * @param null $rememberFor
     * @return mixed
     */
    public function readCoordinates( $address, $rememberFor = null )
    {
		//	Return
        return ( $this->ableToDetectCoordinates() ? read_coordinates_from_address( $address, $rememberFor ) : null );
    }


    /**
     * Scope by Radius
     *
     * @param $query
     * @param $data
     * @param integer $radius
     * @param bool $paged
     * @param string $units
     * @return mixed
     */
    public function scopeRadius( $query, $data, $radius = null, $paged = false, $units = 'K' ) {

        //  Fix Radius
        $radius || $radius = 5;

        //  Lat & Lng
        $lat = ( is_array( $data ) ? $data[0] : null );
        $lng = ( is_array( $data ) ? $data[1] : null );

        //  Check
        if( !$lat || !$lng ) {

            //  Location
            $location = $this->readCoordinates( $data, 60 * 24 * 30 * 12 );

            //  Check
            if( $location ) {

                //  Set Lat & Lng
                $lat = $location['lat'];
                $lng = $location['lng'];
            }
        }

        //  Check
        if( !is_null( $lat ) && !is_null( $lng ) )
        {
            //  Fix Radius
            $radius || $radius = 50;

            //  Get Distance Unit
            $distanceUnit = $this->distanceUnit( $units );

            //  Raw Query
            $rawQuery = "
                ( {$distanceUnit} *
                    acos(
                        cos( radians(" . $lat . ") ) *
                        cos( radians( coord_lat ) ) *
                        cos(
                            radians( coord_lng ) - radians(" . $lng . ")
                        ) +
                        sin( radians(" . $lat . ") ) *
                        sin( radians( coord_lat ) )
                    )
                ) ";

            //  Set Select
            $query->select( $this->getTable() . '.*' );

            //  Add Select
            $query->selectRaw( $rawQuery . " AS distance" );

            //  Set Condition
            if( $paged )    $query->whereRaw( $rawQuery . " <= " . $radius );
            else    $query->having( 'distance', '<=', $radius );
        }

        //  Return
        return $query;
    }

    /**
     * Scope by Radius Better
     *
     * @param $query
     * @param $data
     * @param integer $radius
     * @param bool $paged
     * @param string $units
     * @return mixed
     * @throws \Exception
     */
    public function scopeRadiusBetter( $query, $data, $radius = null, $paged = false, $units = 'K' ) {

        //  Fix Radius
        $radius || $radius = 5;

        //  Lat & Lng
        $lat = ( is_array( $data ) ? $data[0] : null );
        $lng = ( is_array( $data ) ? $data[1] : null );

        //  Check
        if( !$lat || !$lng ) {

            //  Location
            $location = $this->readCoordinates( $data, 60 * 24 * 30 * 12 );

            //  Check
            if( $location ) {

                //  Set Lat & Lng
                $lat = $location['lat'];
                $lng = $location['lng'];
            }
        }

        //  Check
        if( !is_null( $lat ) && !is_null( $lng ) )
        {
            //  Fix Radius
            $radius || $radius = 50;

            //  Get Distance Unit
            $distanceUnit = $this->distanceUnit( $units );

            //  Validate Latitude
            if( !( is_numeric( $lat ) && $lat >= -90 && $lat <= 90 ) )  throw new \Exception( "Latitude must be between -90 and 90 degrees." );

            //  Validate Longitude
            if( !( is_numeric( $lng ) && $lng >= -180 && $lng <= 180 ) )    throw new \Exception( "Longitude must be between -180 and 180 degrees." );

            //  Set Coordinates Data Join
            $query->join( DB::raw( "( SELECT {$lat} AS latpoint, {$lng} AS lngpoint, {$radius} AS radius, {$distanceUnit} AS distance_unit ) as coord_data" ), function( $join )
            {
                //  Join On
                $join->on( DB::raw( '1' ), '=', DB::raw( '1' ) );
            } );

            //  Set Select
            $query->select( $this->getTable() . '.*' );

            //  Raw Query
            $rawQuery = "( coord_data.distance_unit * vincenty( coord_lat, coord_lng, coord_data.latpoint, coord_data.lngpoint ) )";

            //  Add the Select
            $query->selectRaw( "{$rawQuery} AS distance" );

            //  Set Condition
            if( $paged )    $query->whereRaw( $rawQuery . " <= " . $radius );
            else    $query->having( 'distance', '<=', $radius );
        }

        //  Return
        return $query;
    }

    /**
     * Get the Distance Unit
     *
     * @param string $units
     * @return float
     * @throws \Exception
     */
    private function distanceUnit( $units = 'K' )
    {
        //  Validate
        if( $units == 'K' ) {
            return self::$DISTANCE_UNIT_KILOMETERS;
        } elseif ( $units == 'K2' ) {
            return self::$DISTANCE_UNIT_KILOMETERS2;
        } elseif ( $units == 'M' ) {
            return self::$DISTANCE_UNIT_MILES;
        } elseif ( $units == 'M2' ) {
            return self::$DISTANCE_UNIT_MILES2;
        } elseif ( $units == 'ME' ) {
            return self::$DISTANCE_UNIT_METRES;
        } else {
            throw new \Exception( "Unknown distance unit measure '$units'." );
        }
    }


    /** Read the Location */
    abstract public function getLocation();

    /**
     * Get the Map Address
     *
     * @return mixed
     */
    public function getMapAddress() {

        //  Return
        return ( method_exists( $this, 'getLocation' ) ? $this->getLocation() : null );
    }

	/**
	 * Check is Able to Detect Coordinated
	 *
	 * @return bool
	 */
	public function ableToDetectCoordinates()
	{
		//	Return
		return true;
	}

    /**
     * Detect Map Co-ordinates
     *
     * @return $this
     */
    public function detectCoordinates() {

        //  Get Address
        $mapAddress = $this->getMapAddress();

        //  Geocoder
        $location = ( $mapAddress ? $this->readCoordinates( $mapAddress ) : null );

        //  Check
        if( $location ) {

            //  Set Data
            $this->coord_lat = $location['lat'];
            $this->coord_lng = $location['lng'];
        }

        //  Return
        return $this;
    }

    /**
     * Get the Map Location Points
     *
     * @return array
     */
    public function getLocationPoints() {

        //  Lat & Lng
        $lat = $this->coord_lat;
        $lng = $this->coord_lng;

        //  Check
        if( is_null( $lat ) || is_null( $lng ) ) {

            //  Detect Again
            $this->detectCoordinates();

            //  Save
            $this->save();

            //  Lat & Lng
            $lat = $this->coord_lat;
            $lng = $this->coord_lng;
        }

        //  Return
        return array( 'lat' => $lat, 'lng' => $lng );
    }

    /**
     * Get Latitude
     *
     * @return mixed
     */
    public function getLatitude() {

        //  Points
        $points = $this->getLocationPoints();

        //  Return
        return $points['lat'];
    }

    /**
     * Get Longitude
     *
     * @return mixed
     */
    public function getLongitude() {

        //  Points
        $points = $this->getLocationPoints();

        //  Return
        return $points['lng'];
    }
}
