<?php namespace Ayedev\LaravelCore\Traits;

trait HasRelationTrait
{
    /**
     * Listen Calls
     *
     * @param $method
     * @param $parameters
     * @return mixed
     */
    public function __magicCallHasRelationTrait( $method, $parameters )
    {
        //  Get the Relations
        $relations = array_merge( ( property_exists( $this, '_relations' ) ? $this->_relations : array() ), $this->relations() );

        //  Loop Each
        foreach( $relations as $relKey => $props )
        {
            //  Singular
            $singularKey = $relKey;
            $pluralKey = ( isset( $props['plural'] ) ? $props['plural'] : str_plural( $singularKey ) );

            //  Check
            $found = ( $method == $singularKey ? 'single' : ( $method == $pluralKey ? 'collection' : null ) );

            //  Check
            if( $found )
            {
                //  Relation
                $relation = ( isset( $props['relation'] ) ? $props['relation'] : 'belongsTo' );

                //  Result
                $result = call_user_func_array( array( $this, $relation ), array( $props['class'], @$props['key'], @$props['key2'], @$props['key3'], @$props['key4'] ) );

                //  Check
                if( isset( $props['query'] ) )
                {
                    //  Run Callback
                    call_user_func_array( $props['query'], array( $this, $relKey, $props ) );
                }

                //  Check for Pivot
                if( isset( $props['pivot'] ) )
                {
                    //  Check
                    if( $props['pivot'] !== true )
                    {
                        //  Result
                        call_user_func_array( array( $result, 'withPivot' ), (array)$props['pivot'] );
                    }

                    //  Check for Stamp
                    if( @$props['pivot_stamps'] === true )
                    {
                        //  Set Timestamps
                        $result->withTimestamps();
                    }
                }

                //  Check for Callback
                if( isset( $props['callback'] ) )   call_user_func_array( $props['callback'], array( $result, $parameters, $props ) );

                //  Check
                if( $found == 'single' )
                {
                    //  Get First
                    $result = $result->first();

                    //  Check
                    if( !$result )  $result = self::$NODATA;
                }

                //  Return
                return $result;
            }
        }
    }

    /** Read Relations */
    protected function relations() { return array(); }
}