<?php namespace Ayedev\LaravelCore\Traits;

trait SessionInfoTrait
{
	/**
	 * Try to Detect the Address from IP using API
	 *
	 * @param string $ip
	 * @return string
	 */
	public static function approxAddress( $ip = null )
	{
		//	Fix IP
		$ip || $ip = app( 'request' )->ip();

		//	Cache Key
		$cacheKey = md5( $ip . ' to address' );

		//	Try to Read
		$address = \Cache::get( $cacheKey, null );

		//	Check
		if( !$address )
		{
			//	Read from API
			//$response = file_get_contents( 'https://ipapi.co/' . $ip . '/json' );
			$response = file_get_contents( 'http://ipinfo.io/' . $ip );

			//	Convert to JSON
			$responseJSON = @json_decode( $response, true );

			//	Check
			if( $responseJSON && isset( $responseJSON['city'] ) )
			{
				//	Assign Address
				$address = ( @$responseJSON['hostname'] && $responseJSON['hostname'] != 'No Hostname' ? $responseJSON['hostname'] . ', ' : '' ) . $responseJSON['city'] . ', ' . $responseJSON['country'];

				//	Store Cache
				\Cache::put( $cacheKey, $address );
			}
		}

		//	Return
		return $address;
	}

	/**
	 * Get the Sessions for User
	 *
	 * @param int $user_id
	 * @return Collection
	 */
	public static function searchUserSessions( $user_id = null )
	{
		//	Fix
		$user_id || $user_id = currentUserID();

		//	Return
		return self::query()->filterRegistered( $user_id );
	}

	/**
	 * Get the Related Sessions for User ignoring Current One
	 *
	 * @param int $user_id
	 * @return Collection
	 */
	public static function searchUserRelatedSessions( $user_id = null )
	{
		//	Return
		return self::userSessions( $user_id )->ignoreSession( self::currentSessionID() );
	}

	/**
	 * Get Current Browser Session
	 *
	 * return mixed
	 */
	public static function currentSession()
	{
		//	Return
		return self::find( self::currentSessionID() );
	}

	/**
	 * Get Current Session ID
	 *
	 * @return string
	 */
	public static function currentSessionID()
	{
		//	Return
		return app( 'request' )->cookie( config( 'session.cookie' ) );
	}


	/**
	 * Ignore Session ID
	 *
	 * @param  $query
	 * @param  $id
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeIgnoreSession( $query, $id )
	{
		//	Return
		return $query->where( 'id', '!=', $id );
	}

	/**
	 * Filter Guests
	 *
	 * @param  $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeFilterGuests( $query )
	{
	    return $query->whereNull( 'user_id' );
	}

	/**
	 * Filter Registered Users
	 *
	 * @param  $query
	 * @param  $user_id
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeFilterRegistered( $query, $user_id = null )
	{
		//	Add Query
		$query->whereNotNull( 'user_id' )->with( 'user' );

		//	Check
		if( $user_id )	$query->where( 'user_id', $user_id );

		//	Return
	    return $query;
	}


	/**
	 * Get the Address
	 *
	 * @return string
	 */
	public function getAddress()
	{
		//	Address
		$address = $this->address;

		//	Check
		if( !$address )
		{
			//	Get Address
			$address = self::approxAddress( $this->ip );

			//	Check
			if( !$address )	$address = 'Unknown';

			//	Assign
			$this->address = $address;

			//	Save
			$this->save();
		}

		//	Return
		return $address;
	}

	/**
	 * Get Browser Info
	 *
	 * @return string
	 */
	public function getBrowserInfo()
	{
		//	Parse
		$info = getBrowser( $this->user_agent );

		//	Trimmed Version
		$tVersion = head( explode( '.', $info['version'] ) );

		//	Return
		return ( $info ? $info['name'] . ' v' . $tVersion . ' on ' . ucwords( $info['platform'] ) . ( $this->isMobile() ? ' [Mobile]' : ' [Desktop]' ) : '-' );
	}

	/**
	 * Check is Mobile
	 *
	 * @return bool
	 */
	public function isMobile()
	{
		//	Parse
		$info = getBrowser( $this->user_agent );

		//	Return
		return ( $info['is_mobile'] == '1' );
	}

	/**
	 * Get the Related Sessions for User ignoring Current One
	 *
	 * @param int $user_id
	 * @return Collection
	 */
	public function relatedSessions()
	{
		//	Return
		return self::searchUserSessions( $this->user_id )->ignoreSession( $this->id );
	}

	/**
	 * Clear the Related Sessions
	 *
	 * @return $this
	 */
	public function clearRelatedSessions()
	{
		//	Clear
		$this->relatedSessions()->delete();

		//	Return
		return $this;
	}

	/**
	 * Check is Current Session
	 *
	 * @return bool
	 */
	public function isCurrentSession()
	{
		//	Return
		return ( $this->id == self::currentSessionID() );
	}

	/**
	 * Get Last Activity
	 *
	 * @return Carbon
	 */
	public function lastActivity()
	{
		//	Return
		return carbon( $this->last_activity );
	}

	/**
	 * Returns the user that belongs to this entry.
	 */
	public function user()
	{
		//	Return
	    return $this->belongsTo( $this->resolveClassName( 'UserModel' ) );
	}
}
