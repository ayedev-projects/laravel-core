<?php namespace Ayedev\LaravelCore\Traits;

trait MetaTrait
{
    /**
     * Get the Meta Object Index
     *
     * @return string
     */
    public function metaObjectIndex()
    {
        //  Return
        return 'user_id';
    }

    /** Get Meta Object Class */
    abstract public function metaObjectClass();

    /**
     * Get Meta Key Index
     *
     * @return string
     */
    public function metaKeyIndex()
    {
        //  Return
        return 'meta_key';
    }

    /**
     * Get Meta Value Index
     *
     * @return string
     */
    public function metaValueIndex()
    {
        //  Return
        return 'meta_value';
    }


    /**
     * Scope by Meta Key
     *
     * @param $query
     * @param $key
     * @return mixed
     */
    public function scopeMetaKey( $query, $key )
    {
        //  Return
        return $query->where( $this->metaObjectIndex(), $key );
    }

    /**
     * Get Meta Object
     *
     * @return mixed
     */
    public function metaObject()
    {
        //  Return
        return $this->belongsTo( $this->metaObjectClass(), $this->metaObjectIndex() )->first();
    }

    /**
     * Get the Meta Object ID
     *
     * @return string
     */
    public function getMetaObjectID()
    {
        //  Return
        return $this->getAttribute( $this->metaObjectIndex() );
    }

    /**
     * Set the Meta Object ID
     *
     * @param $id
     * @return $this
     */
    public function setMetaObjectID( $id )
    {
        //  Store
        $this->setAttribute( $this->metaObjectIndex(), $id );

        //  Return
        return $this;
    }

    /**
     * Get the Meta Key
     *
     * @return string
     */
    public function getKey()
    {
        //  Return
        return $this->getAttribute( $this->metaKeyIndex() );
    }

    /**
     * Set the Meta Key
     *
     * @param $key
     * @return $this
     */
    public function setKey( $key )
    {
        //  Store
        $this->setAttribute( $this->metaKeyIndex(), $key );

        //  Return
        return $this;
    }

    /**
     * Get the Meta Value
     *
     * @param bool $raw
     * @return mixed
     */
    public function getValue( $raw = false )
    {
        //  Value
        $value = ( $raw ? $this->attributes[$this->metaValueIndex()] : $this->getAttribute( $this->metaValueIndex() ) );

        //  Return
        return ( @unserialize( $value ) ? unserialize( $value ) : $value );
    }

    /**
     * Set the Meta Value
     *
     * @param $value
     * @param bool $raw
     * @return $this
     */
    public function setValue( $value, $raw = false )
    {
        //  The Value
        $theValue = ( is_array( $value ) || is_object( $value ) ? serialize( $value ) : $value );

        //  Store
        if( $raw )  $this->attributes[$this->metaValueIndex()] = $theValue;
        else    $this->setAttribute( $this->metaValueIndex(), $theValue );

        //  Return
        return $this;
    }


    /**
     * Override the Common Meta Value Attribute Getter
     *
     * @return mixed
     */
    public function getMetaValueAttribute()
    {
        //  Return
        return $this->getValue( true );
    }

    /**
     * Override the Common Meta Value Attribute Setter
     *
     * @param $value
     * @return mixed
     */
    public function setMetaValueAttribute( $value )
    {
        //  Return
        return $this->setValue( $value, true );
    }
}