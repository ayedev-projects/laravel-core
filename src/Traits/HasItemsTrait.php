<?php namespace Ayedev\LaravelCore\Traits;

trait HasItemsTrait
{
    /** @var string $_tmpKeyAttribute */
    private $_tmpKeyAttribute = null;

    /** @var string $_tmpValueAttribute */
    private $_tmpValueAttribute = null;

    /** @var bool $_clearOnSsaveMultiple */
    private $_clearOnSsveMultiple = false;

    /** @var int $_multipleItemIndex */
    private $_multipleItemIndex = 0;


    /** Define the Items */
    abstract public function itemsDef();


    /**
     * Listen Magic Call
     *
     * @param $method
     * @param $parameters
     * @return mixed
     */
    public function __magicCallHasItemsTrait( $method, $parameters )
    {
        //  Get the Items Definations
        $defs = $this->itemsDef();

        //  Loop Each
        foreach( $defs as $defKey => $defConfig )
        {
            //  Singular
            $singularKey = $defKey;
            $pluralKey = ( isset( $defConfig['plural'] ) ? $defConfig['plural'] : str_plural( $singularKey ) );

            //  Action
            $action = null;

            //  Check for Actions
            if( $method == 'save' . ucfirst( $singularKey ) )   $action = 'save_single';
            else if( $method == 'add' . ucfirst( $singularKey ) )    $action = 'save_single';
            else if( $method == 'create' . ucfirst( $singularKey ) )    $action = 'save_single';
            else if( $method == 'save' . ucfirst( $pluralKey ) )    $action = 'save_multiple';
            else if( $method == 'new' . ucfirst( $singularKey ) )   $action = 'new';
            else if( $method == 'sync' . ucfirst( $pluralKey ) )    $action = 'sync';
            else if( $method == 'get' . ucfirst( $singularKey ) )   $action = 'get_single';
            else if( $method == 'get' . ucfirst( $pluralKey ) ) $action = 'get_multiple';
            else if( $method == 'getActive' . ucfirst( $pluralKey ) ) $action = 'get_active_multiple';
            else if( $method == 'get' . ucfirst( $pluralKey ) . 'List' ) $action = 'get_multiple_list';
            else if( $method == 'getActive' . ucfirst( $pluralKey ) . 'List' ) $action = 'get_active_multiple_list';
            else if( $method == 'delete' . ucfirst( $singularKey ) )    $action = 'delete';
            else if( $method == 'delete' . ucfirst( $pluralKey ) )    $action = 'delete';
            else if( $method == 'deleteAll' . ucfirst( $pluralKey ) )    $action = 'delete_all';
            else if( $method == 'get' . ucfirst( $pluralKey ) . 'Count' )    $action = 'count';
            else if( $method == 'getActive' . ucfirst( $pluralKey ) . 'Count' )    $action = 'active_count';
            else if( $method == 'get' . ucfirst( $singularKey ) . 'Value' ) $action = 'get_value';
            else if( $method == 'get' . ucfirst( $pluralKey ) . 'Values' ) $action = 'get_values';
            else if( $method == 'get' . ucfirst( $pluralKey ) . 'Avg' ) $action = 'get_values_average';
            else if( $method == 'getActive' . ucfirst( $pluralKey ) . 'Avg' ) $action = 'get_active_values_average';
            else if( $method == 'merged' . ucfirst( $singularKey ) ) $action = 'get_merged';
            else if( $method == 'match' . ucfirst( $singularKey ) . 'Value' )   $action = 'match_value';
            else if( $method == 'set' . ucfirst( $singularKey ) . 'KeyAttribute' )   $action = 'set_key_attribute';
            else if( $method == 'set' . ucfirst( $singularKey ) . 'ValueAttribute' )   $action = 'set_value_attribute';
            else if( $method == 'setClear' . ucfirst( $pluralKey ) . 'OnSaveMultiple' )   $action = 'set_clear_on_save';

            //  Check
            if( $action )
            {
                //  Handle the Action
                return $this->_handleItemAction( $action, $parameters, $defConfig, $defKey );
            }
        }

        //  Return
        return null;
    }

    /**
     * Handle Item Action
     *
     * @param $action
     * @param $parameters
     * @param $defConfig
     * @param $defKey
     * @return mixed
     */
    private function _handleItemAction( $action, $parameters, $defConfig, $defKey )
    {
        //  Result
        $result = $this;

        //  Classname
        $className = $defConfig['class'];

        //  Attributes
        $attributes = ( isset( $defConfig['attributes'] ) ? (array)$defConfig['attributes'] : array() );

        //  Key Attribute
        $keyAttribute = $this->_tmpKeyAttribute ?: ( isset( $defConfig['key_attribute'] ) ? $defConfig['key_attribute'] : 'id' );

        //  Value Attribute
        $valueAttribute = $this->_tmpValueAttribute ?: ( isset( $defConfig['value_attribute'] ) ? $defConfig['value_attribute'] : 'id' );

        //  Switch
        switch( $action )
        {
            //  New
            case 'new':

                //  Create Instance
                $result = new $className();

                //  Assign the Foreign Key
                $result->setAttribute( $defConfig['foreign_key'], $this->getAttribute( $defConfig['key'] ) );
                break;

            //  Get Single
            case 'get_single':

                //  Search
                $records = $this->_handleItemAction( 'get_multiple', array( $parameters[0] ), $defConfig, $defKey );

                //  Set Result
                $result = $records->first();

                //  Check
                if( !$result )  $result = self::$NODATA;
                break;

            //  Get Value
            case 'get_value':

                //  Get Record
                $record = $this->_handleItemAction( 'get_single', array( $parameters[0] ), $defConfig, $defKey );

                //  Set Result
                $result = ( $record && $record !== self::$NODATA ? $record->getAttribute( $valueAttribute ) : ( isset( $parameters[1] ) ? $parameters[1] : self::$NODATA ) );

                //  Fix
                if( !$result )  $result = self::$NODATA;
                break;

            //  Get Values
            case 'get_values':

                //  Get Records
                $records = $this->_handleItemAction( 'get_multiple', $parameters, $defConfig, $defKey );

                //  Result
                $result = array();

                //  Loop Each
                foreach( $records->get() as $record )
                {
                    //  Append
                    $result[ $record->getAttribute( $keyAttribute ) ] = $record->getAttribute( $valueAttribute );
                }
                break;

            //  Get Values Average
            case 'get_values_average':

                //  Get Records
                $records = $this->_handleItemAction( 'get_multiple', array(), $defConfig, $defKey );

                //  Query Result
                $queryResult = $records->selectRaw( 'AVG(' . $valueAttribute . ') as average_value' )->first();

                //  Set Result
                $result = ( $queryResult && $queryResult->average_value ? ( ( $queryResult->average_value / 10 ) * ( isset( $parameters[0] ) ? $parameters[0] : 100 ) ) : 0 );
                break;

            //  Get Active Values Average
            case 'get_active_values_average':

                //  Get Records
                $records = $this->_handleItemAction( 'get_active_multiple', array(), $defConfig, $defKey );

                //  Query Result
                $queryResult = $records->selectRaw( 'AVG(' . $valueAttribute . ') as average_value' )->first();

                //  Set Result
                $result = ( $queryResult && $queryResult->average_value ? ( ( $queryResult->average_value / 10 ) * ( isset( $parameters[0] ) ? $parameters[0] : 100 ) ) : 0 );
                break;

            //  Get Merged Value
            case 'get_merged':

                //  Param Key
                $pKey = $parameters[0];

                //  Param Value
                $pValue = ( isset( $parameters[1] ) ? $parameters[1]  : null );

                //  Meta Key
                $mKey = ( isset( $parameters[2] ) ? $parameters[2]  : $pKey );

                //  Related Object
                $relObject = ( isset( $parameters[3] ) ? $parameters[3] : null );

                //  Related Object Method
                $relObjectMethod = ( isset( $parameters[4] ) ? $parameters[4]  : $pKey );

                //  Param Key #2
                $pKey2 = ( isset( $parameters[5] ) ? $parameters[5]  : $pKey );

                //  Meta Key #2
                $mKey2 = ( isset( $parameters[6] ) ? $parameters[6]  : $mKey );

                //  Check
                if( $pValue ) {

                    //  Store
                    $this->_handleItemAction( 'save_single', array( $mKey, $pValue ), $defConfig, $defKey );
                }
                else
                {
                    //  Get the Value
                    $mergedValue = $this->_handleItemAction( 'get_value', array( $mKey, null ), $defConfig, $defKey );

                    //  Fix
                    if( $mergedValue == self::$NODATA )   $mergedValue = null;

                    //  Check
                    if( !$mergedValue )
                    {
                        //  Get Attribute
                        $mergedValue = $this->getAttribute( $pKey );
                    }

                    //  Check
                    if( !$mergedValue && $relObject )
                    {
                        //  Get the Value
                        $mergedValue = call_user_func_array( array( $relObject, $relObjectMethod ), array( $mKey2 ) );

                        //  Fix
                        if( $mergedValue == self::$NODATA )   $mergedValue = null;

                        //  Check
                        if( !$mergedValue )
                        {
                            //  Load the Meta from Relation
                            $mergedValue = $relObject->getAttribute( $pKey2 );
                        }
                    }

                    //  Check
                    if( !$mergedValue ) $mergedValue = self::$NODATA;

                    //  Set the Result
                    $result = $mergedValue;
                }
                break;

            //  Match Value
            case 'match_value':

                //  Set No Match
                $result = false;

                //  Compare Value
                $cVal = $parameters[1];

                //  Default Value
                $dVal = ( isset( $parameters[2] ) ? $parameters[2] : null );

                //  Get Value
                $value = $this->_handleItemAction( 'get_value', array( $parameters[0] ), $defConfig, $defKey );

                //  Fix
                if( $value == self::$NODATA )   $value = null;

                //  Check
                if( is_null( $value ) && !is_null( $dVal ) )
                {
                    //  Set
                    $value = array( $dVal );
                }

                //  Check
                if( ( ( is_array( $value ) || is_object( $value ) ) && in_array( $cVal, $value ) )
                    || $value == $cVal )
                {
                    //  Set Matched
                    $result = true;
                }
                break;

            //  Get Multiple
            case 'get_multiple':

                //  Query
                $query = ( isset( $defConfig['query'] ) ? ( is_callable( $defConfig['query'] ) ? call_user_func_array( $defConfig['query'], array( $parameters, $defKey, $defConfig ) ) : $defConfig['query'] ) : $className::query() );

                //  Set Result
                $result = $query->where( $defConfig['foreign_key'], $this->getAttribute( $defConfig['key'] ) )
                    ->where( $defConfig['foreign_key'], $this->getAttribute( $defConfig['key'] ) );

                //  Check
                if( sizeof( $parameters ) > 0 )
                {
                    //  Add Condition
                    $result->whereIn( $keyAttribute, fixListIfEmpty( $parameters ) );
                }

                //  Check for Sort
                if( @$defConfig['use_sort'] === true )  $result->orderBy( isset( $defConfig['sort_col'] ) ? $defConfig['sort_col'] : 'sort_order', 'ASC' );
                break;

            //  Get Multiple List
            case 'get_multiple_list':

                //  Set Result
                $result = $this->_handleItemAction( 'get_multiple', array(), $defConfig, $defKey )->get();
                break;

            //  Get Active Multiple
            case 'get_active_multiple':

                //  Set Result
                $result = $this->_handleItemAction( 'get_multiple', $parameters, $defConfig, $defKey );

                //  Set Active Only
                $result->active();

                //  Query
                if( isset( $defConfig['active_query'] ) )   call_user_func_array( $defConfig['active_query'], array( $result, $parameters, $defKey, $defConfig ) );
                break;

            //  Get Active Multiple List
            case 'get_active_multiple_list':

                //  Set Result
                $result = $this->_handleItemAction( 'get_active_multiple', $parameters, $defConfig, $defKey )->get();
                break;

            //  Save Single
            case 'save_single':

                //  Get Params
                $params = $this->_convertObjectParams( $attributes, $parameters );

                //  Search First
                $existing = $this->_handleItemAction( 'get_single', array( @$params[$keyAttribute] ), $defConfig, $defKey );

                //  Create Instance
                $result = ( $existing && $existing !== self::$NODATA ? $existing : new $className() );

                //  Exists
                $exists = $result->exists;

                //  Assign the Foreign Key
                if( !$exists )
                {
                    //  Set ID
                    $result->setAttribute( $defConfig['foreign_key'], $this->getAttribute( $defConfig['key'] ) );

                    //  Check Method
                    if( method_exists( $this, $dynMethod = '__new' . ucfirst( $defKey ) . 'Fill' ) )
                    {
                        //  Run Method
                        call_user_func_array( array( $this, $dynMethod ), array( $result, $defConfig ) );
                    }
                }

                //  Check
                if( @$defConfig['use_sort'] === true )  $result->setAttribute( isset( $defConfig['sort_col'] ) ? $defConfig['sort_col'] : 'sort_order', $this->_multipleItemIndex );

                //  Callback
                $uCallback = null;

                //  Final Params
                $finalParams = array();

                //  Loop Each
                foreach( $params as $key => $val )
                {
                    //  Check
                    if( $key === '__callback' )
                    {
                        //  Set Callback
                        $uCallback = $val;
                    }
                    else
                    {
                        //  Check for ignore
                        if( substr( $key, 0, 11 ) != '__ignore__:' )
                        {
                            //  Store
                            $finalParams[$key] = $val;

                            //  Assign
                            $result->setAttribute( $key, $val );
                        }
                        else
                        {
                            //  Store
                            $finalParams[ substr( $key, 11 ) ] = $val;
                        }
                    }
                }

                //  Check
                if( $uCallback )
                {
                    //  Run Callback
                    call_user_func_array( $uCallback, array( $result, $finalParams, $exists, $parameters, $defConfig ) );
                }

                //  Callback Response
                $callbackResponse = true;

                //  Check Method
                if( method_exists( $this, $dynMethod = '__save' . ucfirst( $defKey ) . 'Process' ) )
                {
                    //  Run Method
                    $callbackResponse = call_user_func_array( array( $this, $dynMethod ), array( $result, $finalParams, $exists, $parameters, $defConfig ) );
                }

                //  Save
                if( $callbackResponse !== false )   $result->save();
                break;

            //  Save Multiple
            case 'save_multiple':

                //  Keys
                $keys = array();

                //  Loop Each
                foreach( $parameters as $i => $paramCollec )
                {
                    //  Set the Index
                    $this->_multipleItemIndex = $i;

                    //  Trigger
                    $item = $this->_handleItemAction( 'save_single', $paramCollec, $defConfig, $defKey );

                    //  Append
                    $keys[] = $item->getAttribute( $keyAttribute );
                }

                //  Clear the Index
                $this->_multipleItemIndex = 0;

                //  Check Clear
                if( $this->_clearOnSsveMultiple )
                {
                    //  Run Sync
                    $this->_handleItemAction( 'sync', $keys, $defConfig, $defKey );
                }

                break;

            //  Set Key Attribute
            case 'set_key_attribute':

                //  Assign
                $this->_tmpKeyAttribute = $parameters[0];
                break;

            //  Set Value Attribute
            case 'set_value_attribute':

                //  Assign
                $this->_tmpValueAttribute = $parameters[0];
                break;

            //  Set Clear on Save
            case 'set_clear_on_save':

                //  Assign
                $this->_clearOnSsveMultiple = (bool)intval( $parameters[0] );
                break;

            //  Sync
            case 'sync':

                //  The Values
                $theValues = fixListIfEmpty( sizeof( $parameters ) > 1 ? $parameters : ( sizeof( $parameters ) > 0 ? (array)$parameters[0] : array() ) );

                //  Search
                $records = $this->_handleItemAction( 'get_multiple', array(), $defConfig, $defKey )->whereNotIn( $keyAttribute, $theValues );

                //  Delete
                $records->delete();
                break;

            //  Delete
            case 'delete':

                //  Delete
                $this->_handleItemAction( 'get_multiple', $parameters, $defConfig, $defKey )->delete();
                break;

            //  Delete All
            case 'delete_all':

                //  Delete
                $this->_handleItemAction( 'get_multiple', array(), $defConfig, $defKey )->delete();
                break;

            //  Count
            case 'count':

                //  Set Result
                $result = $this->_handleItemAction( 'get_multiple', array(), $defConfig, $defKey )->count();
                break;

            //  Active Count
            case 'active_count':

                //  Set Result
                $result = $this->_handleItemAction( 'get_active_multiple', array(), $defConfig, $defKey )->count();
                break;
        }

        //  Return
        return $result;
    }

    /**
     * Convert to Params for Object
     *
     * @param $attributes
     * @param $parameters
     * @param int $index
     * @return array
     */
    private function _convertObjectParams( $attributes, $parameters, $index = null )
    {
        //  Filler Params
        $fillerParams = array();

        //  Raw Params
        $rawParams = ( sizeof( $parameters ) > 0 && !is_null( $index ) ? ( isset( $parameters[$index] ) ? (array)$parameters[$index] : array() ) : ( sizeof( $parameters ) == 1 ? (array)$parameters : $parameters ) );

        //  Count
        $count = 0;

        //  Loop Each
        foreach( $rawParams as $i => $v )
        {
            //  Check for Callback
            if( $v instanceof \Closure )
            {
                //  Store
                $fillerParams['__callback'] = $v;
            }
            else
            {
                //  Check for Array
                if( is_array( $v ) )
                {
                    //  Merge
                    $fillerParams = array_merge( $fillerParams, $v );
                }
                else
                {
                    //  Check
                    if( is_int( $i ) && isset( $attributes[$count] ) )
                    {
                        //  Store
                        $fillerParams[ $attributes[$count] ] = $v;
                    }
                    else
                    {
                        //  Store
                        if( !is_int( $i ) ) $fillerParams[$i] = $v;
                    }
                }
            }

            //  Increase
            $count++;
        }

        //  Return
        return $fillerParams;
    }
}