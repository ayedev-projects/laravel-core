<?php namespace Ayedev\LaravelCore\Traits;

use Ayedev\LaravelCore\ModelSource\FileField;

trait ProgressCalculationTrait
{
    /**
     * Calculate Progress Info
     *
     * @param $defs
     * @return array
     */
    public function calculateProgressInfo( $defs )
    {
        //  Total
        $total = 0;

        //  Outcome
        $outcome = 0;

        //  Completed
        $completed = array();

        //  Not Completed
        $notCompleted = array();

        //  Loop Each
        foreach( $defs as $def )
        {
            //  Key
            $theKey = ( is_array( $def ) ? $def['key'] : $def );

            //  Match Type
            $matchType = ( is_array( $def ) && isset( $def['match'] ) ? $def['match'] : 'default' );

            //  Score
            $score = intval( is_array( $def ) && isset( $def['score'] ) ? $def['score'] : 10 );

            //  Original Score
            $originalScore = $score;

            //  Info
            $info = ( is_array( $def ) && isset( $def['info'] ) ? $def['info'] : 'Please configure ' . camel_case( $theKey ) );

            //  Detected Value
            $theValue = null;

            //  Switch
            switch( $matchType )
            {
                //  Item
                case 'item':

                    //  Get Item Type
                    $itemType = ( is_array( $def ) && isset( $def['item_type'] ) ? $def['item_type'] : 'meta' );

                    //  Method
                    $theMethod = camel_case( 'get_' . $itemType . '_value' );

                    //  Get Result
                    $theValue = $this->{$theMethod}( $theKey );

                    break;

                //  Count
                case 'count':

                    //  Get Count Property Type
                    $propertyType = ( is_array( $def ) && isset( $def['prop_type'] ) ? $def['prop_type'] : 'property' );

                    //  Check
                    if( $propertyType == 'property' )
                    {
                        //  Get Value
                        $theValue = $this->{$theKey};
                    }
                    else
                    {
                        //  Get Count Property Method
                        $propertyMethod = ( is_array( $def ) && isset( $def['prop_method'] ) ? $def['prop_method'] : $theKey );

                        //  Get Result
                        $theValue = $this->{$propertyMethod}();
                    }

                    //  Validate Value
                    $theValue = ( $theValue ? ( is_array( $theValue ) ? sizeof( $theValue ) : ( method_exists( $theValue, 'count' ) ? $theValue->count() : sizeof( $theValue ) ) ) : 0 );

                    //  Expected Target
                    $target = ( is_array( $def ) && isset( $def['target'] ) ? $def['target'] : null );

                    //  Check
                    if( !is_null( $target ) && $theValue > 0 && $theValue < $target )
                    {
                        //  Calculate
                        $score = intval( ceil( ( $theValue / $target ) * $originalScore ) );
                    }

                    break;

                //  Callback
                case 'callback':

                    //  Get Value
                    $theValue = call_user_func_array( $def['callback'], array( $this, $def ) );

                    break;

                //  Default
                default:

                    //  Get Attribute
                    $theValue = ( method_exists( $this, $theKey ) ? $this->{$theKey} : $this->getAttribute( $theKey ) );

                    //  Check
                    if( $theValue instanceof FileField )
                    {
                        //  Exists
                        $exists = $theValue->getFile()->exists();

                        //  Change the Value
                        $theValue = $theValue->getFile()->relPath();

                        //  Check Exists
                        if( !$exists )  $theValue = null;
                    }

                    break;
            }

            //  Check
            if( $theValue )
            {
                //  Add to Completed
                $completed[$theKey] = $score;

                //  Add to Outcome
                $outcome += $score;
            }
            else
            {
                //  Add to Not Completed
                $notCompleted[$theKey] = $info;
            }

            //  Add to Total
            $total += $originalScore;
        }

        //  Return
        return array(
            'completed' => $completed,
            'not_completed' => $notCompleted,
            'progress' => intval( ( $outcome / $total ) * 100 )
        );
    }

    /**
     * Calculate Progress Rate
     *
     * @param $defs
     * @return int
     */
    public function calculateProgressRate( $defs )
    {
        //  Get Progress Data
        $progressData = $this->calculateProgressInfo( $defs );

        //  Return
        return $progressData['progress'];
    }
}