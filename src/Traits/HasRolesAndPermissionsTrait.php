<?php namespace Ayedev\LaravelCore\Traits;

trait HasRolesAndPermissionsTrait
{
    use HasRoleTrait, HasPermissionTrait;
}