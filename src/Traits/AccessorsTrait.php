<?php namespace Ayedev\LaravelCore\Traits;

trait AccessorsTrait
{
    /** Field Mappings **/
    protected $mappings = array();

    /** Mapper Fields **/
    protected $mappers_fields = array();


    /**
     * Init
     */
    public function __initAccessorsTrait()
    {
        //  Add Boolean Mapper
        $this->mappings[] = array(
            'pre' => 'isNot',
            'post' => null,
            'callback' => 'getBooleanFieldInverse',
            'type' => 'bool'
        );
        $this->mappings[] = array(
            'pre' => 'is',
            'post' => null,
            'callback' => 'getBooleanField',
            'type' => 'bool'
        );
        $this->mappings[] = array(
            'pre' => 'set',
            'post' => null,
            'callback' => 'enableBooleanField',
            'type' => 'bool'
        );
        $this->mappings[] = array(
            'pre' => 'setNot',
            'post' => null,
            'callback' => 'disableBooleanField',
            'type' => 'bool'
        );
    }

    /**
     * Register Default Boolean Field
     *
     * @return $this
     */
    protected function registerDefaultBooleanField()
    {
        //  Register
        $this->registerBooleanField( 'active', 'active' );

        //  Return
        return $this;
    }

    /**
     * Register Boolean Field
     *
     * @param $key
     * @param $field
     * @param array $extras
     * @param bool $default
     * @return $this
     */
    public function registerBooleanField( $key, $field = null, $extras = array(), $default = false )
    {
        //  Fix Field
        $field = $field ?: $key;

        //  Append
        $this->mappers_fields[$key] = array(
            'field' => $field,
            'default' => $default,
            'extras' => $extras,
            'type' => 'bool'
        );

        //  Return
        return $this;
    }


    /**
     * Magic Call
     *
     * @param $method
     * @param $parameters
     * @return mixed
     */
    public function __magicCallAccessorsTrait( $method, $parameters )
    {
        //  Detected Data
        $field = $param = $default = $extras = $callback = $type = null;

        //  Loop Each
        foreach( $this->mappings as $mapping )
        {
            //  Break
            $break = false;

            //  Check
            if( ( $mapping['pre'] == '' || substr( $method, 0, strlen( $mapping['pre'] ) ) == $mapping['pre'] ) )
            {
                //  Set Type
                $type = $mapping['type'];

                //  Map Posts
                $mapPosts = ( $mapping['post'] ? (array)$mapping['post'] : null );

                //  Check
                if( $mapPosts )
                {
                    //  Loop
                    foreach( $mapPosts as $mapPost )
                    {
                        //  Check
                        if( substr( $method, -strlen( $mapPost ) ) == $mapPost )
                        {
                            //  Field
                            $field = snake_case( substr( $method, ( $mapping['pre'] == '' ? 0 : strlen( $mapping['pre'] ) ), -strlen( $mapPost ) ) );

                            //  Callback
                            $callback = $mapping['callback'];

                            //  Param
                            $param = snake_case( substr( $method, -strlen( $mapPost ) ) );

                            //  Break
                            $break = true;
                            break;
                        }
                    }
                }
                else
                {
                    //  Field
                    $field = snake_case( substr( $method, strlen( $mapping['pre'] ) ) );

                    //  Callback
                    $callback = $mapping['callback'];

                    //  Break
                    $break = true;
                }
            }

            //  Check
            if( $break )    break;
        }

        //  Check
        if( $field && !isset( $this->mappers_fields[$field] ) )   $field = null;
        if( $field && $type != $this->mappers_fields[$field]['type'] )  $field = null;
        if( !$callback )    $field = null;

        //  Check
        if( $field && method_exists( $this, $callback ) )
        {
            //  Default
            $default = $this->mappers_fields[$field]['default'];

            //  Extras
            $extras = $this->mappers_fields[$field]['extras'];

            //  Return
            $data = $this->{$callback}( $this->mappers_fields[$field]['field'], $param, $parameters, $default, $extras, $field );

            //  Check
            if( $data !== self::$NODATA )  return $data;
        }
        else
        {
            //  Check
            if( substr( $method, 0, 3 ) == 'get' && substr( $method, -4 ) == 'Date' )
            {
                //  Field Key
                $fieldKey = snake_case( substr( $method, 3, -4 ) );

                //  Check
                if( in_array( $fieldKey, $this->getDates() ) )
                {
                    //  Return
                    return $this->manipulateDateTime( $this->getAttribute( $fieldKey ), ( sizeof( $parameters ) > 0 ? $parameters[0] : null ) );
                }
            }
        }

        //  Return
        return null;
    }

    /**
     * Get Boolean Field
     *
     * @param $field
     * @return bool
     */
    protected function getBooleanField( $field )
    {
        //  Field Value
        $field_value = ( $this->getAttribute( $field ) == '1' );

        //  Return
        return $field_value;
    }

    /**
     * Get Boolean Field Inverse
     *
     * @param $field
     * @return bool
     */
    protected function getBooleanFieldInverse( $field )
    {
        //  Return
        return !$this->getBooleanField( $field );
    }

    /**
     * Enable Boolean Field
     *
     * @param $field
     * @return $this
     */
    protected function enableBooleanField( $field )
    {
        //  Return
        return $this->updateBooleanField( $field, true );
    }

    /**
     * Disable Boolean Field
     *
     * @param $field
     * @return $this
     */
    protected function disableBooleanField( $field )
    {
        //  Return
        return $this->updateBooleanField( $field, false );
    }

    /**
     * Update Boolean Field
     *
     * @param $field
     * @param $flag
     * @return $this
     */
    protected function updateBooleanField( $field, $flag )
    {
        //  Set Field
        $this->setAttribute( $field, (int)( (bool) $flag ) );

        //  Save Record
        $this->save();

        //  Return
        return $this;
    }
}