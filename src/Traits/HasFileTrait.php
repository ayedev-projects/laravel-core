<?php namespace Ayedev\LaravelCore\Traits;

use Ayedev\LaravelCore\ModelSource\FileField;
use Ayedev\LaravelCore\ModelSource\FileFieldCollection;

trait HasFileTrait
{
    /** Get the File Definations */
    abstract public function filesDef();


    /**
     * Boot Trait
     */
    public static function bootHasFileTrait()
    {
        //  Listen when Saved
        saveFilesWhenModel( static::class );
    }

    /**
     * Override Sync Original
     *
     * @return $this
     */
    public function syncOriginal()
    {
        //  Run Parent
        parent::syncOriginal();

        //  Check
        if( $this->attributes )
        {
            //  Run the Filler
            $this->__fillFileFields();
        }

        //  Return
        return $this;
    }

    /**
     * Override Set Attribute
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function setAttribute( $key, $value )
    {
        //  Get Definations
        $defs = $this->filesDef();

        //  Check
        if( isset( $defs[$key] ) )
        {
            //  Get Config
            $defConfig = $defs[$key];

            //  Manipulate the Value
            $value = $this->manipulateTheValue( $key, $value, $defConfig );
        }

        //  Return
        return parent::setAttribute( $key, $value );
    }

    /**
     * Manipulate the File Value
     */
    protected function manipulateTheValue( $key, $value, $defConfig )
    {
        //  Current
        $currentValue = @$this->attributes[$key];

        //  Is Instance
        $isInstance = ( $currentValue instanceof FileField || $currentValue instanceof FileFieldCollection );

        //  Check
        if( is_null( $value ) && $isInstance )   $currentValue->clear();

        //  Check
        if( !is_null( $value ) )
        {
            //  Assign the Property
            $value = ( $isInstance ? $currentValue->add( $value, null, true ) : $this->_generateField( $key, $defConfig, $value ) );
        }

        //  Is Virtual
        $isVirtual = ( @$defConfig['virtual'] === true );

        //  Store to Original Also
        if( $isVirtual )    $this->original[$key] = $value;

        //  Return
        return $value;
    }

    /**
     * Generate the Field
     *
     * @param $defKey
     * @param $defConfig
     * @param $value
     * @param bool $fromOriginal
     * @return FileField
     */
    protected function _generateField( $defKey, $defConfig, $value, $fromOriginal = false )
    {
        //  Is Multiple
        $isMultiple = ( @$defConfig['multiple'] === true );

        //  Check
        if( !$isMultiple && !$value && $fromOriginal )
        {
            //  Set Value
            $value = ( $this->exists && $this->getOriginal( $defKey ) ? 'load:' . $this->getOriginal( $defKey ) : null );
        }

        //  Return
        return ( $isMultiple ? new FileFieldCollection( $this, $defKey, $defConfig, $value ) : new FileField( $this, $defKey, $defConfig, $value ) );
    }

    /**
     * Fillup the File Field
     */
    protected function __fillFileFields()
    {
        //  Get Definations
        $defs = $this->filesDef();

        //  Loop Each
        foreach( $defs as $defKey => $defConfig )
        {
            //  Is Virtual
            $isVirtual = ( @$defConfig['virtual'] === true );

            //  Assign the Property
            $this->attributes[$defKey] = $this->_generateField( $defKey, $defConfig, null, true );
            if( $isVirtual )    $this->original[$defKey] = $this->attributes[$defKey];
        }
    }

    /**
     * Listen Magic Call
     * @param $method
     */
    protected function __magicCallHasFileTrait( $method )
    {
        //  Get Definations
        $defs = $this->filesDef();

        //  Loop Each
        foreach( $defs as $defKey => $defConfig )
        {
            //  Access Type
            $accessType = null;

            //  Accessor Key
            $accessorKey = ucfirst( camel_case( isset( $defConfig['accessor'] ) ? $defConfig['accessor'] : str_singular( $defKey ) ) );

            //  Check for Get First
            if( $method == 'primary' . $accessorKey )
            {
                //  Set Method
                $method = $defKey;

                //  Set
                $accessType = 'primary';
            }
            else if( $method == 'first' . $accessorKey )
            {
                //  Set Method
                $method = $defKey;

                //  Set
                $accessType = 'first';
            }
            else if( $method == 'featured' . $accessorKey )
            {
                //  Set Method
                $method = $defKey;

                //  Set
                $accessType = 'featured';
            }

            //  Check
            if( $method == $defKey )
            {
                //  Get Value
                $value = $this->getAttribute( $defKey );

                //  Check
                if( !$value && @$defConfig['multiple'] === true )   $this->setAttribute( $defKey, array() );

                //  Data to Return
                $data = $this->getAttribute( $defKey );

                //  Check for Multiple
                if( @$defConfig['multiple'] === true )
                {
                    //  Check
                    if( $accessType == 'first' )
                    {
                        //  Get First
                        $data = $data->firstFile();
                    }
                    else if( $accessType == 'featured' )
                    {
                        //  Get Featured
                        $data = $data->featuredFile();
                    }
                    else if( $accessType == 'primary' )
                    {
                        //  Org Data
                        $orgData = $data;

                        //  Get Featured
                        $data = $orgData->featuredFile();

                        //  Check
                        if( !$data )
                        {
                            //  Get the First
                            $data = $orgData->firstFile();
                        }
                    }
                }

                //  Check Data
                if( is_null( $data ) )  $data = self::$NODATA;

                //  Return
                return $data;
            }
        }
    }

    /**
     * Save the Files
     */
    public function saveTheFiles()
    {
        //  Get Definations
        $defs = $this->filesDef();

        //  Loop Each
        foreach( $defs as $defKey => $defConfig )
        {
            //  Check
            if( $this->hasAttribute( $defKey ) )
            {
                //  Run When Saved
                $this->{$defKey}()->whenSaved();
            }
        }
    }


    /**
     * Get 404 File as backup
     *
     * @return string
     */
    public function file404( $key )
    {
        //  Return
        return assets()->image( 'placeholders/no_image.jpg' );
    }

    /**
     * Get 404 Cache File as backup
     *
     * @return string
     */
    public function cacheFile404( $key )
    {
        //  Return
        return $this->file404( $key );
    }
}