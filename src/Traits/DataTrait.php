<?php namespace Ayedev\LaravelCore\Traits;

use Ayedev\LaravelCore\Core\SimpleData;

trait DataTrait
{
    /** @var array $_data */
    protected $_data = array();

    /** @var bool $_manipulate */
    protected $_manipulate = true;


    /**
     * Copy data from another Record
     *
     * @param mixed $data
     * @return $this
     */
    public function copy( $data )
    {
        //  Check for Record Instance
        if( $data instanceof SimpleData )
        {
            //  Copy Data
            $this->setData( $data->getData() );
        }
        else
        {
            //  Set Data
            $this->setData( $data );
        }

        //  Return
        return $this;
    }

    /**
     * Get Data
     *
     * @return array
     */
    public function getData()
    {
        //  Return
        return $this->_data;
    }

    /**
     * Clear all Data
     *
     * @return $this
     */
    public function clearData()
    {
        //  Clear
        $this->_data = array();

        //  Return
        return $this;
    }

    /**
     * Set Data
     *
     * @param $data
     * @param bool $clear
     * @return $this
     */
    public function setData( $data, $clear = false )
    {
        //  Check for Clear
        if( $clear )
        {
            //  Clear
            $this->_data = array();
        }

        //  Check
        if( $this->_manipulate )
        {
            //  Loop Each
            foreach( $data as $key => $val )
            {
                //  Assign
                $this->set( $key, $val );
            }
        }
        else
        {
            //  Merge
            $this->_data += $data;
        }

        //  Return
        return $this;
    }

    /**
     * Set Property
     *
     * @param $key
     * @param $val
     * @return $this
     */
    public function set( $key, $val )
    {
        //  Check
        if( !$val instanceof \Carbon\Carbon && $this->_manipulate && ( is_array( $val ) || is_object( $val ) ) && !$val instanceof SimpleData )  $val = new static( $val );

        //  Store Value
        $this->_data[$key] = $val;

        //  Return
        return $this;
    }

    /**
     * Get Property
     *
     * @param $key
     * @param null $def
     * @return mixed|null
     */
    public function get( $key, $def = null )
    {
        //  Value
        $value = $def;

        //  Check
        if( $this->exists( $key ) )
        {
            //  Set
            $value = $this->_data[$key];
        }

        //  Return
        return $value;
    }

    /**
     * Check Property Exists
     *
     * @param $key
     * @return bool
     */
    public function exists( $key )
    {
        //  Return
        return isset( $this->_data[$key] );
    }

    /**
     * Remove Property
     *
     * @param $key
     * @return $this
     */
    public function remove( $key )
    {
        //  Check
        if( $this->exists( $key ) )
        {
            //  Remove
            unset( $this->_data[$key] );
        }

        //  Return
        return $this;
    }


    /**
     * Set Property
     *
     * @param $key
     * @param $val
     */
    public function __set( $key, $val )
    {
        //  Set
        $this->set( $key, $val );
    }

    /**
     * Get Property
     *
     * @param $key
     * @return mixed|null
     */
    public function __get( $key )
    {
        //  Return
        return $this->get( $key );
    }

    /**
     * Check Property
     *
     * @param $key
     * @return bool
     */
    public function __isset( $key )
    {
        //  Return
        return $this->exists( $key );
    }

    /**
     * Unset Property
     *
     * @param $key
     */
    public function __unset( $key )
    {
        //  Remove
        $this->remove( $key );
    }

    /**
     * Clone Record
     *
     * @return static
     */
    public function __clone()
    {
        //  Return
        return new static( $this );
    }

    /**
     * Magic Call Enabler
     *
     * @param $method
     * @param $arguments
     * @return mixed
     */
    public function __call( $method, $arguments )
    {
        //  Valid
        $valid = false;

        //  Result
        $result = null;

        //  Check for Setter
        if( substr( $method, 0, 3 ) == 'set' )
        {
            //  Property
            $property = snake_case( substr( $method, 3 ) );

            //  Set Property
            $this->set( $property, $arguments[0] );

            //  Set Result
            $result = $this;

            //  Set Valid
            $valid = true;
        }
        else if( substr( $method, 0, 3 ) == 'get' )
        {
            //  Property
            $property = snake_case( substr( $method, 3 ) );

            //  Set Result
            $result = $this->get( $property, ( sizeof( $arguments ) > 0 ? $arguments[0] : null ) );

            //  Set Valid
            $valid = true;
        }

        //  Check
        if( !$valid )   user_error( sprintf( 'Method %s doesn\'t exist.', $method ) );

        //  Return
        return $result;
    }


    /**
     * Filter the Data
     *
     * @param $callback
     * @return static
     */
    public function pluckData( callable $callback )
    {
        //  New Data
        $data = new static;

        //  Loop Each
        foreach( $this->toArray() as $key => $val )
        {
            //  Check
            if( call_user_func_array( $callback, array( $key, $val, $this ) ) === true )
            {
                //  Store
                $data->set( $key, $val );
            }
        }

        //  Return
        return $data;
    }

    /**
     * Get the data to Array
     *
     * @return array
     */
    public function toArray()
    {
        //  Data
        $data = array();

        //  Loop
        foreach( $this->getData() as $key => $val )
        {
            //  Is Object
            $isObject = ( is_object( $val ) && method_exists( $val, 'getData' ) );

            //  Store
            $data[$key] = ( $isObject ? $val->toArray() : $val );
        }

        //  Return
        return $data;
    }

    /**
     * Get the data to JSON
     *
     * @return string
     */
    public function toJSON()
    {
        //  Return
        return json_encode( $this->toArray() );
    }


    /**
     * @inheritdoc
     */
    public function offsetExists( $offset )
    {
        //  Return
        return $this->exists( $offset );
    }

    /**
     * @inheritdoc
     */
    public function offsetGet( $offset )
    {
        //  Return
        return $this->get( $offset );
    }

    /**
     * @inheritdoc
     */
    public function offsetSet( $offset, $value )
    {
        //  Store
        $this->set( $offset, $value );
    }

    /**
     * @inheritdoc
     */
    public function offsetUnset( $offset )
    {
        //  Remove
        $this->remove( $offset );
    }
}