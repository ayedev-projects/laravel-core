<?php namespace Ayedev\LaravelCore\Traits;

trait PermissionTrait
{
    /**
     * Boot Trait
     */
    public static function bootPermissionTrait()
    {
        //  Listen Deleting
        static::deleting( function( $permission )
        {
            //  Check for Delete
            if( !method_exists( $permission, 'isForceDeleting' ) || $permission->isForceDeleting() )
            {
                //  Sync Permissions
                $permission->syncPermissionsBeforeDelete();
            }
        } );
    }

    /** Sync Roles */
    abstract public function syncPermissionsBeforeDelete();


    /**
     * Get Name
     *
     * @return string
     */
    public function getName()
    {
        //  Return
        return $this->attribute( 'name' );
    }

    /**
     * Get Label
     *
     * @return string
     */
    public function getLabel()
    {
        //  Return
        return $this->attribute( 'display_name' );
    }


    /**
     * Get the Permission
     *
     * @param $permission
     * @return static
     */
    public static function getPermission( $permission )
    {
        //  Return
        return self::whereName( $permission )->first();
    }

    /**
     * Save Permission
     *
     * @param $name
     * @param null $label
     * @param null $desc
     * @return static
     */
    public static function savePermission( $name, $label = null, $desc = null )
    {
        //  Search First
        $permission = static::getPermission( $name );

        //  Check
        if( !$permission )
        {
            //  Create Instance
            $permission = new static();
            $permission->name = $name;
            if( $label ) $permission->display_name = $label;
            if( $desc ) $permission->description = $desc;

            //  Save
            $permission->save();
        }

        //  Return
        return $permission;
    }
}