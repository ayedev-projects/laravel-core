<?php namespace Ayedev\LaravelCore\Traits;

trait HasActionTrait
{
    /** Define Actions */
    abstract public function actions();


    /**
     * Listen Calls
     *
     * @param $method
     * @param $parameters
     * @return mixed
     */
    public function __magicCallHasActionTrait( $method, $parameters )
    {
        //  Get Actions
        $actions = $this->actions();
        if( !$actions ) $actions = array();

        //  Result
        $result = null;

        //  Live Params
        $liveParams = ( sizeof( $parameters ) > 0 ? (array)$parameters[0] : array() );

        //  Loop Each
        foreach( $actions as $action => $actionData )
        {
            //  Found
            $found = null;

            //  Camelize
            $actionName = camel_case( $action );

            //  Check for Link Request
            if( in_array( $method, array( $actionName . 'Link', $actionName . 'URI' ) ) )
            {
                //  Set Found
                $found = 'link';
            }
            else if( in_array( $method, array( $actionName . 'Redirect', $actionName . 'Action' ) ) )
            {
                //  Set Found
                $found = 'redirect';
            }

            //  Check
            if( $found )
            {
                //  Params
                $params = array();

                //  Check
                if( isset( $actionData['copy'] ) && isset( $actions[$actionData['copy']] ) )    $actionData = $actions[$actionData['copy']];

                //  Check
                if( isset( $actionData['uri'] ) )
                {
                    //  Result
                    $result = ( is_callable( $actionData['uri'] ) ? call_user_func_array( $actionData['uri'], array( $this, user() ) ) : $actionData['uri'] );
                }
                else
                {
                    //  Raw Params
                    $rawParams = ( isset( $actionData['params'] ) ? ( is_callable( $actionData['params'] ) ? call_user_func_array( $actionData['params'], array( $this, user() ) ) : (array)$actionData['params'] ) : array() );

                    //  Morph Params
                    $morphParams = ( isset( $actionData['morph'] ) ? (array)$actionData['morph'] : array() );

                    //  Convert Params
                    $convertParams = ( isset( $actionData['convert'] ) ? (array)$actionData['convert'] : array() );

                    //  Loop Each
                    foreach( $rawParams as $key => $val )
                    {
                        //  Index Key
                        $indexKey = ( is_int( $key ) ? $val : $key );

                        //  The Value
                        $theValue = ( is_int( $key ) ? $this->getAttribute( $val ) : null );

                        //  Check
                        if( !is_int( $key ) )
                        {
                            //  Set
                            $theValue = ( is_callable( $val ) ? call_user_func_array( $val, array( $this, $key, $actionData ) ) : $val );
                        }

                        //  Check
                        if( isset( $morphParams[$indexKey] ) )  $indexKey = $morphParams[$indexKey];

                        //  Store the Value
                        $params[$indexKey] = $theValue;
                    }

                    //  Filler Params
                    $fillerParams = array();

                    //  Loop Each
                    foreach( $liveParams as $i => $v )
                    {
                        //  Check
                        if( is_null( $v ) ) continue;

                        //  Check
                        if( is_int( $i ) && isset( $convertParams[$i] ) )
                        {
                            //  Store
                            $fillerParams[ $convertParams[$i] ] = $v;
                        }
                        else
                        {
                            //  Store
                            $fillerParams[$i] = $v;
                        }
                    }

                    //  Route Name
                    $routeName = ( is_callable( $actionData['route'] ) ? call_user_func_array( $actionData['route'], array( $this, user() ) ) : $actionData['route'] );

                    //  Link
                    $link = route( $routeName, array_merge( $params, $fillerParams ) );

                    //  Result
                    $result = ( $found == 'link' ? $link : redirect( $link ) );
                }

                //  Break
                break;
            }
        }

        //  Return
        return $result;
    }

    /**
     * Check has Action
     *
     * @param $key
     * @return bool
     */
    public function hasAction( $key )
    {
        //  Get Actions
        $actions = $this->actions();
        if( !$actions ) $actions = array();

        //  Return
        return ( isset( $actions[$key] ) );
    }
}