<?php namespace Ayedev\LaravelCore\ModelSource;

use Illuminate\Http\UploadedFile;
use Ayedev\LaravelCore\Traits\HasFileTrait;
use Illuminate\Database\Eloquent\Model;
use Ayedev\LaravelCore\Traits\IsFileTrait;
use Symfony\Component\HttpFoundation\File\File;

class FileField
{
    /** @var Model $_model */
    protected $_model;

    /** @var string $_key */
    protected $_key;

    /** @var array $_config */
    protected $_config = array();

    /** @var IsFileTrait $_file */
    protected $_file;

    /** @var bool $_virtual */
    private $_virtual = false;

    /** @var bool $_clearOnSave */
    protected $_clearOnSave = false;


    /**
     * FileField constructor.
     *
     * @param $model
     * @param $defKey
     * @param $defConfig
     * @param null $file
     * @throws \Exception
     */
    public function __construct( $model, $defKey, $defConfig, $file = null )
    {
        //  Class Recursive
        $classes = trait_uses_recursive( get_class( $model ) );

        //  Check
        if( !in_array( HasFileTrait::class, $classes ) )
        {
            //  Throw Exception
            throw new \Exception( 'Invalid Class invoked: must be invoked via HasFileTrait' );
        }

        //  Store Details
        $this->_key = $defKey;
        $this->_config = $defConfig;

        //  Check Virtual
        $this->_virtual = ( @$this->_config['virtual'] === true );

        //  Set Model
        $this->setModel( $model );

        //  Check
        if( $file ) $this->assign( $file );
        else    $this->getFile();
    }

    /**
     * Assign Model
     *
     * @param $model
     * @return $this
     */
    public function setModel( $model )
    {
        //  Assign
        $this->_model = $model;

        //  Return
        return $this;
    }

    /**
     * Save File Collection
     */
    public function whenSaved()
    {
        //  Is Multiple
        $isMultiple = ( @$this->_config['multiple'] === true );

        //  Check
        if( !$isMultiple )
        {
            //  Run Save
            $this->saveFile();
        }

        //  Return
        return $this;
    }

    /**
     * Get the Model
     *
     * @return Model
     */
    public function getModel()
    {
        //  Return
        return $this->_model;
    }

    /**
     * Get Rel Key
     *
     * @return string
     */
    public function getRelKey()
    {
        //  Return
        return ( isset( $this->_config['rel_key'] ) ? $this->_config['rel_key'] : $this->getModel()->getTable() );
    }

    /**
     * Get Field Key
     *
     * @return string
     */
    public function getFieldKey()
    {
        //  Return
        return $this->_key;
    }

    /**
     * Get Field Name
     *
     * @return string
     */
    public function getFieldName()
    {
        //  Return
        return $this->getRelKey() . '.' . $this->getFieldKey();
    }

    /**
     * Get Sub Dir Path
     *
     * @return string
     */
    public function getSubPath()
    {
        //  Return
        return ( isset( $this->_config['path'] ) ? $this->_config['path'] : ( @$this->_config['multiple'] === true ? $this->getFieldKey() : str_plural( $this->getFieldKey() ) ) );
    }

    /**
     * Get the Class Name
     *
     * @return string
     */
    public function getClassName()
    {
        //  Return
        return ( isset( $this->_config['class'] ) ? $this->_config['class'] : $this->getModel()->resolveClassName( 'FileModel' ) );
    }

    /**
     * Get the File Object
     *
     * @param null $filename
     * @param bool $force
     * @return IsFileTrait
     */
    public function getFile( $filename = null, $force = false )
    {
        //  Check
        if( !$this->_file || $force )
        {
            //  Class Name
            $className = $this->getClassName();

            //  Get Field Name
            $fieldName = $this->getFieldName();

            //  Query
            $query = $className::where( 'rel_id', $this->getModel()->getID() )
                ->where( 'rel_type', $fieldName )
                ->where( 'category', ( isset( $this->_config['category'] ) ? $this->_config['category'] : 'default' ) );

            //  Force
            if( $force )    $query->remember( null );

            //  Check Identifier
            if( isset( $this->_config['identifier'] ) ) $query->where( 'identifier', $this->_config['identifier'] );

            //  Check File Name
            if( $filename ) $query->where( 'filename', $filename );

            //  Load
            $this->_file = $query->first();

            //  Check
            if( !$this->_file )
            {
                //  Create Instance
                $this->_file = new $className;

                //  Assign Properties
                $this->_file->setAttribute( 'rel_id', $this->getModel()->getID() );
                $this->_file->setAttribute( 'rel_type', $fieldName );
                $this->_file->setAttribute( 'category', ( isset( $this->_config['category'] ) ? $this->_config['category'] : 'default' ) );
                if( isset( $this->_config['identifier'] ) ) $this->_file->setAttribute( 'identifier', $this->_config['identifier'] );
            }
        }

        //  Return
        return $this->_file;
    }


    /**
     * Assign new File
     *
     * @param $file
     * @return $this
     * @throws \Exception
     */
    public function add( $file )
    {
        //  Return
        return $this->assign( $file );
    }

    /**
     * Assign new File
     *
     * @param $file
     * @return $this
     * @throws \Exception
     */
    public function assign( $file )
    {
        //  Class Name
        $className = $this->getClassName();

        //  Check for File Instance
        if( $file instanceof $className )
        {
            //  Store
            $this->_file = $file;
        }
        //  Check for Load
        else if( substr( $file, 0, 5 ) == 'load:' )
        {
            //  Get Filename
            $fileName = substr( $file, 5 );

            //  Get the File
            $this->getFile( $fileName, true );
        }
        else
        {
            //  File Name
            $fileName = null;

            //  Real Path
            $realPath = null;

            //  Move File
            $moveFile = true;

            //  Sub Path
            $subPath = $this->getRelKey() . '/' . $this->getSubPath() . '/';

            //  Dir Path
            $dirPath = assets()->upload( $subPath, false );

            //  Make Directory if doesn't exist
            if( !file_exists( $dirPath ) )  mkdir( $dirPath, 0777, true );

            //  Check for UploadedFile
            if( $file instanceof UploadedFile )
            {
                //  Get the Filename
                $fileName = $file->getClientOriginalName();
            }
            //  Check for String or FileInfo
            else if( is_string( $file ) || $file instanceof File )
            {
                //  Check for String
                if( is_string( $file ) )
                {
                    //  Get Full Path
                    $fullPath = ( file_exists( $file ) ? $file : assets()->upload( $file, false ) );

                    //  Move File
                    $moveFile = !file_exists( $fullPath );

                    //  Load the File
                    $file = new File( $fullPath );
                }

                //  Filename
                $fileName = $file->getFilename();

                //  Set Real Path
                $realPath = $file->getRealPath();
            }

            //  Check
            if( $fileName )
            {
                //  Check
                if( $moveFile && file_exists( $dirPath . $fileName ) )
                {
                    //  Change Filename
                    $fileName = pathinfo( $fileName, PATHINFO_FILENAME ) . '_' . uniqid() . '.' . pathinfo( $fileName, PATHINFO_EXTENSION );
                }

                //  Check
                if( $moveFile && strlen( $fileName ) > 100 ) $fileName = substr( $fileName, 0, 20 ) . '_' . uniqid() . '.' . pathinfo( $fileName, PATHINFO_EXTENSION );

                //  Copy File
                if( $file instanceof UploadedFile ) $file->move( $dirPath, $fileName );
                else if( $moveFile )    @copy( $realPath, assets()->upload( $subPath . $fileName, false ) );

                //  Set File Path
                $filePath = $subPath . $fileName;

                //  Set Filename
                $this->getFile()->setAttribute( 'filename', basename( $fileName ) );

                //  Set the File Path
                $this->getFile()->setAttribute( 'filepath', $filePath );

                //  Set Title
                $this->getFile()->setAttribute( 'title', pathinfo( $filePath, PATHINFO_FILENAME ) );

                //  Sync the Data
                $this->sync();

                //  Mark for no Clear
                $this->clear( false );
            }
        }

        //  Return
        return $this;
    }

    /**
     * Sync the File Info
     *
     * @return $this
     */
    public function sync()
    {
        //  Run Sync
        $this->getFile()->syncFileInfo();

        //  Return
        return $this;
    }

    /**
     * Clear the File
     *
     * @param bool $clear
     * @return $this
     */
    public function clear( $clear = true )
    {
        //  Mark for Clear
        $this->_clearOnSave = $clear;

        //  Return
        return $this;
    }

    /**
     * Check if needs to be cleared
     *
     * @return bool
     */
    public function needsToBeCleared()
    {
        //  Return
        return $this->_clearOnSave;
    }

    /**
     * Clear the File
     */
    public function clearFile()
    {
        //  Unlink File
        if( $this->getFile()->exists() )    @unlink( $this->getFile()->path() );

        //  Delete
        $this->getFile()->delete();

        //  Clear the Reference
        $this->_file = null;

        //  Return
        return $this;
    }

    /**
     * Run Save
     */
    public function saveFile( $identifier = null )
    {
        //  Check for Clear
        if( $this->needsToBeCleared() )
        {
            //  Delete
            $this->clearFile();

            //  Return
            return false;
        }
        else
        {
            //  Assign the ID
            $this->getFile()->setAttribute( 'rel_id', $this->getModel()->getID() );

            //  Save
            if( $this->getFile()->relPath() )   $this->getFile()->save();
        }

        //  Return
        return true;
    }


    /**
     * Magic Getter
     *
     * @param $key
     * @return mixed
     * @throws \Exception
     */
    public function __get( $key )
    {
        //  Check
        if( !$this->getFile() ) throw new \Exception( 'File Model is not loaded' );

        //  Return
        return $this->getFile()->getAttribute( $key );
    }

    /**
     * Magic Call
     *
     * @param $method
     * @param $parameters
     * @return mixed
     * @throws \Exception
     */
    public function __call( $method, $parameters )
    {
        //  Check
        if( !$this->getFile() ) throw new \Exception( 'File Model is not loaded' );

        //  Return
        return call_user_func_array( array( $this->getFile(), $method ), $parameters );
    }

    /**
     * Convert to String
     *
     * @return string
     */
    public function __toString()
    {
        //  Return
        return ( $this->getFile() ? (string) $this->getFile()->getAttribute( 'filename' ) : '' );
    }
}