<?php namespace Ayedev\LaravelCore\ModelSource;

use Illuminate\Database\Eloquent\Collection;
use Ayedev\LaravelCore\Traits\HasFileTrait;
use Illuminate\Database\Eloquent\Model;

class FileFieldCollection
{
    /** @var Model $_model */
    protected $_model;

    /** @var string $_key */
    protected $_key;

    /** @var array $_config */
    protected $_config = array();

    /** @var Collection $_files */
    protected $_files;

    /** @var int $_limit */
    private $_limit = -1;

    /** @var int $_maxCount */
    private $_maxCount = 0;

    /** @var bool $_clearOnSave */
    protected $_clearOnSave = false;


    /**
     * FileFieldCollection constructor.
     *
     * @param $model
     * @param $defKey
     * @param $defConfig
     * @param null $file
     * @throws \Exception
     */
    public function __construct( $model, $defKey, $defConfig, $file = null )
    {
        //  Class Recursive
        $classes = trait_uses_recursive( get_class( $model ) );

        //  Check
        if( !in_array( HasFileTrait::class, $classes ) )
        {
            //  Throw Exception
            throw new \Exception( 'Invalid Class invoked: must be invoked via HasFileTrait' );
        }

        //  Store Details
        $this->_key = $defKey;
        $this->_config = $defConfig;

        //  Check Limit
        $this->_limit = ( isset( $this->_config['limit'] ) ? $this->_config['limit'] : -1 );

        //  Set Model
        $this->setModel( $model );

        //  Load Files
        if( $file ) $this->add( $file );
        else    $this->getFiles();
    }

    /**
     * Assign Model
     *
     * @param $model
     * @return $this
     */
    public function setModel( $model )
    {
        //  Assign
        $this->_model = $model;

        //  Return
        return $this;
    }

    /**
     * Save File Collection
     */
    public function whenSaved()
    {
        //  Save
        $this->save();
    }

    /**
     * Get the Model
     *
     * @return Model
     */
    public function getModel()
    {
        //  Return
        return $this->_model;
    }

    /**
     * Get Rel Key
     *
     * @return string
     */
    public function getRelKey()
    {
        //  Return
        return ( isset( $this->_config['rel_key'] ) ? $this->_config['rel_key'] : $this->getModel()->getTable() );
    }

    /**
     * Get Field Name
     *
     * @return string
     */
    public function getFieldName()
    {
        //  Return
        return $this->getRelKey() . '.' . $this->_key;
    }

    /**
     * Make Identifier
     *
     * @return string
     */
    public function makeIdentifier( $index )
    {
        //  Return
        return $this->getFieldName() . '.' . $index;
    }

    /**
     * Get the Class Name
     *
     * @return string
     */
    public function getClassName()
    {
        //  Return
        return ( isset( $this->_config['class'] ) ? $this->_config['class'] : $this->getModel()->resolveClassName( 'FileModel' ) );
    }

    /**
     * Get all Files
     *
     * @param bool $force
     * @return Collection
     */
    public function getFiles( $force = false )
    {
        //  Check
        if( is_null( $this->_files ) || $force )
        {
            //  Class Name
            $className = $this->getClassName();

            //  Get Field Name
            $fieldName = $this->getFieldName();

            //  Query
            $query = $className::where( 'rel_id', $this->getModel()->getID() )
                ->where( 'rel_type', $fieldName )
                ->where( 'category', ( isset( $this->_config['category'] ) ? $this->_config['category'] : 'default' ) )
                ->orderBy( 'identifier', 'ASC' );

            //  Force
            if( $force )    $query->remember( null );

            //  Check Identifier
            if( isset( $this->_config['identifier'] ) ) $query->where( 'identifier', $this->_config['identifier'] );

            //  Create Empty
            $this->_files = new Collection();

            //  Loop Each
            foreach( $query->get() as $item )
            {
                //  Split
                $splits = explode( '.', $item->identifier );

                //  Append
                $this->add( $item, $item->identifier );

                //  Check
                if( end( $splits ) > $this->_maxCount ) $this->_maxCount = intval( end( $splits ) );
            }
        }

        //  Return
        return $this->_files;
    }

    /**
     * Get the Files: Alias
     *
     */
    public function all()
    {
        //  Return
        return $this->getFiles();
    }

    /**
     * Get the Files Count
     *
     * @return int
     */
    public function total()
    {
        //  Return
        return $this->getFiles()->count();
    }

    /**
     * Get the Files Count : ALIAS
     *
     * @return int
     */
    public function count()
    {
        //  Return
        return $this->total();
    }

    /**
     * Get the First File
     *
     * @return mixed
     */
    public function firstFile()
    {
        //  Return
        return $this->getFiles()->first();
    }

    /**
     * Get the File by Identifier
     *
     * @param $identifier
     * @return FileField|null
     */
    public function fileByIdentifier( $identifier )
    {
        //  Return
        return $this->getFiles()->filter( function( FileField $file ) use ( $identifier )
        {
            //  Return
            return ( $file->getFile()->relIdentifier() == $identifier );
        } )->first();
    }

    /**
     * Get the Featured Files
     *
     * @return mixed
     */
    public function featuredFiles()
    {
        //  Return
        return $this->getFiles()->filter( function( $file )
        {
            //  Return
            return $file->getFile()->isFeatured();
        } );
    }

    /**
     * Get the Featured File
     *
     * @return mixed
     */
    public function featuredFile()
    {
        //  Return
        return $this->featuredFiles()->first();
    }


    /**
     * Add Multiple Files
     *
     * @param array $files
     * @return array
     */
    public function addFiles( array $files )
    {
        //  Data
        $data = array();

        //  Loop Each
        foreach( $files as $file )
        {
            //  Append
            $data[] = $this->add( $file );
        }

        //  Return
        return $data;
    }

    /**
     * Add File to Collection
     *
     * @param $file
     * @param null $identifier
     * @param bool $self
     * @return mixed
     */
    public function add( $file, $identifier = null, $self = false )
    {
        //  Check
        if( $this->_limit > -1 && $this->getFiles()->count() >= $this->_limit ) return false;

        //  Identifier
        $theIdentifier = $identifier ?: ( $this->makeIdentifier( $this->_maxCount + 1 ) );

        //  Create the File
        $object = new FileField( $this->getModel(), $this->_key, array_merge( $this->_config, array( 'identifier' => $theIdentifier ) ), $file );

        //  Add to Collection
        $this->getFiles()->add( $object );

        //  Change the Max Count
        if( !$identifier )  $this->_maxCount += 1;

        //  Return
        return ( $self ? $this : $object );
    }

    /**
     * Save File to Collection
     *
     * @param $file
     * @param null $identifier
     * @param bool $self
     * @return mixed
     */
    public function saveFile( $file, $identifier = null, $self = false )
    {
        //  Return
        return $this->add( $file, $identifier, $self );
    }

    /**
     * Remove File(s)
     *
     * @param $indexes
     * @return $this
     */
    public function remove( $indexes )
    {
        //  Indexed Values
        $indexedValues = array();

        //  Loop Each
        foreach( (array)$indexes as $ind )
        {
            //  Append
            $indexedValues[] = $this->makeIdentifier( $ind );
        }

        //  Loop
        foreach( $this->getFiles() as $file )
        {
            //  Check
            if( in_array( $file->getAttribute( 'identifier' ), $indexedValues ) )
            {
                //  Clear
                $file->clear();
            }
        }

        //  Return
        return $this;
    }

    /**
     * Save Files
     */
    public function save()
    {
        //  Count Index
        $countIndex = 0;

        //  Loop Each Items
        foreach( $this->getFiles()->all() as $i => $file )
        {
            //  Savable
            $savable = !$file->needsToBeCleared();

            //  New Identifier
            $newIdentifier = ( $savable ? $this->makeIdentifier( $countIndex + 1 ) : null );

            //  Save Again with Identifier
            $file->saveFile( $newIdentifier );

            //  Count Index
            if( $savable )    $countIndex++;
        }
    }

    /**
     * Clear All
     */
    public function clear()
    {
        //  Loop Each Items
        foreach( $this->getFiles() as $file )
        {
            //  Save
            $file->clear();
        }

        //  Return
        return $this;
    }

    /**
     * Get Field by Field
     *
     * @param $value
     * @param string $field
     * @return null|FileField
     */
    public function getFile( $value, $field = 'identifier' )
    {
        //  File
        $file = null;

        //  Fix Value
        if( $field == 'identifier' )    $value = $this->makeIdentifier( $value );

        //  Get the Filtered Collection
        $fCollection = $this->getFiles()->filter( function( FileField $file ) use ( $value, $field )
        {
            //  Return
            return ( $file->getFile()->getAttribute( $field ) == $value );
        } );

        //  Check
        if( $fCollection && $fCollection->count() > 0 ) $file = $fCollection->first();

        //  Return
        return $file;
    }


    /**
     * Magic Call
     *
     * @param $method
     * @param $parameters
     * @return mixed
     * @throws \Exception
     */
    public function __call( $method, $parameters )
    {
        //  Check
        if( !$this->getFiles() ) throw new \Exception( 'File Model is not loaded' );

        //  Return
        return call_user_func_array( array( $this->getFiles(), $method ), $parameters );
    }


    /**
     * Convert to String
     *
     * @return string
     */
    public function __toString()
    {
        //  Return
        return $this->getFiles()->toJson();
    }
}
